DROP TABLE accounts;

CREATE TABLE `accounts` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `hide` int(10) NOT NULL,
  `password` varchar(100) NOT NULL,
  `ip` varchar(100) NOT NULL DEFAULT '0',
  `joined` varchar(100) NOT NULL,
  `lpip` int(255) NOT NULL,
  `frozen` varchar(1) NOT NULL DEFAULT '0',
  `notes` text NOT NULL,
  `tpl` text NOT NULL,
  `rank` int(10) NOT NULL DEFAULT '0',
  `lastlogin` varchar(100) NOT NULL,
  `signature` text NOT NULL,
  `gender` int(10) NOT NULL,
  `location` varchar(100) NOT NULL,
  `timezone` varchar(100) NOT NULL DEFAULT '0',
  `warning` int(10) NOT NULL DEFAULT '0',
  `postcount` int(100) NOT NULL DEFAULT '0',
  `bantime` int(100) NOT NULL DEFAULT '0',
  `reputation` int(100) NOT NULL DEFAULT '0',
  `emailme` int(100) NOT NULL DEFAULT '1',
  `lastpost` varchar(100) NOT NULL,
  `activated` int(100) NOT NULL DEFAULT '0',
  `activation_code` varchar(100) NOT NULL,
  `nickn` varchar(225) NOT NULL,
  `fb` varchar(225) NOT NULL,
  `tw` varchar(225) NOT NULL,
  `tb` varchar(225) NOT NULL,
  `siterank` varchar(225) NOT NULL DEFAULT 'Member',
  `website` varchar(1000) NOT NULL,
  `class` varchar(225) NOT NULL DEFAULT 'userfont',
  `flocation` varchar(1000) NOT NULL,
  `page` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

INSERT INTO accounts VALUES("0","System","none@none.com","0","","0","1360544481","0","0","","","0","1360544481","","0","","0","0","3","0","0","1","1364615983","1","","","","","","","","","$session_location","$page_title_pro");
INSERT INTO accounts VALUES("1","Ipsum","sasori_150@hotmail.com","0","756fb2b980ffa9a26e5290293efba3aa","0","1360544481","15856","0","","default","16","1367791567","[align=center][img]http://i666.photobucket.com/albums/vv25/hi2431/p-z-p1_zpse1b4ab7c.png[/img][/align]","1","Atlanta, Georgia ","-18000","1","353","0","10","0","1367687078","1","","Daiske","1","","","Webmaster","forum.pirate-zone.com","malikfont","http://forum.pirate-zone.com","> Forum");
INSERT INTO accounts VALUES("2","Linksbrawler","Linksbrawler@Hotmail.com","0","55e7dd3016ce4ac57b9a0f56af12f7c2","207.112.82.103","1361584685","17501","0","","default","14","1367737934","[align=center]Thanks Night for the Icon, thanks Nick for the sig, and thanks Oreo for helping me with the php coding :D I owe you all.[/align]\n[align=center][img]http://gyazo.com/d3b8986acc0271791b138ca9a7720796.png[/img][/align]\n[align=center][url=http://aniver.se/]Anime Downloads[/url][/align]\n[align=center][url=http://aniver.se/]Free Anime Downloads[/url][/align]","1","Canada","-18000","0","140","0","3","1","1366922158","1","6d27jegp","Link","","","","Webmaster1","","linkfont","/"," > Forum");
INSERT INTO accounts VALUES("6","OceanMaster","oceanmaster98@hotmail.com","1","7c4687dc6c6c96d5dc78c38945395627","69.9.112.220","1366079632","8240","0","","","15","1367656863","","1","Vancouver","-28800","1","39","0","0","1","1367656861","1","wfp4sku2","Ocean","","","","Administrator","","adminfont","/"," > Forum");
INSERT INTO accounts VALUES("3","Nelo","Pirate_King0@hotmail.com","0","948a8ec07a3e9d38dd51cc84a055f41e","69.171.163.123","1363647891","584","0","","Nelo","1","1367665411","","1","","0","0","23","0","0","1","1366746011","1","fbhq2kvw","","","","","Member","","userfont","/?area=forum&s=topic&t=63&page=1"," >Staff Only > Started From the Bottom > Books");
INSERT INTO accounts VALUES("4","Dracodino300","dce300@gmail.com","0","208b0f72fd7b35bd6ee92eea78bb88bc","86.176.1.46","1363997508","15701","0","","default","3","1367746186","   [IMG]http://i1128.photobucket.com/albums/m493/dracodino300/AlduinSmall_zps3c4312c9.png[/IMG]\n[size=14][i]And the Scrolls have foretold, of black wings in the cold, that when brothers wage war come unfurled, \nAlduin, Bane of Kings, ancient shadow unbound, with a hunger to swallow the world.[/i][/size]\n","1","UK","0","0","34","0","0","1","1366872563","1","","Draco","","","","Administrator","","dracofont","/"," > Forum");
INSERT INTO accounts VALUES("5","Pirate-Zone","webmaster@pirate-zone.com","0","756fb2b980ffa9a26e5290293efba3aa","76.105.109.168","1364086861","6253","0","","default","2","1367622589","","1","","0","0","1","0","0","1","1365648964","1","rtdif3zw","","","","","Member","http://forum.pirate-zone.com","userfont","/?area=profile&user=6"," Profile > OceanMaster");



DROP TABLE crew_members;

CREATE TABLE `crew_members` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `account_id` int(100) NOT NULL,
  `crew_id` int(100) NOT NULL,
  `crew_rank` int(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO crew_members VALUES("1","1","1","1");
INSERT INTO crew_members VALUES("2","3","2","1");
INSERT INTO crew_members VALUES("3","2","3","1");



DROP TABLE crew_ranks;

CREATE TABLE `crew_ranks` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `crew_id` int(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  `group` int(10) NOT NULL,
  KEY `id` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO crew_ranks VALUES("1","1","Madness","1");
INSERT INTO crew_ranks VALUES("2","2","Crew Leader","1");
INSERT INTO crew_ranks VALUES("3","3","Crew Leader","1");



DROP TABLE crews;

CREATE TABLE `crews` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `author_id` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `short` varchar(100) NOT NULL,
  `date` varchar(100) NOT NULL,
  `level` int(100) NOT NULL DEFAULT '0',
  `exp` int(100) NOT NULL DEFAULT '0',
  `wins` int(100) NOT NULL DEFAULT '0',
  `lose` int(100) NOT NULL DEFAULT '0',
  `percentage` int(100) NOT NULL DEFAULT '0',
  `topic` varchar(1000) NOT NULL,
  `thread` varchar(1000) NOT NULL,
  `forum` varchar(1000) NOT NULL,
  `chat` varchar(1000) NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO crews VALUES("1","1","Kyouki","Kyo","1365912596","0","0","0","0","0","http://forum.pirate-zone.com/","http://forum.pirate-zone.com/?area=forums=topic&amp;t=46","http://forum.pirate-zone.com/","http://forum.pirate-zone.com/","dsssssssssss");
INSERT INTO crews VALUES("2","3","Nelo Crew","Nelo","1365912830","0","0","0","0","0","","","","","");
INSERT INTO crews VALUES("3","2","Straw Hats","SH","1365916243","0","0","0","0","0","","","","","");



DROP TABLE forum_attachments;

CREATE TABLE `forum_attachments` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `account_id` int(100) NOT NULL,
  `file` varchar(250) NOT NULL,
  `date` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;




DROP TABLE forum_banlist;

CREATE TABLE `forum_banlist` (
  `id` int(200) NOT NULL AUTO_INCREMENT,
  `value` varchar(100) NOT NULL,
  `type` int(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;




DROP TABLE forum_categories;

CREATE TABLE `forum_categories` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `sort` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

INSERT INTO forum_categories VALUES("1","Marineford","2");
INSERT INTO forum_categories VALUES("2","Staff Only","1");
INSERT INTO forum_categories VALUES("3","The Going Merry","3");
INSERT INTO forum_categories VALUES("4","Pirate\'s Treasures","4");
INSERT INTO forum_categories VALUES("7","Otaku\'s Place ","5");
INSERT INTO forum_categories VALUES("8","Grand Line","6");
INSERT INTO forum_categories VALUES("9","Sabaody Archipelago","7");



DROP TABLE forum_categories_permission;

CREATE TABLE `forum_categories_permission` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `category_id` int(100) NOT NULL,
  `group_id` int(100) NOT NULL,
  `view` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;

INSERT INTO forum_categories_permission VALUES("1","1","1","1");
INSERT INTO forum_categories_permission VALUES("2","1","2","1");
INSERT INTO forum_categories_permission VALUES("3","1","3","1");
INSERT INTO forum_categories_permission VALUES("4","1","4","1");
INSERT INTO forum_categories_permission VALUES("5","2","4","1");
INSERT INTO forum_categories_permission VALUES("6","2","3","1");
INSERT INTO forum_categories_permission VALUES("8","2","2","1");
INSERT INTO forum_categories_permission VALUES("9","2","7","1");
INSERT INTO forum_categories_permission VALUES("10","1","7","1");
INSERT INTO forum_categories_permission VALUES("11","1","6","1");
INSERT INTO forum_categories_permission VALUES("12","1","5","1");
INSERT INTO forum_categories_permission VALUES("15","2","5","1");
INSERT INTO forum_categories_permission VALUES("16","3","2","1");
INSERT INTO forum_categories_permission VALUES("17","3","3","1");
INSERT INTO forum_categories_permission VALUES("18","3","1","1");
INSERT INTO forum_categories_permission VALUES("19","3","4","1");
INSERT INTO forum_categories_permission VALUES("20","3","6","1");
INSERT INTO forum_categories_permission VALUES("21","3","5","1");
INSERT INTO forum_categories_permission VALUES("22","4","2","1");
INSERT INTO forum_categories_permission VALUES("23","4","1","1");
INSERT INTO forum_categories_permission VALUES("24","4","3","1");
INSERT INTO forum_categories_permission VALUES("25","4","4","1");
INSERT INTO forum_categories_permission VALUES("26","4","5","1");
INSERT INTO forum_categories_permission VALUES("27","4","6","1");
INSERT INTO forum_categories_permission VALUES("28","7","1","1");
INSERT INTO forum_categories_permission VALUES("29","7","6","1");
INSERT INTO forum_categories_permission VALUES("30","7","2","1");
INSERT INTO forum_categories_permission VALUES("31","7","3","1");
INSERT INTO forum_categories_permission VALUES("32","7","4","1");
INSERT INTO forum_categories_permission VALUES("33","7","5","1");
INSERT INTO forum_categories_permission VALUES("34","8","1","1");
INSERT INTO forum_categories_permission VALUES("35","8","2","1");
INSERT INTO forum_categories_permission VALUES("36","8","3","1");
INSERT INTO forum_categories_permission VALUES("37","8","4","1");
INSERT INTO forum_categories_permission VALUES("38","8","5","1");
INSERT INTO forum_categories_permission VALUES("39","8","6","1");
INSERT INTO forum_categories_permission VALUES("40","9","2","1");
INSERT INTO forum_categories_permission VALUES("41","9","5","1");
INSERT INTO forum_categories_permission VALUES("42","9","1","1");
INSERT INTO forum_categories_permission VALUES("43","9","3","1");
INSERT INTO forum_categories_permission VALUES("44","9","4","1");
INSERT INTO forum_categories_permission VALUES("45","9","6","1");



DROP TABLE forum_confdata;

CREATE TABLE `forum_confdata` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

INSERT INTO forum_confdata VALUES("1","userreg","1");
INSERT INTO forum_confdata VALUES("2","siteclosed","0");
INSERT INTO forum_confdata VALUES("3","version","Official Forum of Pirate-Zone ");
INSERT INTO forum_confdata VALUES("4","meta_info","Official Forum of Pirate-Zone");
INSERT INTO forum_confdata VALUES("5","meta_keywords","Official Forum of Pirate-Zone ");
INSERT INTO forum_confdata VALUES("6","usertemplate","0");
INSERT INTO forum_confdata VALUES("7","admkey","84500228237696242");
INSERT INTO forum_confdata VALUES("8","session","PZSESS");
INSERT INTO forum_confdata VALUES("9","adminemail","webmaster@pirate-zone.com");
INSERT INTO forum_confdata VALUES("10","iplock","0");
INSERT INTO forum_confdata VALUES("11","activation","1");
INSERT INTO forum_confdata VALUES("12","avatar","1");
INSERT INTO forum_confdata VALUES("13","sitename","Pirate-Zone Forum");
INSERT INTO forum_confdata VALUES("14","url","http://forum.pirate-zone.com");
INSERT INTO forum_confdata VALUES("15","template","default");
INSERT INTO forum_confdata VALUES("16","path","");
INSERT INTO forum_confdata VALUES("17","postlimit","15");
INSERT INTO forum_confdata VALUES("18","topiclimit","15");
INSERT INTO forum_confdata VALUES("19","staffnotice","");
INSERT INTO forum_confdata VALUES("20","hottopic","5");
INSERT INTO forum_confdata VALUES("21","tos","By accessing http://pirate-zone.com, you agree to the following terms, If you do not agree with these terms then please do not access http://pirate-zone.com These terms can be modified, added to or removed at any time, it is your responsibility to regularly check these terms and familiarise yourself with them as your continued access of http://pirate-zone.com will be bound by these terms as they are updated.\n\n\nYou agree and understand that a &quot;User Account&quot; is not property of the individual but of http://pirate-zone.com, this includes the right for management to modify, suspend, hinder or terminate user accounts as deemed appropriate.\nYou agree to follow any displayed Terms and conditions / Rules displayed within this site at all times as bound under this agreement.\n\nYou agree for all data submitted to http://pirate-zone.com to be retained securely within the forum database and used for the site to function as intended, the management agrees to treat all data confidentially under the Data Protection act. ");
INSERT INTO forum_confdata VALUES("22","avatar_filesize","    25600");
INSERT INTO forum_confdata VALUES("23","avatar_width","100");
INSERT INTO forum_confdata VALUES("24","avatar_height","100");
INSERT INTO forum_confdata VALUES("25","attach","1");
INSERT INTO forum_confdata VALUES("26","attach_filesize","0 ");
INSERT INTO forum_confdata VALUES("27","anti_flood","12");
INSERT INTO forum_confdata VALUES("28","language","english");
INSERT INTO forum_confdata VALUES("29","facebook_like","0");
INSERT INTO forum_confdata VALUES("30","rss","0");
INSERT INTO forum_confdata VALUES("31","activation","1");



DROP TABLE forum_extensions;

CREATE TABLE `forum_extensions` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `value` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

INSERT INTO forum_extensions VALUES("3","png");



DROP TABLE forum_forums;

CREATE TABLE `forum_forums` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `name` varchar(225) NOT NULL,
  `info` text NOT NULL,
  `cat_id` int(100) NOT NULL,
  `parent_id` int(100) NOT NULL DEFAULT '0',
  `sort` int(10) NOT NULL DEFAULT '0',
  `postcount` int(225) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;

INSERT INTO forum_forums VALUES("1","Announcements &amp; News","Find here the latest information about the game and forums.","1","0","1","1");
INSERT INTO forum_forums VALUES("3","Project FX","This forum is for our staff to project / think about In-Game and Forums.","2","0","1","-73");
INSERT INTO forum_forums VALUES("4","Death Note","Talk about bans here.","2","0","2","0");
INSERT INTO forum_forums VALUES("6","Started From the Bottom","This is were everything else gets posted that doesn\'t fit in the staff forums.","2","0","4","-21");
INSERT INTO forum_forums VALUES("7","Admiral\'s Lounge ","..The Admins Secret Forum..","2","0","5","32");
INSERT INTO forum_forums VALUES("8","High Sea Council","Where decisions are made.","2","0","6","65");
INSERT INTO forum_forums VALUES("9","Latest Game Updates","Any updates on the characters will be posted in here.","1","0","2","0");
INSERT INTO forum_forums VALUES("10","Game Feedback","Problems, updates and suggestions about the gaming engine can be posted here.","1","0","3","0");
INSERT INTO forum_forums VALUES("11","Forum Feedback","Problems, updates and suggestions about the forums can be posted here.","1","0","4","0");
INSERT INTO forum_forums VALUES("12","Showcases","Here you can post your showcase to others members to see.","3","0","1","0");
INSERT INTO forum_forums VALUES("13","Battles, Crew Battles and Tournaments","Are you hungry for battle? Do you want to test out your skills? Plan your fights and matches here.","3","0","2","0");
INSERT INTO forum_forums VALUES("14","Crews"," Create, organize, recruit. Everything concerning Pirate-Zone crews can be managed in here!","3","0","3","9");
INSERT INTO forum_forums VALUES("15","Team Strategies &amp; Missions","Need teams to rank up? For a specific mission? Or share a strategy? Find/Post them here!","3","0","4","0");
INSERT INTO forum_forums VALUES("16","Contests","Battle it out against people in your custom made contests!","4","0","1","0");
INSERT INTO forum_forums VALUES("17","Custom Made Characters","Here you can find custom characters created by our members.","4","0","7","0");
INSERT INTO forum_forums VALUES("18","Guides &amp; Tutorials","Here you can find tutorials and guides made by the members.","4","0","8","0");
INSERT INTO forum_forums VALUES("19","Great Ship of Gfxing","Here you can find various graphic images, icons, avatars and much more.","4","0","9","0");
INSERT INTO forum_forums VALUES("20","Official Contests","Official contests held by the staff of Pirate-Boards!","4","16","10","0");
INSERT INTO forum_forums VALUES("21","Signature &amp; Avatar Battles","Do you think your signature or avatar looks better than someone else\'s? Battle it out here!","4","16","11","0");
INSERT INTO forum_forums VALUES("22","Custom Character Competitions","Your custom-made characters can battle it out in this section.","4","16","12","0");
INSERT INTO forum_forums VALUES("24","Believe It","Where the news and announcements are posted for the staff","2","0","7","0");
INSERT INTO forum_forums VALUES("25","The Magical Whale Pagoda","Where we post about new characters.","2","7","8","0");
INSERT INTO forum_forums VALUES("26","Missions","Post missions","2","7","9","0");
INSERT INTO forum_forums VALUES("27","The Devil Fruits","Talk about promotions/demotions.","2","7","10","0");
INSERT INTO forum_forums VALUES("28","One Piece","Discuss about the One Piece anime and manga here.","7","0","1","0");
INSERT INTO forum_forums VALUES("29","Fairy Tail","Discuss about the Fairy Tail anime and manga here.","7","0","2","38");
INSERT INTO forum_forums VALUES("30","Naruto","Discuss about the Naruto anime and manga here.","7","0","3","0");
INSERT INTO forum_forums VALUES("31","Other anime series discussions","Discuss about other anime series in here.","7","39","5","0");
INSERT INTO forum_forums VALUES("32","Other manga series discussions","Discuss about other manga series in here.","7","39","6","0");
INSERT INTO forum_forums VALUES("33","Anime","Discuss about the One Piece anime.","7","28","7","16");
INSERT INTO forum_forums VALUES("34","Manga","Discuss about the One Piece manga.","7","28","8","0");
INSERT INTO forum_forums VALUES("35","Anime","Discuss about the Fairy Tail anime.","7","29","9","0");
INSERT INTO forum_forums VALUES("36","Manga","Discuss about the Fairy Tail manga.","7","29","10","0");
INSERT INTO forum_forums VALUES("37","Anime","Discuss about the Naruto anime.","7","30","11","0");
INSERT INTO forum_forums VALUES("38","Manga","Discuss about the Naruto manga.","7","30","12","0");
INSERT INTO forum_forums VALUES("39","Other series","Discuss about other anime and manga here.","7","0","4","0");
INSERT INTO forum_forums VALUES("40","Otaku chat","Talk about otaku things here","7","0","13","19");
INSERT INTO forum_forums VALUES("41","Introduction &amp; Farewells ","Introduce yourself or say your goodbyes ","8","0","1","-7");
INSERT INTO forum_forums VALUES("42","General Discussion","Talk about anything else here","8","0","2","0");
INSERT INTO forum_forums VALUES("43","Forum Games","Forum games, chat threads and useless threads can be found here. Do not spam!","8","0","3","0");
INSERT INTO forum_forums VALUES("44","Debate Center","Have something you want to debate? Then this is the place! (Keep it within the forum rules)","9","0","1","0");
INSERT INTO forum_forums VALUES("45","Electronics ","Everything about electronics can be posted here.","9","0","2","0");
INSERT INTO forum_forums VALUES("46","Games","Everything about computer, console, and other types of games can be posted in here.","9","45","3","0");
INSERT INTO forum_forums VALUES("47","Computers","Having computer troubles? Post your question here and you might find a solution.","9","45","4","0");
INSERT INTO forum_forums VALUES("48","Music","Any topic related to music can be posted here.","9","0","5","0");
INSERT INTO forum_forums VALUES("49","Series and Movies","Chat about the shows running on the small screen at home or of the big blockbusters at the movies!","9","0","6","0");
INSERT INTO forum_forums VALUES("50","Sports","Talk about your favorite sports teams or get exercises you like to do.","9","0","7","0");
INSERT INTO forum_forums VALUES("51","Off Topic","Can\'t find the proper section for your topic? Then this might be it! Post here topics which aren\'t covered by any other section.","9","0","8","0");
INSERT INTO forum_forums VALUES("52","Battles","Plan your battles against other pirates in here.","3","13","5","0");
INSERT INTO forum_forums VALUES("53","Crew Battles","Organize your crew battles here!","3","13","6","0");
INSERT INTO forum_forums VALUES("54","Tournaments","Organize your own or join an existing tournament.","3","13","7","3");
INSERT INTO forum_forums VALUES("55","Mirai kanji","Talk about Mirai kanji (the Future)","2","7","11","0");



DROP TABLE forum_forums_permission;

CREATE TABLE `forum_forums_permission` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `forum_id` int(100) NOT NULL,
  `group_id` int(100) NOT NULL,
  `view` int(100) NOT NULL DEFAULT '0',
  `post` int(100) NOT NULL DEFAULT '0',
  `reply` int(100) NOT NULL DEFAULT '0',
  `upload` int(100) NOT NULL DEFAULT '0',
  `moderator` int(100) NOT NULL DEFAULT '0',
  `poll` int(100) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=309 DEFAULT CHARSET=latin1;

INSERT INTO forum_forums_permission VALUES("1","1","1","1","0","0","0","0","0");
INSERT INTO forum_forums_permission VALUES("2","1","2","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("3","1","3","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("4","1","4","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("8","3","2","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("46","4","5","1","1","1","1","0","1");
INSERT INTO forum_forums_permission VALUES("38","3","3","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("11","3","4","1","1","1","1","0","1");
INSERT INTO forum_forums_permission VALUES("16","4","7","1","1","1","1","0","1");
INSERT INTO forum_forums_permission VALUES("17","4","2","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("45","4","3","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("19","4","4","1","1","1","1","0","1");
INSERT INTO forum_forums_permission VALUES("20","5","2","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("43","5","3","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("22","5","7","1","1","1","1","0","1");
INSERT INTO forum_forums_permission VALUES("23","5","4","1","1","1","1","0","1");
INSERT INTO forum_forums_permission VALUES("24","6","2","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("25","6","3","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("37","3","5","1","1","1","1","0","1");
INSERT INTO forum_forums_permission VALUES("42","5","5","1","1","1","1","0","1");
INSERT INTO forum_forums_permission VALUES("41","6","5","1","1","1","1","0","1");
INSERT INTO forum_forums_permission VALUES("30","6","4","1","1","1","1","0","1");
INSERT INTO forum_forums_permission VALUES("47","7","3","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("32","7","2","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("40","8","5","1","1","1","1","0","1");
INSERT INTO forum_forums_permission VALUES("34","8","2","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("35","8","3","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("36","8","4","1","1","1","1","0","1");
INSERT INTO forum_forums_permission VALUES("48","9","2","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("49","9","3","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("50","9","4","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("51","9","5","1","1","1","1","0","1");
INSERT INTO forum_forums_permission VALUES("52","10","1","1","0","0","0","0","0");
INSERT INTO forum_forums_permission VALUES("53","10","3","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("54","10","2","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("55","10","4","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("56","10","5","1","1","1","0","0","1");
INSERT INTO forum_forums_permission VALUES("57","10","6","1","1","1","0","0","0");
INSERT INTO forum_forums_permission VALUES("58","11","2","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("59","11","3","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("60","11","4","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("61","11","1","1","0","0","0","0","0");
INSERT INTO forum_forums_permission VALUES("64","11","6","1","1","1","0","0","0");
INSERT INTO forum_forums_permission VALUES("63","11","5","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("65","1","5","1","0","1","0","0","1");
INSERT INTO forum_forums_permission VALUES("66","1","6","1","0","1","0","0","0");
INSERT INTO forum_forums_permission VALUES("67","9","6","1","0","1","0","0","0");
INSERT INTO forum_forums_permission VALUES("68","9","1","1","0","0","0","0","0");
INSERT INTO forum_forums_permission VALUES("69","12","2","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("70","12","3","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("71","12","4","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("72","12","5","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("73","12","6","1","1","1","0","0","1");
INSERT INTO forum_forums_permission VALUES("74","12","1","1","0","0","0","0","0");
INSERT INTO forum_forums_permission VALUES("75","13","2","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("76","13","3","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("77","13","4","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("78","13","5","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("79","13","6","1","1","1","1","0","1");
INSERT INTO forum_forums_permission VALUES("80","13","1","1","0","0","0","0","0");
INSERT INTO forum_forums_permission VALUES("82","14","2","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("83","14","3","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("84","14","4","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("85","14","5","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("86","14","6","1","1","1","1","0","1");
INSERT INTO forum_forums_permission VALUES("87","14","1","1","0","0","0","0","0");
INSERT INTO forum_forums_permission VALUES("88","15","2","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("89","15","3","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("90","15","4","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("91","15","5","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("92","15","6","1","1","1","1","0","1");
INSERT INTO forum_forums_permission VALUES("93","15","1","1","0","0","0","0","0");
INSERT INTO forum_forums_permission VALUES("94","16","1","1","0","0","0","0","0");
INSERT INTO forum_forums_permission VALUES("95","16","2","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("96","16","3","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("97","16","4","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("98","16","5","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("99","16","6","1","1","1","1","0","1");
INSERT INTO forum_forums_permission VALUES("100","17","1","1","0","0","0","0","0");
INSERT INTO forum_forums_permission VALUES("101","17","2","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("102","17","3","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("103","17","4","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("104","17","5","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("105","17","6","1","1","1","1","0","1");
INSERT INTO forum_forums_permission VALUES("106","18","2","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("107","18","3","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("108","18","4","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("109","18","5","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("110","18","6","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("111","18","1","1","0","0","0","0","0");
INSERT INTO forum_forums_permission VALUES("112","19","2","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("113","19","3","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("114","19","4","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("115","19","5","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("116","19","6","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("117","19","1","1","0","0","0","0","0");
INSERT INTO forum_forums_permission VALUES("118","20","2","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("119","20","3","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("120","20","4","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("121","20","5","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("122","20","6","1","0","1","1","0","1");
INSERT INTO forum_forums_permission VALUES("124","22","1","1","0","0","0","0","0");
INSERT INTO forum_forums_permission VALUES("125","22","2","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("126","22","3","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("127","22","4","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("128","22","5","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("129","22","6","1","1","1","1","0","1");
INSERT INTO forum_forums_permission VALUES("130","23","1","1","0","0","0","0","0");
INSERT INTO forum_forums_permission VALUES("131","23","2","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("132","23","3","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("133","23","4","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("134","23","5","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("135","23","6","1","1","1","1","0","1");
INSERT INTO forum_forums_permission VALUES("136","21","2","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("137","21","1","1","0","0","0","0","0");
INSERT INTO forum_forums_permission VALUES("138","21","3","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("139","21","4","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("140","21","5","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("141","21","6","1","1","1","1","0","1");
INSERT INTO forum_forums_permission VALUES("142","24","2","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("143","24","3","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("144","24","4","1","1","1","1","0","1");
INSERT INTO forum_forums_permission VALUES("145","24","5","1","1","1","1","0","1");
INSERT INTO forum_forums_permission VALUES("146","27","2","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("147","27","3","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("151","26","3","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("150","26","2","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("154","25","2","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("153","25","3","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("155","28","1","1","0","0","0","0","0");
INSERT INTO forum_forums_permission VALUES("156","28","2","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("157","28","3","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("158","28","4","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("159","28","5","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("160","28","6","1","1","1","1","0","1");
INSERT INTO forum_forums_permission VALUES("161","29","1","1","0","0","0","0","0");
INSERT INTO forum_forums_permission VALUES("162","29","2","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("163","29","3","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("164","29","4","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("165","29","5","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("166","29","6","1","1","1","1","0","1");
INSERT INTO forum_forums_permission VALUES("167","30","1","1","0","0","0","0","0");
INSERT INTO forum_forums_permission VALUES("168","30","2","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("169","30","3","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("170","30","4","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("171","30","5","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("172","30","6","1","1","1","1","0","1");
INSERT INTO forum_forums_permission VALUES("173","31","2","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("174","31","1","1","0","0","0","0","0");
INSERT INTO forum_forums_permission VALUES("175","31","3","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("176","31","4","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("177","31","5","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("178","31","6","1","1","1","1","0","1");
INSERT INTO forum_forums_permission VALUES("179","32","1","1","0","0","0","0","0");
INSERT INTO forum_forums_permission VALUES("180","32","2","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("181","32","3","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("182","32","4","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("183","32","5","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("184","32","6","1","1","1","1","0","1");
INSERT INTO forum_forums_permission VALUES("185","33","1","1","0","0","0","0","0");
INSERT INTO forum_forums_permission VALUES("186","33","2","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("187","33","3","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("188","33","4","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("189","33","5","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("190","33","6","1","1","1","1","0","1");
INSERT INTO forum_forums_permission VALUES("191","34","1","1","0","0","0","0","0");
INSERT INTO forum_forums_permission VALUES("192","34","2","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("193","34","3","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("194","34","4","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("195","34","6","1","1","1","1","0","1");
INSERT INTO forum_forums_permission VALUES("196","34","5","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("197","35","2","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("198","35","1","1","0","0","0","0","0");
INSERT INTO forum_forums_permission VALUES("199","35","3","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("200","35","4","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("201","35","5","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("202","35","6","1","1","1","1","0","1");
INSERT INTO forum_forums_permission VALUES("203","37","1","1","0","0","0","0","0");
INSERT INTO forum_forums_permission VALUES("204","37","2","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("205","37","3","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("206","37","4","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("207","37","5","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("208","37","6","1","1","1","1","0","1");
INSERT INTO forum_forums_permission VALUES("209","38","1","1","0","0","0","0","0");
INSERT INTO forum_forums_permission VALUES("210","38","2","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("211","38","3","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("212","38","4","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("213","38","5","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("214","38","6","1","1","1","1","0","1");
INSERT INTO forum_forums_permission VALUES("215","39","1","1","0","0","0","0","0");
INSERT INTO forum_forums_permission VALUES("217","39","6","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("218","39","3","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("219","39","4","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("221","39","2","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("222","39","5","1","1","1","1","0","1");
INSERT INTO forum_forums_permission VALUES("223","40","1","1","0","0","0","0","0");
INSERT INTO forum_forums_permission VALUES("224","40","3","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("225","40","2","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("226","40","5","1","1","1","1","0","1");
INSERT INTO forum_forums_permission VALUES("227","40","6","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("228","40","4","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("229","41","1","1","0","0","0","0","0");
INSERT INTO forum_forums_permission VALUES("230","41","2","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("231","41","3","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("232","41","4","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("233","41","5","1","1","1","1","0","1");
INSERT INTO forum_forums_permission VALUES("234","41","6","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("235","42","1","1","0","0","0","0","0");
INSERT INTO forum_forums_permission VALUES("236","42","2","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("237","42","3","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("238","42","4","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("239","42","5","1","1","1","1","0","1");
INSERT INTO forum_forums_permission VALUES("240","42","6","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("241","43","1","1","0","0","0","0","0");
INSERT INTO forum_forums_permission VALUES("242","43","2","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("243","43","3","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("244","43","4","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("245","43","5","1","1","1","1","0","1");
INSERT INTO forum_forums_permission VALUES("246","44","1","1","0","0","0","0","0");
INSERT INTO forum_forums_permission VALUES("247","44","2","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("248","44","4","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("249","44","5","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("250","44","6","1","1","1","1","0","1");
INSERT INTO forum_forums_permission VALUES("251","44","3","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("252","45","1","1","0","0","0","0","0");
INSERT INTO forum_forums_permission VALUES("253","45","2","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("254","45","3","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("255","45","4","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("256","45","5","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("257","45","6","1","1","1","1","0","1");
INSERT INTO forum_forums_permission VALUES("258","46","1","1","0","0","0","0","0");
INSERT INTO forum_forums_permission VALUES("259","46","2","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("260","46","3","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("261","46","4","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("262","46","5","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("263","46","6","1","1","1","1","0","1");
INSERT INTO forum_forums_permission VALUES("264","47","1","1","0","0","0","0","0");
INSERT INTO forum_forums_permission VALUES("265","47","2","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("266","47","3","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("267","47","4","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("268","47","5","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("269","47","6","1","1","1","1","0","1");
INSERT INTO forum_forums_permission VALUES("270","48","1","1","0","0","0","0","0");
INSERT INTO forum_forums_permission VALUES("271","48","2","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("272","48","4","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("273","48","5","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("274","48","6","1","1","1","1","0","1");
INSERT INTO forum_forums_permission VALUES("275","48","3","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("276","49","1","1","0","0","0","0","0");
INSERT INTO forum_forums_permission VALUES("277","49","2","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("278","49","4","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("279","49","3","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("280","49","5","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("281","49","6","1","1","1","1","0","1");
INSERT INTO forum_forums_permission VALUES("282","50","1","1","0","0","0","0","0");
INSERT INTO forum_forums_permission VALUES("283","50","2","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("284","50","3","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("285","50","4","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("286","50","5","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("287","50","6","1","1","1","1","0","1");
INSERT INTO forum_forums_permission VALUES("288","52","1","1","0","0","0","0","0");
INSERT INTO forum_forums_permission VALUES("289","52","2","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("290","52","3","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("291","52","4","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("292","52","5","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("293","52","6","1","1","1","1","0","1");
INSERT INTO forum_forums_permission VALUES("294","53","1","1","0","0","0","0","0");
INSERT INTO forum_forums_permission VALUES("295","53","2","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("296","53","3","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("297","53","4","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("298","53","5","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("299","53","6","1","1","1","1","0","1");
INSERT INTO forum_forums_permission VALUES("300","54","2","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("301","54","1","1","0","0","0","0","0");
INSERT INTO forum_forums_permission VALUES("302","54","3","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("303","54","4","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("304","54","5","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("305","54","6","1","1","1","1","0","1");
INSERT INTO forum_forums_permission VALUES("306","55","2","1","1","1","1","1","1");
INSERT INTO forum_forums_permission VALUES("307","55","3","1","1","1","1","0","1");



DROP TABLE forum_forums_read;

CREATE TABLE `forum_forums_read` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `account_id` int(100) NOT NULL,
  `topic_id` int(100) NOT NULL,
  `date` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=220 DEFAULT CHARSET=latin1;

INSERT INTO forum_forums_read VALUES("164","1","50","1367695394");
INSERT INTO forum_forums_read VALUES("165","2","50","1367207728");
INSERT INTO forum_forums_read VALUES("166","2","27","1366922296");
INSERT INTO forum_forums_read VALUES("170","6","50","1367621896");
INSERT INTO forum_forums_read VALUES("190","3","57","1367664941");
INSERT INTO forum_forums_read VALUES("175","2","28","1367011582");
INSERT INTO forum_forums_read VALUES("189","3","50","1367664940");
INSERT INTO forum_forums_read VALUES("179","4","50","1367052360");
INSERT INTO forum_forums_read VALUES("180","4","28","1367745980");
INSERT INTO forum_forums_read VALUES("182","2","49","1366907153");
INSERT INTO forum_forums_read VALUES("183","2","57","1367694800");
INSERT INTO forum_forums_read VALUES("184","1","57","1367776070");
INSERT INTO forum_forums_read VALUES("186","0","28","1366920790");
INSERT INTO forum_forums_read VALUES("191","4","57","1367746128");
INSERT INTO forum_forums_read VALUES("192","0","50","1367011937");
INSERT INTO forum_forums_read VALUES("193","1","27","1367623652");
INSERT INTO forum_forums_read VALUES("197","1","49","1367694658");
INSERT INTO forum_forums_read VALUES("200","1","28","1367791509");
INSERT INTO forum_forums_read VALUES("205","5","57","1367526837");
INSERT INTO forum_forums_read VALUES("206","5","28","1367526915");
INSERT INTO forum_forums_read VALUES("207","6","57","1367656861");
INSERT INTO forum_forums_read VALUES("208","6","28","1367621859");
INSERT INTO forum_forums_read VALUES("209","6","49","1367621942");
INSERT INTO forum_forums_read VALUES("214","2","63","1367694790");
INSERT INTO forum_forums_read VALUES("213","1","63","1367748087");
INSERT INTO forum_forums_read VALUES("215","6","63","1367656750");
INSERT INTO forum_forums_read VALUES("216","3","63","1367665412");
INSERT INTO forum_forums_read VALUES("217","4","63","1367745859");
INSERT INTO forum_forums_read VALUES("218","1","64","1367791231");
INSERT INTO forum_forums_read VALUES("219","1","1","1367791391");



DROP TABLE forum_friends;

CREATE TABLE `forum_friends` (
  `id` int(200) NOT NULL AUTO_INCREMENT,
  `account_id` int(200) NOT NULL,
  `friend_id` int(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;

INSERT INTO forum_friends VALUES("18","6","1");
INSERT INTO forum_friends VALUES("15","1","6");
INSERT INTO forum_friends VALUES("11","1","3");
INSERT INTO forum_friends VALUES("36","1","2");
INSERT INTO forum_friends VALUES("32","3","1");
INSERT INTO forum_friends VALUES("41","1","5");
INSERT INTO forum_friends VALUES("22","1","4");
INSERT INTO forum_friends VALUES("43","2","3");
INSERT INTO forum_friends VALUES("25","2","6");
INSERT INTO forum_friends VALUES("27","2","4");
INSERT INTO forum_friends VALUES("31","2","2");
INSERT INTO forum_friends VALUES("35","2","1");
INSERT INTO forum_friends VALUES("42","5","2");
INSERT INTO forum_friends VALUES("45","2","0");
INSERT INTO forum_friends VALUES("46","3","2");



DROP TABLE forum_groups;

CREATE TABLE `forum_groups` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `colour` varchar(100) NOT NULL,
  `info` text NOT NULL,
  `rc` int(10) NOT NULL DEFAULT '0',
  `acp` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

INSERT INTO forum_groups VALUES("1","Guest","","Need to become pirates","0","0");
INSERT INTO forum_groups VALUES("2","Webmaster","alexfont","Fleet Admiral","1","1");
INSERT INTO forum_groups VALUES("3","Administrator","adminfont","Admiral","1","1");
INSERT INTO forum_groups VALUES("4","Global Moderator","gmodfont","Commander","1","0");
INSERT INTO forum_groups VALUES("5","Moderator","modfont","Marines","0","0");
INSERT INTO forum_groups VALUES("6","Members","userfont","Pirates","0","0");



DROP TABLE forum_groups_members;

CREATE TABLE `forum_groups_members` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `account_id` int(100) NOT NULL,
  `group_id` int(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

INSERT INTO forum_groups_members VALUES("1","-1","1");
INSERT INTO forum_groups_members VALUES("2","1","2");
INSERT INTO forum_groups_members VALUES("3","2","2");
INSERT INTO forum_groups_members VALUES("4","3","3");
INSERT INTO forum_groups_members VALUES("5","4","3");
INSERT INTO forum_groups_members VALUES("6","5","5");
INSERT INTO forum_groups_members VALUES("7","6","3");
INSERT INTO forum_groups_members VALUES("11","10","6");
INSERT INTO forum_groups_members VALUES("12","8","6");



DROP TABLE forum_late_topic;

CREATE TABLE `forum_late_topic` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `topic_id` int(100) NOT NULL,
  `title` varchar(100) NOT NULL,
  `author_id` int(100) NOT NULL,
  `date` varchar(100) NOT NULL,
  `forum_id` int(10) NOT NULL,
  `url` varchar(255) NOT NULL,
  `post_id` int(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=405 DEFAULT CHARSET=latin1;

INSERT INTO forum_late_topic VALUES("40","49","Move Characters/Tasks?","6","1366338844","8","","0");
INSERT INTO forum_late_topic VALUES("38","28","Powers","6","1366337941","7","","0");
INSERT INTO forum_late_topic VALUES("39","49","Move Characters/Tasks?","6","1366338442","8","","0");
INSERT INTO forum_late_topic VALUES("37","28","Powers","2","1366239676","7","","0");
INSERT INTO forum_late_topic VALUES("41","49","Move Characters/Tasks?","6","1366340346","8","","0");
INSERT INTO forum_late_topic VALUES("42","50","Ideas ","6","1366340428","3","","0");
INSERT INTO forum_late_topic VALUES("43","49","Move Characters/Tasks?","6","1366344381","8","","0");
INSERT INTO forum_late_topic VALUES("44","49","Move Characters/Tasks?","6","1366345842","8","","0");
INSERT INTO forum_late_topic VALUES("45","27","Who should have access to the forums?","2","1366347575","7","","0");
INSERT INTO forum_late_topic VALUES("46","50","Ideas ","1","1366373192","3","","0");
INSERT INTO forum_late_topic VALUES("47","50","Ideas ","6","1366423011","3","","0");
INSERT INTO forum_late_topic VALUES("49","50","Ideas ","2","1366516867","3","","0");
INSERT INTO forum_late_topic VALUES("52","50","Ideas ","3","1366517290","3","","0");
INSERT INTO forum_late_topic VALUES("53","50","Ideas ","1","1366517335","3","","0");
INSERT INTO forum_late_topic VALUES("55","50","Ideas ","6","1366517383","3","","0");
INSERT INTO forum_late_topic VALUES("57","50","Ideas ","1","1366517490","3","","0");
INSERT INTO forum_late_topic VALUES("58","50","Ideas ","2","1366517502","3","","0");
INSERT INTO forum_late_topic VALUES("59","50","Ideas ","1","1366517548","3","","0");
INSERT INTO forum_late_topic VALUES("62","50","Ideas ","6","1366518984","3","","0");
INSERT INTO forum_late_topic VALUES("63","50","Ideas ","1","1366519063","3","","0");
INSERT INTO forum_late_topic VALUES("65","50","Ideas ","2","1366519395","3","","0");
INSERT INTO forum_late_topic VALUES("66","50","Ideas ","1","1366519453","3","","0");
INSERT INTO forum_late_topic VALUES("67","50","Ideas ","6","1366519717","3","","0");
INSERT INTO forum_late_topic VALUES("68","50","Ideas ","1","1366519984","3","","0");
INSERT INTO forum_late_topic VALUES("69","50","Ideas ","2","1366520709","3","","0");
INSERT INTO forum_late_topic VALUES("70","50","Ideas ","6","1366522165","3","","0");
INSERT INTO forum_late_topic VALUES("84","28","Powers","2","1366601865","7","","0");
INSERT INTO forum_late_topic VALUES("86","28","Powers","1","1366602590","7","","0");
INSERT INTO forum_late_topic VALUES("87","28","Powers","2","1366603202","7","","0");
INSERT INTO forum_late_topic VALUES("88","28","Powers","1","1366603829","7","","0");
INSERT INTO forum_late_topic VALUES("89","28","Powers","1","1366604165","7","","0");
INSERT INTO forum_late_topic VALUES("90","28","Powers","2","1366604608","7","","0");
INSERT INTO forum_late_topic VALUES("91","28","Powers","1","1366604688","7","","0");
INSERT INTO forum_late_topic VALUES("92","28","Powers","2","1366605011","7","","0");
INSERT INTO forum_late_topic VALUES("93","28","Powers","1","1366605049","7","","0");
INSERT INTO forum_late_topic VALUES("94","28","Powers","2","1366605445","7","","0");
INSERT INTO forum_late_topic VALUES("95","28","Powers","1","1366605464","7","","0");
INSERT INTO forum_late_topic VALUES("97","49","Move Characters/Tasks?","2","1366610599","8","","0");
INSERT INTO forum_late_topic VALUES("102","28","Powers","6","1366613305","7","","0");
INSERT INTO forum_late_topic VALUES("104","49","Move Characters/Tasks?","6","1366613553","8","","0");
INSERT INTO forum_late_topic VALUES("107","28","Powers","2","1366614305","7","","0");
INSERT INTO forum_late_topic VALUES("108","49","Move Characters/Tasks?","2","1366614405","8","","0");
INSERT INTO forum_late_topic VALUES("112","27","Who should have access to the forums?","4","1366654089","7","","0");
INSERT INTO forum_late_topic VALUES("113","28","Powers","4","1366654523","7","","0");
INSERT INTO forum_late_topic VALUES("117","28","Powers","2","1366656891","7","","0");
INSERT INTO forum_late_topic VALUES("119","50","Ideas ","2","1366657107","3","","0");
INSERT INTO forum_late_topic VALUES("120","28","Powers","4","1366661023","7","","0");
INSERT INTO forum_late_topic VALUES("123","28","Powers","1","1366661403","7","","0");
INSERT INTO forum_late_topic VALUES("124","28","Powers","2","1366661628","7","","0");
INSERT INTO forum_late_topic VALUES("128","28","Powers","1","1366663326","7","","0");
INSERT INTO forum_late_topic VALUES("130","28","Powers","2","1366663415","7","","0");
INSERT INTO forum_late_topic VALUES("131","28","Powers","1","1366664094","7","","0");
INSERT INTO forum_late_topic VALUES("132","28","Powers","1","1366664299","7","","0");
INSERT INTO forum_late_topic VALUES("133","28","Powers","2","1366664367","7","","0");
INSERT INTO forum_late_topic VALUES("134","28","Powers","1","1366664484","7","","0");
INSERT INTO forum_late_topic VALUES("135","50","Ideas ","1","1366665061","3","","0");
INSERT INTO forum_late_topic VALUES("136","50","Ideas ","2","1366665381","3","","0");
INSERT INTO forum_late_topic VALUES("137","50","Ideas ","1","1366666236","3","","0");
INSERT INTO forum_late_topic VALUES("138","50","Ideas ","2","1366666796","3","","0");
INSERT INTO forum_late_topic VALUES("139","50","Ideas ","4","1366669014","3","","0");
INSERT INTO forum_late_topic VALUES("140","50","Ideas ","1","1366673650","3","","0");
INSERT INTO forum_late_topic VALUES("141","50","Ideas ","1","1366689917","3","","0");
INSERT INTO forum_late_topic VALUES("142","50","Ideas ","1","1366690416","3","/?area=forum&s=topic&t=50&page=3","0");
INSERT INTO forum_late_topic VALUES("143","50","Ideas ","2","1366690459","3","/?area=forum&s=topic&t=50&page=3","0");
INSERT INTO forum_late_topic VALUES("144","50","Ideas ","1","1366690663","3","/?area=forum&s=topic&t=50&page=3","0");
INSERT INTO forum_late_topic VALUES("145","50","Ideas ","2","1366690961","3","/?area=forum&s=topic&t=50&page=3","0");
INSERT INTO forum_late_topic VALUES("146","50","Ideas ","1","1366691048","3","/?area=forum&s=topic&t=50&page=3","0");
INSERT INTO forum_late_topic VALUES("149","50","Ideas ","2","1366691595","3","/?area=forum&s=topic&t=50&page=3","0");
INSERT INTO forum_late_topic VALUES("150","50","Ideas ","1","1366691672","3","/?area=forum&s=topic&t=50&page=3","0");
INSERT INTO forum_late_topic VALUES("151","50","Ideas ","2","1366691716","3","/?area=forum&s=topic&t=50&page=3","0");
INSERT INTO forum_late_topic VALUES("154","50","Ideas ","1","1366691975","3","/?area=forum&s=topic&t=50&page=3","0");
INSERT INTO forum_late_topic VALUES("155","50","Ideas ","2","1366692378","3","/?area=forum&s=topic&t=50&page=4","0");
INSERT INTO forum_late_topic VALUES("156","50","Ideas ","1","1366692459","3","/?area=forum&s=topic&t=50&page=4","0");
INSERT INTO forum_late_topic VALUES("157","50","Ideas ","2","1366692509","3","/?area=forum&s=topic&t=50&page=4","0");
INSERT INTO forum_late_topic VALUES("158","50","Ideas ","1","1366692561","3","/?area=forum&s=topic&t=50&page=4","0");
INSERT INTO forum_late_topic VALUES("159","50","Ideas ","2","1366692641","3","/?area=forum&s=topic&t=50&page=4","0");
INSERT INTO forum_late_topic VALUES("163","50","Ideas ","1","1366732838","3","/?area=forum&s=topic&t=50&page=4","0");
INSERT INTO forum_late_topic VALUES("165","28","Powers","6","1366740970","7","/?area=forum&s=topic&t=28&page=6","0");
INSERT INTO forum_late_topic VALUES("167","50","Ideas ","6","1366741083","3","/?area=forum&s=topic&t=50&page=4","0");
INSERT INTO forum_late_topic VALUES("168","50","Ideas ","2","1366742779","3","/?area=forum&s=topic&t=50&page=4","0");
INSERT INTO forum_late_topic VALUES("169","28","Powers","2","1366742944","7","/?area=forum&s=topic&t=28&page=6","0");
INSERT INTO forum_late_topic VALUES("176","50","Ideas ","1","1366753853","3","/?area=forum&s=topic&t=50&page=4","0");
INSERT INTO forum_late_topic VALUES("177","28","Powers","1","1366753921","7","/?area=forum&s=topic&t=28&page=6","0");
INSERT INTO forum_late_topic VALUES("179","50","Ideas ","2","1366755372","3","/?area=forum&s=topic&t=50&page=4","0");
INSERT INTO forum_late_topic VALUES("180","28","Powers","2","1366755450","7","/?area=forum&s=topic&t=28&page=6","0");
INSERT INTO forum_late_topic VALUES("181","28","Powers","1","1366764770","7","/?area=forum&s=topic&t=28&page=6","0");
INSERT INTO forum_late_topic VALUES("194","27","Who should have access to the forums?","4","1366825939","7","?area=forum&amp;s=topic&amp;t=27&page=3","0");
INSERT INTO forum_late_topic VALUES("196","27","Who should have access to the forums?","1","1366837493","7","","0");
INSERT INTO forum_late_topic VALUES("199","27","Who should have access to the forums?","6","1366842271","7","?area=forum&amp;s=topic&amp;t=27&page=3","0");
INSERT INTO forum_late_topic VALUES("201","49","Move Characters/Tasks?","6","1366842395","8","","0");
INSERT INTO forum_late_topic VALUES("204","49","Move Characters/Tasks?","1","1366842509","8","?area=forum&amp;s=topic&amp;t=49&page=2","0");
INSERT INTO forum_late_topic VALUES("206","27","Who should have access to the forums?","1","1366842636","7","?area=forum&amp;s=topic&amp;t=27&page=3","0");
INSERT INTO forum_late_topic VALUES("207","28","Powers","1","1366845325","7","?area=forum&amp;s=topic&amp;t=28&page=6","596");
INSERT INTO forum_late_topic VALUES("219","50","Ideas ","1","1366847514","3","?area=forum&amp;s=topic&amp;t=50&page=4","608");
INSERT INTO forum_late_topic VALUES("233","50","Ideas ","1","1366854347","3","?area=forum&amp;s=topic&amp;t=50&page=4","622");
INSERT INTO forum_late_topic VALUES("230","50","Ideas ","6","1366853014","3","","0");
INSERT INTO forum_late_topic VALUES("236","50","Ideas ","6","1366855558","3","","0");
INSERT INTO forum_late_topic VALUES("279","50","Ideas ","2","1366867472","3","?area=forum&amp;s=topic&amp;t=50&page=5","669");
INSERT INTO forum_late_topic VALUES("276","50","Ideas ","6","1366866701","3","?area=forum&amp;s=topic&amp;t=50&page=4","666");
INSERT INTO forum_late_topic VALUES("282","57","Offical thread","2","1366907298","8","?area=forum&s=topic&t=57&page=1","673");
INSERT INTO forum_late_topic VALUES("283","57","Offical thread","2","1366907320","8","?area=forum&s=topic&t=57&page=1","674");
INSERT INTO forum_late_topic VALUES("284","57","Offical thread","2","1366907354","8","?area=forum&s=topic&t=57&page=1","675");
INSERT INTO forum_late_topic VALUES("285","57","Offical thread","1","1366920149","8","?area=forum&s=topic&t=57&page=1","676");
INSERT INTO forum_late_topic VALUES("326","50","Ideas ","1","1366929070","3","?area=forum&s=topic&t=50&page=5","719");
INSERT INTO forum_late_topic VALUES("292","57","Offical thread","2","1366922158","8","?area=forum&s=topic&t=57&page=1","683");
INSERT INTO forum_late_topic VALUES("325","57","Offical thread","1","1366928983","8","?area=forum&s=topic&t=57&page=1","718");
INSERT INTO forum_late_topic VALUES("347","50","Ideas ","4","1367020882","3","?area=forum&s=topic&t=50&page=5","740");
INSERT INTO forum_late_topic VALUES("346","50","Ideas ","1","1367011024","3","?area=forum&s=topic&t=50&page=5","739");
INSERT INTO forum_late_topic VALUES("348","50","Ideas ","1","1367020939","3","?area=forum&s=topic&t=50&page=5","741");
INSERT INTO forum_late_topic VALUES("394","57","Offical thread","6","1367621844","8","?area=forum&s=topic&t=57&page=1","794");
INSERT INTO forum_late_topic VALUES("395","57","Offical thread","1","1367621927","8","?area=forum&s=topic&t=57&page=1","795");
INSERT INTO forum_late_topic VALUES("396","63","Books","6","1367656749","6","?area=forum&s=topic&t=63&page=1","798");
INSERT INTO forum_late_topic VALUES("397","57","Offical thread","6","1367656861","8","","0");
INSERT INTO forum_late_topic VALUES("398","63","Books","3","1367665411","6","?area=forum&s=topic&t=63&page=1","800");
INSERT INTO forum_late_topic VALUES("399","63","Books","1","1367687078","6","","0");
INSERT INTO forum_late_topic VALUES("400","57","Offical thread","1","1367687176","8","?area=forum&s=topic&t=57&page=1","802");
INSERT INTO forum_late_topic VALUES("401","63","Books","4","1367745852","6","?area=forum&s=topic&t=63&page=1","803");
INSERT INTO forum_late_topic VALUES("402","57","Offical thread","4","1367746126","8","?area=forum&s=topic&t=57&page=1","804");
INSERT INTO forum_late_topic VALUES("403","63","Books","1","1367748079","6","?area=forum&s=topic&t=63&page=1","805");
INSERT INTO forum_late_topic VALUES("404","57","Offical thread","1","1367748178","8","?area=forum&s=topic&t=57&page=1","806");



DROP TABLE forum_mail;

CREATE TABLE `forum_mail` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `from_id` int(100) NOT NULL,
  `to_id` int(100) NOT NULL,
  `date` varchar(100) NOT NULL,
  `marked` int(100) NOT NULL,
  `text` text NOT NULL,
  `title` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=136 DEFAULT CHARSET=latin1;

INSERT INTO forum_mail VALUES("41","1","4","1364777067","1","Hey can you switch your template to Nelo and tell me if you like it","Template");
INSERT INTO forum_mail VALUES("2","1","1","1363825799","1","A topic you are watching has been replied to \n [url=http://forum.pirate-zone.com//?area=forums=topic&amp;t=25]http://forum.pirate-zone.com//?area=forums=topic&amp;t=25[/url]","Topic Notification");
INSERT INTO forum_mail VALUES("3","1","4","1364001942","1","[url=http://forum.pirate-zone.com/acp/]Click, type in your user and pass.[/url]","ACP");
INSERT INTO forum_mail VALUES("4","4","1","1364002106","1","I did that already, and I just get a screen showing this.\n\nAdmin Panel\nForum Version: Official Forum of Pirate-Zone \nPHP Version: 5.4.12 \nMySQL Version: 5.1.67-log\n\nNothing else.","ACP");
INSERT INTO forum_mail VALUES("5","1","4","1364002161","1","yeah, it\'s at the top. all the links","ACP");
INSERT INTO forum_mail VALUES("6","4","1","1364002256","1","Oh I see now. Thanks for the help.","ACP");
INSERT INTO forum_mail VALUES("7","1","4","1364002266","1","Np","ACP");
INSERT INTO forum_mail VALUES("8","4","1","1364002385","1","Just so you know, this forum is looking great. Although not everything is fully functional yet, it seems like it\'s almost ready for us to move here.","ACP");
INSERT INTO forum_mail VALUES("9","1","4","1364002515","1","Yeah, I gotta finish redoing the link style, since i changed the links and haven\'t completely changed the links, I\'ma get help from Melo with that. About to start on this new template he is making, then ima start on yours.","ACP");
INSERT INTO forum_mail VALUES("10","1","4","1364002588","1","I also need to redo the admin panel for the appropriate templates.","ACP");
INSERT INTO forum_mail VALUES("11","4","1","1364002600","1","Ok, sounds good. Just wanted you to know that you\'ve done a great job so far ^_^.","ACP");
INSERT INTO forum_mail VALUES("12","1","4","1364002714","1","Thanks, appreciate it.","ACP");
INSERT INTO forum_mail VALUES("13","1","4","1364003222","1","A topic you are watching has been replied to \n [url=http://forum.pirate-zone.com//?area=forums=topic&amp;t=26]http://forum.pirate-zone.com//?area=forums=topic&amp;t=26[/url]","Topic Notification");
INSERT INTO forum_mail VALUES("14","1","4","1364004447","1","Stole my glow I see (dhat)","ACP");
INSERT INTO forum_mail VALUES("15","4","1","1364004533","1","Yp. :D\n\nIs it possible to get different ones? I didn\'t want to steal your one but it seas the only other one I knew.","ACP");
INSERT INTO forum_mail VALUES("16","1","4","1364004589","1","Yeah, Send me the colors you want for outside and inside and I will make it.","ACP");
INSERT INTO forum_mail VALUES("17","4","1","1364040118","1","Inside Black, outside Royal Blue.\nPlease.","ACP");
INSERT INTO forum_mail VALUES("18","1","4","1364065106","1","alright when I have time I\'ll add it in.","ACP");
INSERT INTO forum_mail VALUES("19","1","4","1364066981","1","Added.","ACP");
INSERT INTO forum_mail VALUES("20","4","1","1364067837","1","Thanks. Looks great.","ACP");
INSERT INTO forum_mail VALUES("21","1","4","1364068030","1","Np","ACP");
INSERT INTO forum_mail VALUES("22","4","1","1364082371","1","I tried to change my email, and it reset both my avy and my password. Had to get links to set a new pword for me.","Major Bug");
INSERT INTO forum_mail VALUES("23","1","4","1364085636","1","Did you do it through the acp?\n","Major Bug");
INSERT INTO forum_mail VALUES("24","1","2","1364094017","1","Did you tell Ocean to pm me?","Hey");
INSERT INTO forum_mail VALUES("25","2","1","1364094379","1","No I didn\'t?","Hey");
INSERT INTO forum_mail VALUES("26","2","1","1364094622","1","Why? I\'m curious.","Hey");
INSERT INTO forum_mail VALUES("27","1","2","1364095167","1"," (SMIRK)","Hey");
INSERT INTO forum_mail VALUES("28","2","1","1364095304","1","Lol [laugh]","Hey");
INSERT INTO forum_mail VALUES("29","4","1","1364113805","1","No, through the ucp. And I checked again, it reset my whole account. Post count, signup date, last activity. Everything except my email.","Major Bug");
INSERT INTO forum_mail VALUES("30","1","4","1364139957","1","It works for me. Are you pressing enter or submit","Major Bug");
INSERT INTO forum_mail VALUES("31","1","4","1364140180","1","I see what your saying. Lemme edit it","Major Bug");
INSERT INTO forum_mail VALUES("32","1","4","1364141023","1","Its keeps all of your stuff. The default ucp page it doesnt keep. Everything works. It just doesnt show on the def ucp. Ill fix it when i come home. Check on your profile cause that is the real stuff.","Major Bug");
INSERT INTO forum_mail VALUES("33","4","1","1364150540","1","I tried both.","Major Bug");
INSERT INTO forum_mail VALUES("34","1","4","1364151267","1","All your stuff is the same. Nothing has changed\n","Major Bug");
INSERT INTO forum_mail VALUES("35","1","1","1364615484","1","A topic you are watching has been replied to \n [url=http://forum.pirate-zone.com//?area=forums=topic&amp;t=25]http://forum.pirate-zone.com//?area=forums=topic&amp;t=25[/url]","Topic Notification");
INSERT INTO forum_mail VALUES("36","1","1","1364615975","1","A topic you are watching has been replied to \n [url=http://forum.pirate-zone.com//?area=forums=topic&amp;t=25]http://forum.pirate-zone.com//?area=forums=topic&amp;t=25[/url]","Topic Notification");
INSERT INTO forum_mail VALUES("37","1","1","1364615983","1","A topic you are watching has been replied to \n [url=http://forum.pirate-zone.com//?area=forums=topic&amp;t=25]http://forum.pirate-zone.com//?area=forums=topic&amp;t=25[/url]","Topic Notification");
INSERT INTO forum_mail VALUES("38","1","1","1364678544","1","A topic you are watching has been replied to \n [url=http://forum.pirate-zone.com//?area=forums=topic&amp;t=25]http://forum.pirate-zone.com//?area=forums=topic&amp;t=25[/url]","Topic Notification");
INSERT INTO forum_mail VALUES("39","1","1","1364684546","1","A topic you are watching has been replied to \n [url=http://forum.pirate-zone.com//?area=forums=topic&amp;t=25]http://forum.pirate-zone.com//?area=forums=topic&amp;t=25[/url]","Topic Notification");
INSERT INTO forum_mail VALUES("40","1","1","1364702667","1","A topic you are watching has been replied to \n [url=http://forum.pirate-zone.com//?area=forums=topic&amp;t=32]http://forum.pirate-zone.com//?area=forums=topic&amp;t=32[/url]","Topic Notification");
INSERT INTO forum_mail VALUES("42","4","1","1364807248","1","K, I\'ll try it now.","Template");
INSERT INTO forum_mail VALUES("43","4","1","1364807534","1","It\'s alright. I wouldn\'t choose it personally. But that\'s mainly just my personal preference.\n\nThe only real problem with the layout is the top of the page (where the announcement is). It looks very cluttered, and I\'d prefer to have a banner, like Luffy on the default one.","Template");
INSERT INTO forum_mail VALUES("44","1","1","1365387436","1","ddd","ddd");
INSERT INTO forum_mail VALUES("46","1","1","1365883907","1","A topic you are watching has been replied to \n [url=http://forum.pirate-zone.com//?area=forums=topic&amp;t=42]http://forum.pirate-zone.com//?area=forums=topic&amp;t=42[/url]","Topic Notification");
INSERT INTO forum_mail VALUES("47","1","1","1365912247","1","A topic you are watching has been replied to \n [url=http://forum.pirate-zone.com/?area=forums=topic&amp;t=42]http://forum.pirate-zone.com/?area=forums=topic&amp;t=42[/url]","Topic Notification");
INSERT INTO forum_mail VALUES("48","1","1","1365914945","1","A topic you are watching has been replied to \n [url=http://forum.pirate-zone.com/?area=forums=topic&amp;t=42]http://forum.pirate-zone.com/?area=forums=topic&amp;t=42[/url]","Topic Notification");
INSERT INTO forum_mail VALUES("49","1","1","1365914964","1","A topic you are watching has been replied to \n [url=http://forum.pirate-zone.com/?area=forums=topic&amp;t=42]http://forum.pirate-zone.com/?area=forums=topic&amp;t=42[/url]","Topic Notification");
INSERT INTO forum_mail VALUES("50","1","1","1365915740","1","A topic you are watching has been replied to \n [url=http://forum.pirate-zone.com/?area=forums=topic&amp;t=42]http://forum.pirate-zone.com/?area=forums=topic&amp;t=42[/url]","Topic Notification");
INSERT INTO forum_mail VALUES("51","1","1","1365916185","1","A topic you are watching has been replied to \n [url=http://forum.pirate-zone.com/?area=forums=topic&amp;t=42]http://forum.pirate-zone.com/?area=forums=topic&amp;t=42[/url]","Topic Notification");
INSERT INTO forum_mail VALUES("52","1","6","1366224529","1","You have to make you avatar into an icon please. I\'m about to disable avatars and all non \nPNG icons\n","Avatar");
INSERT INTO forum_mail VALUES("55","1","0","1366329544","0","Substantial has removed you from their friend list","A Lost Friend");
INSERT INTO forum_mail VALUES("56","1","0","1366329589","0","Substantial has added you to their friend list","A New Friend");
INSERT INTO forum_mail VALUES("57","1","0","1366329600","0","Substantial has added you to their friend list","A New Friend");
INSERT INTO forum_mail VALUES("58","1","0","1366329610","0","Substantial has added you to their friend list","A New Friend");
INSERT INTO forum_mail VALUES("59","1","0","1366329723","0","Substantial has removed you from their friend list","A Lost Friend");
INSERT INTO forum_mail VALUES("60","1","1","1366329771","1","<a href=\"http://forum.pirate-zone.com/?area=profile&amp;user=1\" class=\"malikfont\">Substantial</a> has removed you from their friend list","A Lost Friend");
INSERT INTO forum_mail VALUES("61","1","2","1366329813","1","<a href=\"http://forum.pirate-zone.com/?area=profile&amp;user=2\" class=\"linkfont\">Linksbrawler</a> has removed you from their friend list","A Lost Friend");
INSERT INTO forum_mail VALUES("62","1","0","1366329946","0","Substantial has added you to their friend list","A New Friend");
INSERT INTO forum_mail VALUES("64","1","0","1366330330","0","<a href=\"http://forum.pirate-zone.com/?area=profile&amp;user=0\" class=\"\">System</a> has removed you from their friend list","A Lost Friend");
INSERT INTO forum_mail VALUES("65","1","0","1366330469","0","Substantial has added you to their friend list","A New Friend");
INSERT INTO forum_mail VALUES("66","1","0","1366330492","0","<a href=\"http://forum.pirate-zone.com/?area=profile&amp;user=0\" class=\"\">System</a> has removed you from their friend list","A Lost Friend");
INSERT INTO forum_mail VALUES("67","1","0","1366330496","0","Substantial has added you to their friend list","A New Friend");
INSERT INTO forum_mail VALUES("68","1","6","1366330560","1","Substantial has added you to their friend list","A New Friend");
INSERT INTO forum_mail VALUES("69","1","0","1366330566","0","<a href=\"http://forum.pirate-zone.com/?area=profile&amp;user=0\" class=\"\">System</a> has removed you from their friend list","A Lost Friend");
INSERT INTO forum_mail VALUES("70","1","6","1366331855","1","<a href=\"http://forum.pirate-zone.com/?area=profile&amp;user=6\" class=\"adminfont\">OceanMaster</a> has removed you from their friend list","A Lost Friend");
INSERT INTO forum_mail VALUES("71","1","6","1366332315","1","Substantial has added you to their friend list","A New Friend");
INSERT INTO forum_mail VALUES("72","1","2","1366332723","1","Substantial has added you to their friend list","A New Friend");
INSERT INTO forum_mail VALUES("74","1","1","1366338590","1","OceanMaster has added you to their friend list","A New Friend");
INSERT INTO forum_mail VALUES("75","1","1","1366339347","1","Nelo has added you to their friend list","A New Friend");
INSERT INTO forum_mail VALUES("76","1","1","1366346998","1","Rogue has added you to their friend list","A New Friend");
INSERT INTO forum_mail VALUES("77","1","0","1366347052","0","Rogue has added you to their friend list","A New Friend");
INSERT INTO forum_mail VALUES("78","1","4","1366347055","1","Rogue has added you to their friend list","A New Friend");
INSERT INTO forum_mail VALUES("79","1","0","1366347066","0","<a href=\"http://forum.pirate-zone.com/?area=profile&amp;user=0\" class=\"\">System</a> has removed you from their friend list","A Lost Friend");
INSERT INTO forum_mail VALUES("80","1","1","1366347173","1","Linksbrawler has added you to their friend list","A New Friend");
INSERT INTO forum_mail VALUES("81","1","6","1366421347","1","Your title is now &quot;The Master of Oceans&quot;","Title");
INSERT INTO forum_mail VALUES("83","1","6","1366520186","1","Linksbrawler has added you to their friend list","A New Friend");
INSERT INTO forum_mail VALUES("84","1","0","1366520195","0","Linksbrawler has added you to their friend list","A New Friend");
INSERT INTO forum_mail VALUES("85","1","0","1366520202","0","<a href=\"http://forum.pirate-zone.com/?area=profile&amp;user=0\" class=\"\">System</a> has removed you from their friend list","A Lost Friend");
INSERT INTO forum_mail VALUES("86","1","4","1366520302","1","Linksbrawler has added you to their friend list","A New Friend");
INSERT INTO forum_mail VALUES("87","1","2","1366520324","1","<a href=\"http://forum.pirate-zone.com/?area=profile&amp;user=2\" class=\"linkfont\">Linksbrawler</a> has removed you from their friend list","A Lost Friend");
INSERT INTO forum_mail VALUES("88","1","2","1366520332","1","Rogue has added you to their friend list","A New Friend");
INSERT INTO forum_mail VALUES("89","1","0","1366520349","0","Rogue has added you to their friend list","A New Friend");
INSERT INTO forum_mail VALUES("90","1","0","1366520354","0","<a href=\"http://forum.pirate-zone.com/?area=profile&amp;user=0\" class=\"\">System</a> has removed you from their friend list","A Lost Friend");
INSERT INTO forum_mail VALUES("91","1","0","1366520359","0","Rogue has added you to their friend list","A New Friend");
INSERT INTO forum_mail VALUES("92","1","0","1366520364","0","<a href=\"http://forum.pirate-zone.com/?area=profile&amp;user=0\" class=\"\">System</a> has removed you from their friend list","A Lost Friend");
INSERT INTO forum_mail VALUES("93","1","2","1366520518","1","Linksbrawler has added you to their friend list","A New Friend");
INSERT INTO forum_mail VALUES("94","1","1","1366585015","1","<a href=\"http://forum.pirate-zone.com/?area=profile&amp;user=1\" class=\"malikfont\">Rogue</a> has removed you from their friend list","A Lost Friend");
INSERT INTO forum_mail VALUES("95","1","1","1366847353","1","<a href=\"http://forum.pirate-zone.com/?area=profile&amp;user=1\" class=\"malikfont\">Encore</a> has removed you from their friend list","A Lost Friend");
INSERT INTO forum_mail VALUES("96","1","1","1366847391","1","Nelo has added you to their friend list","A New Friend");
INSERT INTO forum_mail VALUES("97","1","1","1366862717","1","A topic you are watching has been replied to \n [url=http://forum.pirate-zone.com/?area=forum&amp;s=topic&amp;t=53]http://forum.pirate-zone.com/?area=forum&amp;s=topic&amp;t=53[/url]","Topic Notification");
INSERT INTO forum_mail VALUES("98","1","28","1366862717","1","A topic you are watching has been replied to \n [url=http://forum.pirate-zone.com/?area=forum&amp;s=topic&amp;t=53]http://forum.pirate-zone.com/?area=forum&amp;s=topic&amp;t=53[/url]","Topic Notification");
INSERT INTO forum_mail VALUES("99","1","1","1366862750","1","A topic you are watching has been replied to \n [url=http://forum.pirate-zone.com/?area=forum&amp;s=topic&amp;t=53]http://forum.pirate-zone.com/?area=forum&amp;s=topic&amp;t=53[/url]","Topic Notification");
INSERT INTO forum_mail VALUES("100","1","28","1366862750","1","A topic you are watching has been replied to \n [url=http://forum.pirate-zone.com/?area=forum&amp;s=topic&amp;t=53]http://forum.pirate-zone.com/?area=forum&amp;s=topic&amp;t=53[/url]","Topic Notification");
INSERT INTO forum_mail VALUES("101","2","2","1367026394","1","sdfsd","dfsdf");
INSERT INTO forum_mail VALUES("102","1","1","1367026467","1","Ipsum has added you to their friend list","A New Friend");
INSERT INTO forum_mail VALUES("103","1","1","1367026541","1","<a href=\"http://forum.pirate-zone.com/?area=profile&amp;user=1\" class=\"malikfont\">Ipsum</a> has removed you from their friend list","A Lost Friend");
INSERT INTO forum_mail VALUES("104","1","1","1367026597","1","<a href=\"http://forum.pirate-zone.com/?area=profile&amp;user=1\" class=\"malikfont\">Ipsum</a> has removed you from their friend list","A Lost Friend");
INSERT INTO forum_mail VALUES("105","1","1","1367026598","1","<a href=\"http://forum.pirate-zone.com/?area=profile&amp;user=1\" class=\"malikfont\">Ipsum</a> has removed you from their friend list","A Lost Friend");
INSERT INTO forum_mail VALUES("106","1","0","1367026604","0","Linksbrawler has added you to their friend list","A New Friend");
INSERT INTO forum_mail VALUES("107","1","1","1367026630","1","<a href=\"http://forum.pirate-zone.com/?area=profile&amp;user=1\" class=\"malikfont\">Ipsum</a> has removed you from their friend list","A Lost Friend");
INSERT INTO forum_mail VALUES("108","1","0","1367026663","0","<a href=\"http://forum.pirate-zone.com/?area=profile&amp;user=0\" class=\"\">System</a> has removed you from their friend list","A Lost Friend");
INSERT INTO forum_mail VALUES("109","1","1","1367026687","1","Linksbrawler has added you to their friend list","A New Friend");
INSERT INTO forum_mail VALUES("110","1","2","1367030587","1","<a href=\"http://forum.pirate-zone.com/?area=profile&amp;user=2\" class=\"linkfont\">Linksbrawler</a> has removed you from their friend list","A Lost Friend");
INSERT INTO forum_mail VALUES("111","1","2","1367030680","0","Ipsum has added you to their friend list","A New Friend");
INSERT INTO forum_mail VALUES("115","1","1","1367031327","1","Ipsum has added you to their friend list","A New Friend");
INSERT INTO forum_mail VALUES("116","1","1","1367031630","1","<a href=\"http://forum.pirate-zone.com/?area=profile&amp;user=1\" class=\"malikfont\">Ipsum</a> has removed you from their friend list","A Lost Friend");
INSERT INTO forum_mail VALUES("118","1","1","1367031847","1","sd","test");
INSERT INTO forum_mail VALUES("119","1","1","1367031876","1","Ipsum has added you to their friend list","A New Friend");
INSERT INTO forum_mail VALUES("123","1","2","1367032379","0","Pirate-Zone has added you to their friend list","A New Friend");
INSERT INTO forum_mail VALUES("124","1","1","1367085308","1","<a href=\"http://forum.pirate-zone.com/?area=profile&amp;user=1\" class=\"malikfont\">Ipsum</a> has removed you from their friend list","A Lost Friend");
INSERT INTO forum_mail VALUES("127","1","0","1367171906","0","Linksbrawler has added you to their friend list","A New Friend");
INSERT INTO forum_mail VALUES("128","1","0","1367171941","0","<a href=\"http://forum.pirate-zone.com/?area=profile&amp;user=0\" class=\"\">System</a> has removed you from their friend list","A Lost Friend");
INSERT INTO forum_mail VALUES("129","1","0","1367171947","0","Linksbrawler has added you to their friend list","A New Friend");
INSERT INTO forum_mail VALUES("130","2","2","1367172069","0","123","Test");
INSERT INTO forum_mail VALUES("131","1","2","1367198136","1","Nelo has added you to their friend list","A New Friend");
INSERT INTO forum_mail VALUES("132","1","0","1367278282","0","Ipsum has added you to their friend list","A New Friend");
INSERT INTO forum_mail VALUES("133","1","0","1367278581","0","<a href=\"http://forum.pirate-zone.com/?area=profile&amp;user=0\" class=\"\">System</a> has removed you from their friend list","A Lost Friend");
INSERT INTO forum_mail VALUES("134","1","0","1367278585","0","Ipsum has added you to their friend list","A New Friend");
INSERT INTO forum_mail VALUES("135","1","0","1367278591","0","<a href=\"http://forum.pirate-zone.com/?area=profile&amp;user=0\" class=\"\">System</a> has removed you from their friend list","A Lost Friend");



DROP TABLE forum_polls;

CREATE TABLE `forum_polls` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `topic_id` int(100) NOT NULL,
  `question` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;




DROP TABLE forum_polls_option;

CREATE TABLE `forum_polls_option` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `poll_id` int(100) NOT NULL,
  `value` varchar(100) NOT NULL,
  `total` int(100) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;




DROP TABLE forum_polls_vote;

CREATE TABLE `forum_polls_vote` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `poll_id` int(100) NOT NULL,
  `account_id` int(100) NOT NULL,
  `option_id` int(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;




DROP TABLE forum_posts;

CREATE TABLE `forum_posts` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `author_id` int(100) NOT NULL,
  `text` text NOT NULL,
  `date` varchar(100) NOT NULL,
  `topic_id` int(100) NOT NULL,
  `attachment` int(100) NOT NULL,
  `forum_id` int(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=808 DEFAULT CHARSET=latin1;

INSERT INTO forum_posts VALUES("89","1","sd","1363641873","24","0","0");
INSERT INTO forum_posts VALUES("103","2","I need a list of people who should have access to the forums. I\'ve only pmed Draco and he said he\'ll make an account today. But I also need to know who else to pm. S ogive me a list of people and I\'ll pm them. \n\nI think Night and Ocean should be pmed.\n\nEdit: Removed Night from the list\n\n------List of people who has access----\nOreo\nLinksbrawler\nMelo\nDraco\nOceanMaster","1363972663","27","0","0");
INSERT INTO forum_posts VALUES("105","1","I already Informed Night, he has the info. Also, We only need the admins to have the links to transfer the stuff. Night should be gm or admin, I would like to have him as a forum admin, since he has the most experience then all of us, not including melo.","1363988782","27","0","0");
INSERT INTO forum_posts VALUES("106","2","I don\'t think our current admins on the forum should have the link to this one yet.\n\n----\n\nI think both Night and Melo should be moderator right away. It\'s way too early and we need to atleast have out trusted staff to know more about them. And I hope I\'m not giving you the wrong idea. These two are good workers, but I\'m trying not to speak out of our own self interest. I\'m trying to speak for the staff and project as a whole.\n\nPersonally, I don\'t like the ranks of most of our staff member, as you know. So I think that their ranks should be taken care of first before we promote anybody else.\n\n---\n\nDo you want me to pm OceanMaster or not? I think I want to speak to him on xat first. I have siblings too, so I know how hard it is to keep things away from them, especially if they\'re participating too. I think I might come up with a plan with him.\nAnother issue is that if we somehow guarantee that Ocean\'s brother wont see the forums, I\'m still not sure that you want him to have access to the forums.\n\n","1363989647","27","0","0");
INSERT INTO forum_posts VALUES("107","1","IMO Melo has done more then half of our staff.\n\nEdit: I have a lot of links to edit! x.x","1363990197","27","0","0");
INSERT INTO forum_posts VALUES("108","2","That\'s why I said that most of our staff members should be dealt with.. How is it that someone who isn\'t even part of this project (yet) does more than half our staff members... That\'s just sad..","1363990433","27","0","0");
INSERT INTO forum_posts VALUES("109","1","TBH, he is a webmaster, he helps with the forum.","1363990961","27","0","0");
INSERT INTO forum_posts VALUES("110","4","I agree that Ocean might leak the site to SekretGenya (if unintentionally), so maybe we shouldn\'t give it to him just yet. But I think we should invite Zoro-Whore.\n\nAlso, 2 questions:\n~Who\'s Melo? \n~Why would Night be an admin (nothing against him it just seems a bit fast)?","1363999514","27","0","0");
INSERT INTO forum_posts VALUES("111","1","Melo, is my good friend/gfx/coder that has made all my forum layouts, and the new one that is coming, and he helps out with the forum coding so it go by faster, and Night should be gm, and Zoro, not so sure.","1363999718","27","0","0");
INSERT INTO forum_posts VALUES("112","1","Here we can discuss the powers of the ranks, atm all the staff ranks have the same power but the admins are the only one that can access the acp.","1363999803","28","0","0");
INSERT INTO forum_posts VALUES("113","4","Slightly off topic, but the ACP doesn\'t work for me.","1364000683","28","0","0");
INSERT INTO forum_posts VALUES("114","1","What does it do? or doesnt do/","1364001893","28","0","0");
INSERT INTO forum_posts VALUES("115","4","U told me how to do it, thanks.\n\nAnyway, the basic stuff is that moderators only have special powers in the sections which they moderate (e.g. locking, deleting posts/topics, editing other\'s posts etc.).\nGlobal mods have the same powers, but in every section (generally no extra powers I don\'t think).\nAdmins~ same as GM + use of the ACP.\nWebbys~ same as admin.\n\nAlthough this is something we could ask Night or Enaki about: they both have experience already with modding for this type of forum.","1364002955","28","0","0");
INSERT INTO forum_posts VALUES("116","1","Yeah, and Melo, since he\'s a mod on aH\n\nEdit: Gotta fix this post count shizzzzz.","1364003034","28","0","0");
INSERT INTO forum_posts VALUES("122","4","Oh yh, I forgot about him.\n\nAnd postcount: I noticed.","1364003537","28","0","0");
INSERT INTO forum_posts VALUES("129","2","Back on topic\n~~~~~~~~~~~~~~\nI trust that no-one will leak my personal opinion. \n\nI think the staff list is better suited like this:\n\nWebbies:\nLinksbrawler\nOreo\n\nAdmin:\nDraco\nOcean\n\nGM:\nSnake\nHJ\nZoro\nDayo\nPhantom\n\nMod:\nNight\nMelo\nSuperSmash\n\nMods which barely does anything:\nEnaki_Renraku\nLemon\nsecretgenya\n\nPeople who should get fired:\nAeryon\nMystogan\nDragon\nLengkilod\n\n\nThis is only the first batch. I think we should promote/demote again after a few months.\nFor example, few months later, we can clear the gm List by promoting/demoting some of them again. (Mostly demote I think). This will make space for really great people to be a gm, like supersmash(my opinion), Melo(both mine and Oreo\'s opinion), and Night (Mostly Oreo\'s opinion).","1364007623","28","0","0");
INSERT INTO forum_posts VALUES("126","1","Does youtube buffer extremely more now?","1364003809","28","0","0");
INSERT INTO forum_posts VALUES("127","2","I don\'t think Zoro should have access to the forums. \n\nI think Ocean should have access too, but it\'s two against one atm so I\'ll just back off for a while.\n\nAnd I agree with Draco. You seem to be rushing and doing big things too quickly. Both Night and Melo needs months in my opinion before being promoted.\nAnd tbh, everyone else should take months before being promoted from Mod to GM aswell. \nAnd Night hasn\'t done much work around the forums tbh. I\'m not saying he has done no work, but not enough to get anyones support.\nMelo should be a mod on the regular forums, and he has to show that he\'s been doing a lot and still does a lot. Then I think he should be promoted to GM in a few months.\n\n","1364006490","27","0","0");
INSERT INTO forum_posts VALUES("128","1","Atm, I think Melo should be admin. This is because, he has done more work then most of the staff, helps codes the forum, gfxes forum layouts for me, and help me get images. He also knows java, php, and css. So when me and Link are gone, we will have melo for the forums, and Draco/Ocean for the in-game. Melo is also a very known person/liked person around the boards and other games, which will bring more people to P-Z increasing our status, and getting us more known. These are beneficial factors in my opinion.","1364006808","27","0","0");
INSERT INTO forum_posts VALUES("130","2","I\'m impressed with Melo too. He helped me a bit before with mysql. Even though I got little progress, his effort is what I appreciate most.\n\nI\'m not saying that he shouldn\'t be promoted. But having him an admin right away isn\'t what should happen. He should start like everyone else, and make their way up gradually. So I\'d like to see him with that position, but he shouldn\'t get there quickly. \n\n\n","1364007980","27","0","0");
INSERT INTO forum_posts VALUES("131","1","mhm","1364008151","27","0","0");
INSERT INTO forum_posts VALUES("132","3","To be honest Its OK If I don\'t have staff powers since I don\'t plan on playing this game. I barely even play games aside from League, just helping for the time being.\n\nNow based on my experience over the years, I believe only admins and webmasters should have access to the Admin Panel. With the current setup of this forum, Global Moderators should be able to have all the regular privileges such as having attachments, etc, but they shouldn\'t have access to admin areas. Mods shouldn\'t have any powers aside from viewing hidden topics. However, based on how the reputation system is made, you could fork it and turn it into a warning system for mods to use too. That way Mods can at least view topics and warn people.","1364017949","28","0","0");
INSERT INTO forum_posts VALUES("133","4","If anything, Melo should be a webmaster. Not because he hasn\'t done a lot of work, but because most, if not all of it had been coding based. Probably the main reason HJ changed from being a Webby to admin was that he didn\'t do as much coding as he did before, but still did some other work. \n\nI also agree with Links in that its still pretty soon. Despite how much work he\'s done, I still think he needs to be here a few months (officially) before being promoted. The other staff need to know who he is. For starters he could make an account on the other forum.\n\nAs for Zoro, I just thought that at least one of the original admins should have access to this. I though he was more suited than Dayo, Snake or HJ (or Aeryon).\n\nAnd what do you mean &quot;when me and Link are gone&quot;?","1364040648","27","0","0");
INSERT INTO forum_posts VALUES("134","4","@links~ a few changes I would personally make to your list\nZoro~ admin\nHJ~ webby (not cos he necessarily deserves it, but that\'s what he is).\nLemon and Genya~ regular mods: both are still reasonably active (more than night or phantom).\nPhantom~ mod.\n\nAnd I agree on those promotions soon (not too soon though) but only if night is more active, and Melo is more well know.\n\n@melo\nFirst of all, nice to meet you.\nI agree with your view on admins and GMs, but mods should still be able to enforce the rules (locking, editing, deleting etc.) in the section which they moderate.","1364041277","28","0","0");
INSERT INTO forum_posts VALUES("135","1","Menlo do have an account on the forums when he came on the sat all the time, and I\'m planning to go away for a while 3-4 months in when the game is out for school ","1364064763","27","0","0");
INSERT INTO forum_posts VALUES("136","2","@Melo, Melo doesn\'t just code. He has gfxing and moderation skills. If anything, he should be hired to be a mod, then gradually make it to webby/admin. \n\n@HJ, HJ has very little coding skills. He was changed from webby to Admin because me and Oreo knew this. You guys easily got convinced and deceived. He helped me with html, but I had to fix a lot of the codes, and I think he used a program to help.\nAlso, he doesn\'t do anything. When I was hired, it took me a few weeks to start on the ingame. But HJ never started on coding anything.\n\n@Zoro, I don\'t really consider him a good admin. He may do more work and is more active than other admins, but he has bad and poor leadership skills. \n1. He sometimes doesn\'t post for a few days then suddenly randomly spams a lot.\n2. He sometimes breaks the forum rules.\n3. He started an argument and tried to bring down a new registered member because he misread something.\n4. You don\'t have access to the admin section, but he\'s always answering things with one sentence. It doesn\'t matter if what I write is important, big, or small, if he replies, it would be a few words. \n\nSo if anything, I think he would be a good gm, but not an admin.\n\n\n\nAnd I don\'t know what Oreo means by when me and him are gone.\n\n","1364064865","27","0","0");
INSERT INTO forum_posts VALUES("137","1","Hj didn\'t do any coding, all he knows is HTML.","1364064935","28","0","0");
INSERT INTO forum_posts VALUES("138","2","I think every admin should be demoted to GM for fairness. Then from there, we can start promoting some of them to admin, demote some of them to mod, and leave some of them as GM.\n\n@Zoro, as discussed on another thread:\n[quote]@Zoro, I don\'t really consider him a good admin. He may do more work and is more active than other admins, but he has bad and poor leadership skills. \n1. He sometimes doesn\'t post for a few days then suddenly randomly spams a lot.\n2. He sometimes breaks the forum rules.\n3. He started an argument and tried to bring down a new registered member because he misread something.\n4. You don\'t have access to the admin section, but he\'s always answering things with one sentence. It doesn\'t matter if what I write is important, big, or small, if he replies, it would be a few words. \n\nSo if anything, I think he would be a good gm, but not an admin.[/quote]\n\n@HJ\n[quote]HJ has very little coding skills. He was changed from webby to Admin because me and Oreo knew this. You guys easily got convinced and deceived. He helped me with html, but I had to fix a lot of the codes, and I think he used a program to help. \nAlso, he doesn\'t do anything. When I was hired, it took me a few weeks to start on the ingame. But HJ never started on coding anything. [/quote]\n\n@Phantom, I don\'t have a problem with him being a mod. I think it would be better this way.\n\nAnd Night is active on the chat. But him not being active on the forums is annoying tbh :/\n\n\n","1364065075","28","0","0");
INSERT INTO forum_posts VALUES("139","4","Ok, I see your points. Still, from the view of much of the staff, having none of the old admins as admins here seems a bit like a rebellion. If not Zoro, then Dayo.\nTruthfully, I think they should all stay as admins, and we demote them later if they don\'t start doing more. Of course, except Aeryon.","1364067790","28","0","0");
INSERT INTO forum_posts VALUES("140","1","We will have to much admins then.\n--------------------------------------------------\nLinks on the topics are updated.","1364068077","28","0","0");
INSERT INTO forum_posts VALUES("141","2","[quote]Ok, I see your points. Still, from the view of much of the staff, having none of the old admins as admins here seems a bit like a rebellion. If not Zoro, then Dayo.\nTruthfully, I think they should all stay as admins, and we demote them later if they don\'t start doing more. Of course, except Aeryon.[/quote]\n\nI don\'t think that our admins are suited to be admins. \nIt may seem like rebellion, but not if the admins them self agrees. I spoke to Snake about this, and he agrees with this, even though he knows he\'ll be a gm. ","1364073594","28","0","0");
INSERT INTO forum_posts VALUES("142","4","Wow, that\'s pretty selfless of him. \nWell, now that I\'ve heard that, it sounds a lot more reasonable. ","1364076678","28","0","0");
INSERT INTO forum_posts VALUES("143","1","Snake is good, his problem is just that he\'s not active on the forums. He comes on the chat everyday though, but no one is online.\n\nA little of topic, but when a user is afk or logged out, how long should it be till it shows that that user is offline?","1364076686","28","0","0");
INSERT INTO forum_posts VALUES("144","2","Another thing about Snake is that he has poor punctuation skills. Although, not posting much on the forums is a bigger issue. Note: Snake does check the forums, but he just doesn\'t post.\n\n---\nReplying to your offtopic comment\nWhen someone afks, it should be 10 min\nWhen someone presses the log out button, it should be asap.","1364077318","28","0","0");
INSERT INTO forum_posts VALUES("216","3","Yup you see where it says &quot;logging out could&quot;. I think its a resolution problem since most of us have different pc sizes. ","1364246093","28","0","0");
INSERT INTO forum_posts VALUES("215","1","I can do that.","1364244744","27","0","0");
INSERT INTO forum_posts VALUES("151","1","Since it\'s just a game, you don\'t need that good punctuation skills, but it\'s good to have them.\n-------------------------------------------\nPost count is now fix, going to add a redirect so when you click reply, it will take you to the page you\'re on.","1364077591","28","0","0");
INSERT INTO forum_posts VALUES("152","2","You\'re expected to have good punctuation skills when you\'re a staff. How can you lead or communicate to other people if you can\'t even be understood?","1364077719","28","0","0");
INSERT INTO forum_posts VALUES("153","1","I\'m talking about adding commas and stuff. Grammar is important, but punctuation skills, is need but not that much. periods and stuff are need.\n----------------------------------------\nNew Section added.","1364078081","28","0","0");
INSERT INTO forum_posts VALUES("154","1","Redirect is added/","1364078306","28","0","0");
INSERT INTO forum_posts VALUES("156","2","I\'m also talking about punctuation. If I see no punctuation, I\'d be confused. Punctuation is very important aswell. Snake writes big paragraphs without any punctuation, and I have trouble understanding.","1364078594","28","0","0");
INSERT INTO forum_posts VALUES("157","1","Link one please.","1364078654","28","0","0");
INSERT INTO forum_posts VALUES("158","4","Good SPG doesn\'t need to be used all of the time, but you still need to know how to use it if you\'re a staff member, especially an admin. Although Snake may not use if just because he can\'t be bothered or thinks he doesn\'t need to, rather than because he can\'t.\n\nAs for the on/offline thing, I\'d say 5 mins for AFK and 1 min for logged out. Logging out could be accidental, or just while fixing something.\n\nAnd I see you\'ve used by buttons :D","1364078944","28","0","0");
INSERT INTO forum_posts VALUES("159","1","[quote]Good SPG doesn\'t need to be used all of the time, but you still need to know how to use it if you\'re a staff member, especially an admin. Although Zoro may not use if just because he can\'t be bothered or thinks he doesn\'t need to, rather than because he can\'t.\n\nAs for the on/offline thing, I\'d say 5 mins for AFK and 1 min for logged out. Logging out could be accidental, or just while fixing something.\n\nAnd I see you\'ve used by buttons :D[/quote] Buttons?","1364079005","28","0","0");
INSERT INTO forum_posts VALUES("160","2","You know very well that Snake talks a lot more on chat than the forums &gt;.&gt;\n\nAnd besides, it will be time consuming to find snakes post. \nHere is a few that I found quickly.\nhttp://w11.zetaboards.com/Pirate_Boards/topic/8378840/1/#post8134096\nhttp://w11.zetaboards.com/Pirate_Boards/topic/8368330/1/#post8132872\nhttp://prntscr.com/qatgt\n\nIt may not be confusing, but it is annoying to keep reading without a period or comma, atleast in my opinion. ","1364079238","28","0","0");
INSERT INTO forum_posts VALUES("161","2","5 min is also good for afk\nBut for the log off, it should be asap. if it was accidental, than the person can easily log back in. Also, I\'m not talking about the ingame. The ingame will wait for a min or two for the person to come back online.","1364079359","28","0","0");
INSERT INTO forum_posts VALUES("162","1","Mhm","1364079538","28","0","0");
INSERT INTO forum_posts VALUES("168","2","So what do you guys think the staff list should be like? \n\nAnd do you think Ocean approves of these staff list?","1364090923","28","0","0");
INSERT INTO forum_posts VALUES("169","1","Tell Ocean to pm me, and I think that\'s all for now. ","1364091640","27","0","0");
INSERT INTO forum_posts VALUES("202","2","Oh, I just read this message. No wonder you pmed me.. Kk I\'ll do it now.","1364152466","27","0","0");
INSERT INTO forum_posts VALUES("203","1","Okay, thanks.","1364153842","27","0","0");
INSERT INTO forum_posts VALUES("206","3","Actually, you guys could just member me, and just make a rank that allows me to atleast view you guys discussions, without having moderation powers. That way I could stay a member, and atleast have input when you guys need help on something. Its easy to do aswell","1364187022","27","0","0");
INSERT INTO forum_posts VALUES("208","3","Maybe its just me, but all of Draco\'s posts that should auto br overlaps instead. \n\n[img]http://gyazo.com/702910d0a8de8c3a3559f843d9fccf82.png?1364190579[/img]","1364190675","28","0","0");
INSERT INTO forum_posts VALUES("210","4","You mean when it gets to the end of the row and gets cut off? That doesn\'t happen with me...","1364195547","28","0","0");
INSERT INTO forum_posts VALUES("212","1","I\'m planning to take that template out anyway unless anyone wants to keep it\n","1364241587","28","0","0");
INSERT INTO forum_posts VALUES("218","4","So is it just ocean?","1364282388","27","0","0");
INSERT INTO forum_posts VALUES("219","2","@Nelo, why member you?\n\n@Draco, what do you mean is it just Ocean?\n\n@Oreo, I just realized, why did I pm Ocean for you? Why didn\'t you do it yourself [erm]","1364321894","27","0","0");
INSERT INTO forum_posts VALUES("220","1","[angel]","1364330562","27","0","0");
INSERT INTO forum_posts VALUES("221","1","sxz","1364331917","28","0","0");
INSERT INTO forum_posts VALUES("222","4","[quote]@Nelo, why member you?\n\n@Draco, what do you mean is it just Ocean?\n\n@Oreo, I just realized, why did I pm Ocean for you? Why didn\'t you do it yourself [erm][/quote]\nI mean ishe the only person not currently with access to his site that we\'re gonna tell, at the moment?","1364337000","27","0","0");
INSERT INTO forum_posts VALUES("223","4","Very constructive comment[meh]","1364337143","28","0","0");
INSERT INTO forum_posts VALUES("224","2","I want Ocean to have access here even though I dont know how he\'ll react with my opinion on changing the staff list.\n\nMoreover, Oreo and Ocean aren\'t really close and it\'s Oreo\'s choice on who has access to the forum.","1364337482","27","0","0");
INSERT INTO forum_posts VALUES("225","2","*Thumb up that comment*","1364337520","28","0","0");
INSERT INTO forum_posts VALUES("226","1","I was testing something[dry]","1364349588","28","0","0");
INSERT INTO forum_posts VALUES("227","1","Hmm?","1364421470","27","0","0");
INSERT INTO forum_posts VALUES("255","2","Does Night really need access to the forums? It\'s not like he\'s active and is contributing. He hasn\'t even made an account since you\'ve reset the database.\n\nWe shouldn\'t share this site with people we like just to show off.","1364862693","27","0","0");
INSERT INTO forum_posts VALUES("256","2","I know we\'ve talked about this before but I don\'t really think we should have &quot;forum admins&quot; and &quot;in-game admins&quot;","1364862752","28","0","0");
INSERT INTO forum_posts VALUES("257","3","Whats the link to the ZB forum. I installed chrome and deleted torch forgetting to save my bookmarks. ","1364872802","27","0","0");
INSERT INTO forum_posts VALUES("258","3","I never really understood why there are 2 different admins in the first place. That\'s just something that evolved on the arenas.","1364873058","28","0","0");
INSERT INTO forum_posts VALUES("259","4","Are there separate ones on NA?","1364896591","28","0","0");
INSERT INTO forum_posts VALUES("260","4","http://w11.zetaboards.com/Pirate_Boards/","1364896687","27","0","0");
INSERT INTO forum_posts VALUES("261","1","Sib is forum and Oro is Game, Depends how many admins we have, so it can balance out.","1364935128","28","0","0");
INSERT INTO forum_posts VALUES("262","1","I don\'t even think Night remembers this forum. And there is really not much for him to do, we just need to code and rework the characters later. we need to code the stuff first then rework, cause what if it doesn\'t work out like Ocean and Draco want it to....\n\nAlso, you guys should chill on the crew stuff, I can only code so much and I\'m going to stop coding soon.","1364935247","27","0","0");
INSERT INTO forum_posts VALUES("264","1","test","1365022054","27","0","0");
INSERT INTO forum_posts VALUES("265","1","power angers.","1365107149","28","0","0");
INSERT INTO forum_posts VALUES("266","1","test","1365110199","28","0","0");
INSERT INTO forum_posts VALUES("272","1","esr","1365121449","28","0","0");
INSERT INTO forum_posts VALUES("388","6","[quote]The forum guide are the forum rules, I changed the links and never changed it there .\nIcons look better.\nI can add a scroll to make it more \nI can make you one if you want, and that is what I was talking about, people will pay for that.\nAnd you can make your own title in the Acp\n[/quote]\n\n2) That\'s just your opinion. I want avies too.\n4) I want one. I\'ll think about the color...\n5) I gotta check that out.","1366340346","49","0","0");
INSERT INTO forum_posts VALUES("386","1","The forum guide are the forum rules, I changed the links and never changed it there .\nIcons look better.\nI can add a scroll to make it more \nI can make you one if you want, and that is what I was talking about, people will pay for that.\nAnd you can make your own title in the Acp\n","1366339133","49","0","0");
INSERT INTO forum_posts VALUES("387","1","I\'ll edit the buttons. And you can switch your template in the acp and I\'ll make a new big or try.","1366339248","50","0","0");
INSERT INTO forum_posts VALUES("385","6","Continue editing the forum. Many things can be made better. For example, some buttons don\'t work. When I\'m on Edit, the buttons above the emoticons do nothing. As well, I don\'t know which page I\'m on when I\'m on a topic with many pages and I leave the computer for some time. This font kinda annoys me, but hopefully it\'ll be changeable with different templates.\n\nCan we change the background image? It\'s too boring in my opinion.\n\nYes, I\'ll stick at many things.[blush]","1366338989","50","0","0");
INSERT INTO forum_posts VALUES("384","6","[quote]To clear everything. The topic id\'s are all messed up with the post id\'s[/quote]\n\nOh. Wow.\n\nBy the way, I got a few questions, since I\'m new here.\n\n1) What\'s the Forum Guide thing for? It doesn\'t work.\n2) Why can\'t I keep my avy? Why do you want only icons?\n3) In my opinion, the Latest Activity/Topics need to have more posts in them. Like, up to 10-15.\n4) How do you add a glow to my name? I want one... it looks sexy.\n5) Are we going to keep the custom titles thing from P-B like &quot;Master of Oceans&quot; and so on?\n\nA few questions. ","1366338844","49","0","0");
INSERT INTO forum_posts VALUES("383","1","Now that I\'m done with the forum, I want you guys to give me ideas because I have none. Also I will be adding 2 additional templates, Draco\'s and something else.","1366338779","50","0","0");
INSERT INTO forum_posts VALUES("382","1","To clear everything. The topic id\'s are all messed up with the post id\'s","1366338516","49","0","0");
INSERT INTO forum_posts VALUES("380","1","Well, we were supposed to start shifting stuff over, but there\'s no point cause ima delete all the topics.\nThen change them its in the acp[angel]","1366338359","49","0","0");
INSERT INTO forum_posts VALUES("381","6","[quote]Well, we were supposed to start shifting stuff over, but there\'s no point cause ima delete all the topics.[/quote]\n\nAll topics where, and why?\n\nShifting things aside from Chars/Tasks isn\'t a good idea either; it\'s messy there so we\'re organizing it there first.","1366338442","49","0","0");
INSERT INTO forum_posts VALUES("392","1","We can make a poll .","1366341333","49","0","0");
INSERT INTO forum_posts VALUES("393","6","[quote]We can make a poll .[/quote]\n\nOn N-B.\n\nI still say that Avies should be used but Icons should be used too.","1366344381","49","0","0");
INSERT INTO forum_posts VALUES("394","1","Yeah you can do it on zeta and n-b","1366344552","49","0","0");
INSERT INTO forum_posts VALUES("395","6","[quote]Yeah you can do it on zeta and n-b[/quote]\n\nSure. I won\'t do it now cause I gotta leave. Tomorrow; sure.","1366345842","49","0","0");
INSERT INTO forum_posts VALUES("396","1","Alright ","1366346962","49","0","0");
INSERT INTO forum_posts VALUES("397","2","I\'m sure there is still a lot of work that needs to be done. Once I\'m done with exams, I\'ll be able to focus in testing the site in detail.","1366347349","50","0","0");
INSERT INTO forum_posts VALUES("398","2","[quote]I feel a bit left out that you guys pm me like almost a month after the start of this... [/quote]\n\nWell you\'re one of the few staff members that have access here. Not even our admins know about this forum.","1366347575","27","0","0");
INSERT INTO forum_posts VALUES("399","2","For the ranks, I couldn\'t add the highest rank because Oreo set a limit to the max digit. I just needed 1 more digit","1366347663","49","0","0");
INSERT INTO forum_posts VALUES("400","1","I\'ll make it longer when I get back from school.\n","1366373124","49","0","0");
INSERT INTO forum_posts VALUES("401","1","[quote][quote]I\'ll edit the buttons. And you can switch your template in the acp and I\'ll make a new big or try.[/quote]\n\nWhere is the ACP? Lol.[/quote]I meant the UCP.","1366373192","50","0","0");
INSERT INTO forum_posts VALUES("389","6","[quote]I\'ll edit the buttons. And you can switch your template in the acp and I\'ll make a new big or try.[/quote]\n\nWhere is the ACP? Lol.","1366340428","50","0","0");
INSERT INTO forum_posts VALUES("390","3","The only thing I could think of that doesn\'t require you to do more coding is non-flexible templates. There\'s still some things that need fixing like mentioned by Ocean, as well as others like the post ID. When you visit a certain page on a topic it resets itself to 1 again. ","1366340867","50","0","0");
INSERT INTO forum_posts VALUES("391","1","OH I forgot about that I\'ll do it when I get on my laptop","1366341163","50","0","0");
INSERT INTO forum_posts VALUES("379","6","So, since we have this site that will soon be our main, why don\'t we move the characters/tasks here. First of all, it\'s good to have a copy. Second of all, we\'ll test the site.\n\nEdit:\n\nThe ranks seem wrong too:\n\n[url=http://w11.zetaboards.com/Pirate_Boards/topic/7960718/10/]Click.[/url]\n\nP.S. Buttons above emoticons don\'t work.\n\nAs for the emoticons, lets check. [angel][anger][confused][blush][dead][devil][erm][GASP][groggy][meh][sad][shucks][sleep][smile][squwooshy][weep]\n\nThey work.","1366338231","49","0","0");
INSERT INTO forum_posts VALUES("378","1","I\'m going to leave that there for now. And its small caps","1366338190","28","0","0");
INSERT INTO forum_posts VALUES("377","6","[quote]It\'s gone now.[/quote]\n\nThe Rep. points are still there. Also, what\'s with the font? It hurts my eyes thinking that all of this is capital.","1366337941","28","0","0");
INSERT INTO forum_posts VALUES("365","2","Looks like editing doesn\'t work anymore so I\'ll post this for now:\n\n------List of people who has access---- \nOreo \nLinksbrawler \nMelo \nDraco \nNight\nOcean\n\n\nI think we should remove night from the list.","1366083120","27","0","0");
INSERT INTO forum_posts VALUES("366","1","night doesn\'t have access to the forum. And edit does work just gotta remake the button","1366142223","27","0","0");
INSERT INTO forum_posts VALUES("372","6","Ready only the first page. Will edit this post when I complete, but so far I agree with most things.\n\nAnd what\'s the &quot;Reputation thing about&quot;?","1366238824","28","0","0");
INSERT INTO forum_posts VALUES("373","6","I feel a bit left out that you guys pm me like almost a month after the start of this... ","1366238962","27","0","0");
INSERT INTO forum_posts VALUES("374","1","Well.... New Emoictions coming. I already made them, Just have to add them in.","1366239465","27","0","0");
INSERT INTO forum_posts VALUES("375","2","[quote]\n\nAnd what\'s the &quot;Reputation thing about&quot;?[/quote]\n\nOreo said that he\'ll remove it.","1366239676","28","0","0");
INSERT INTO forum_posts VALUES("376","1","It\'s gone now.","1366239742","28","0","0");
INSERT INTO forum_posts VALUES("410","6","[quote][quote][quote]I\'ll edit the buttons. And you can switch your template in the acp and I\'ll make a new big or try.[/quote]\n\nWhere is the ACP? Lol.[/quote]I meant the UCP.[/quote]\n\nEDIT:\n\nNevermind, found it.","1366423011","50","0","0");
INSERT INTO forum_posts VALUES("412","1","kk","1366426627","50","0","0");
INSERT INTO forum_posts VALUES("413","1","[img]http://i666.photobucket.com/albums/vv25/hi2431/kyo_zps36e209bd.gif[/img]","1366430488","50","0","0");
INSERT INTO forum_posts VALUES("414","2","Hey Ocean, can I hear your opinions and thoughts on my suggestion of ranks?","1366433971","28","0","0");
INSERT INTO forum_posts VALUES("415","1","[quote][img]http://i666.photobucket.com/albums/vv25/hi2431/kyo_zps36e209bd.gif[/img][/quote] off topic but rate my first banner [blush] And I know about the font, didn\'t realize how small it was till I uploaded it.","1366434902","28","0","0");
INSERT INTO forum_posts VALUES("424","3","On the quote, do you think you could add who is being quoted? ","1366516228","50","0","0");
INSERT INTO forum_posts VALUES("425","2","Thats what I asked him on skype, but more strangely worded :P","1366516368","50","0","0");
INSERT INTO forum_posts VALUES("428","6","The new layout has some bugs, and overall, when you\'re logged out, you can\'t view anything except for Register/Login screen and the forum. Fix that. I wanna see the sections, rules, etc.\n\nAs for the new layout, below the Reply box the Game/Homepage/Forum sitemap thing is messed up. It just goes down vertically, and looks ugly.","1366516782","50","0","0");
INSERT INTO forum_posts VALUES("429","2","[quote]The new layout has some bugs, and overall, when you\'re logged out, you can\'t view anything except for Register/Login screen and the forum. Fix that. I wanna see the sections, rules, etc.\n\nAs for the new layout, below the Reply box the Game/Homepage/Forum sitemap thing is messed up. It just goes down vertically, and looks ugly.[/quote]\n\nOreo just applied the change, and he\'s still working on fixing the glitches and changing the colours. ","1366516867","50","0","0");
INSERT INTO forum_posts VALUES("431","3","Test post","1366516916","50","0","0");
INSERT INTO forum_posts VALUES("435","3","Blah","1366517290","50","0","0");
INSERT INTO forum_posts VALUES("436","1","qawasdf","1366517335","50","0","0");
INSERT INTO forum_posts VALUES("438","6","[quote][quote]The new layout has some bugs, and overall, when you\'re logged out, you can\'t view anything except for Register/Login screen and the forum. Fix that. I wanna see the sections, rules, etc.\n\nAs for the new layout, below the Reply box the Game/Homepage/Forum sitemap thing is messed up. It just goes down vertically, and looks ugly.[/quote]\n\n\nOreo just applied the change, and he\'s still working on fixing the glitches and changing the colours. [/quote]\n\nI know, but I\'m letting him know.","1366517383","50","0","0");
INSERT INTO forum_posts VALUES("440","1","I know all that stuff, gotta fix the css.","1366517490","50","0","0");
INSERT INTO forum_posts VALUES("441","2","Oh okay :P","1366517502","50","0","0");
INSERT INTO forum_posts VALUES("442","1","And what do you mean? there are no topics to see, all our topics are in the staff only section.","1366517548","50","0","0");
INSERT INTO forum_posts VALUES("446","6","[quote]And what do you mean? there are no topics to see, all our topics are in the staff only section.[/quote]\n\nI can\'t even view the sections for them.","1366518984","50","0","0");
INSERT INTO forum_posts VALUES("447","1","I can when I\'m logged out. what does it say. If you try to go to a mod section forum, it won\'t let you cause of the permissions. And BBcodes work now. going to add new smiles.","1366519063","50","0","0");
INSERT INTO forum_posts VALUES("449","2","We should be seeing something like this right?\n[img]http://img843.imageshack.us/img843/5683/fdc74091bd884ed192a49ac.png[/img]","1366519395","50","0","0");
INSERT INTO forum_posts VALUES("450","1","Yes you should","1366519453","50","0","0");
INSERT INTO forum_posts VALUES("451","6","[quote]We should be seeing something like this right?\n[img]http://img843.imageshack.us/img843/5683/fdc74091bd884ed192a49ac.png[/img][/quote]\n\nI see that, but when I try to go to like the Introduction section, it just, pretty much, refreshes the page.","1366519717","50","0","0");
INSERT INTO forum_posts VALUES("452","1","lemme see \nEdit: I can. with out no refresh","1366519984","50","0","0");
INSERT INTO forum_posts VALUES("453","2","I also don\'t get the same problem..","1366520709","50","0","0");
INSERT INTO forum_posts VALUES("454","6","[quote][quote]The new layout has some bugs, and overall, when you\'re logged out, you can\'t view anything except for Register/Login screen and the forum. Fix that. I wanna see the sections, rules, etc.\n\nAs for the new layout, below the Reply box the Game/Homepage/Forum sitemap thing is messed up. It just goes down vertically, and looks ugly.[/quote]\n\nOreo just applied the change, and he\'s still working on fixing the glitches and changing the colours. [/quote]\n\nI\'ll wait.","1366522165","50","0","0");
INSERT INTO forum_posts VALUES("470","2","Just saying, I don\'t see Night doing anything. He only comes to the zetaboards forums when I ask him too...\nHas he done any work yet?","1366601865","28","0","0");
INSERT INTO forum_posts VALUES("472","1","No  One has told him to do work","1366602590","28","0","0");
INSERT INTO forum_posts VALUES("473","2","We can\'t really tell him to do work if we don\'t see him on the forums...","1366603202","28","0","0");
INSERT INTO forum_posts VALUES("474","1","True.","1366603829","28","0","0");
INSERT INTO forum_posts VALUES("475","1","We need to discuss HJ, He has done absolutely nothing.","1366604165","28","0","0");
INSERT INTO forum_posts VALUES("476","2","I agree with him not doing anything.","1366604608","28","0","0");
INSERT INTO forum_posts VALUES("477","1","He\'s been freeloading this whole time pretending to code.","1366604688","28","0","0");
INSERT INTO forum_posts VALUES("478","2","I had him code something in html for me a few months ago.\n","1366605011","28","0","0");
INSERT INTO forum_posts VALUES("479","1","Oh, how long did it take?","1366605049","28","0","0");
INSERT INTO forum_posts VALUES("480","2","A full day (hehe)\nI think that he probably used a software and w3schools to help him code. I had a lot of redoing and recoding to fix his codes.\n\n","1366605445","28","0","0");
INSERT INTO forum_posts VALUES("481","1","Lol","1366605464","28","0","0");
INSERT INTO forum_posts VALUES("483","2","[quote]Well, we were supposed to start shifting stuff over, but there\'s no point cause ima delete all the topics.\nThen change them its in the acp[angel][/quote]\n\nAre you also going to remove all data from the database(accounts, topics, clans, etc) before we release this forum to the staff and public? ","1366610599","49","0","0");
INSERT INTO forum_posts VALUES("488","6","[quote]Hey Ocean, can I hear your opinions and thoughts on my suggestion of ranks?[/quote]\n\nThe ranks where?","1366613305","28","0","0");
INSERT INTO forum_posts VALUES("490","6","[quote][quote]Well, we were supposed to start shifting stuff over, but there\'s no point cause ima delete all the topics.\nThen change them its in the acp[angel][/quote]\n\nAre you also going to remove all data from the database(accounts, topics, clans, etc) before we release this forum to the staff and public? [/quote]\n\nHe should remove everything here outside of the Mod Lounge, but the Mod Lounge shouldn\'t be touched.\n\nWe can just delete all the topics, or not make much at all.","1366613553","49","0","0");
INSERT INTO forum_posts VALUES("493","2","You should read all of the post here.\nThe ranks that I\'ve suggested is on the first page, post #8","1366614304","28","0","0");
INSERT INTO forum_posts VALUES("494","2","I think we should reset everything completely. \nI also don\'t want most of the staff to see some of our threads. Everyone who currently has access here is the people I trust most. ","1366614405","49","0","0");
INSERT INTO forum_posts VALUES("498","4","[quote][quote]I feel a bit left out that you guys pm me like almost a month after the start of this... [/quote]\n\nWell you\'re one of the few staff members that have access here. Not even our admins know about this forum.[/quote]\n\nExactly. There are only 5 of us here. You\'re still one of the elite few.\n","1366654089","27","0","0");
INSERT INTO forum_posts VALUES("499","4","[quote]Back on topic\n~~~~~~~~~~~~~~\nI trust that no-one will leak my personal opinion. \n\nI think the staff list is better suited like this:\n\nWebbies:\nLinksbrawler\nOreo\n\nAdmin:\nDraco\nOcean\n\nGM:\nSnake\nHJ\nZoro\nDayo\nPhantom\n\nMod:\nNight\nMelo\nSuperSmash\n\nMods which barely does anything:\nEnaki_Renraku\nLemon\nsecretgenya\n\nPeople who should get fired:\nAeryon\nMystogan\nDragon\nLengkilod\n\n\nThis is only the first batch. I think we should promote/demote again after a few months.\nFor example, few months later, we can clear the gm List by promoting/demoting some of them again. (Mostly demote I think). This will make space for really great people to be a gm, like supersmash(my opinion), Melo(both mine and Oreo\'s opinion), and Night (Mostly Oreo\'s opinion).[/quote]\n\nObviously a few things need to be edited (EG Aeryon). \n\nAnd I just noticed: nobody\'s commented on my sig yet....","1366654523","28","0","0");
INSERT INTO forum_posts VALUES("503","2","Well the new list would be:\n[quote]Webbies: \nLinksbrawler \nOreo \n\nAdmin: \nDraco \nOcean \n\nGM: \nSnake \nHJ \nZoro \nDayo \nPhantom \n\nMod: \nNight \nMelo \nSuperSmash \nEnaki_Renraku \nLemon \nsecretgenya [/quote]\n\nI also don\'t mind moving Phantom to mods.","1366656891","28","0","0");
INSERT INTO forum_posts VALUES("505","2","When you comment, or when you click on a thread that\'s on &quot;Latest Activity&quot;, you should be directed to the last page of the thread.","1366657107","50","0","0");
INSERT INTO forum_posts VALUES("506","4","I still strongly believe that at least 1 of the current admins should be an admin after the switch.","1366661023","28","0","0");
INSERT INTO forum_posts VALUES("509","1","I nominate Snake. and we have to much gms and mods.","1366661403","28","0","0");
INSERT INTO forum_posts VALUES("510","2","I still believe non of our current admins should be admins.\nIf we keep 1, than it will be much harder to demote him later on.","1366661628","28","0","0");
INSERT INTO forum_posts VALUES("514","1","I don\'t think so.","1366663326","28","0","0");
INSERT INTO forum_posts VALUES("516","2","Well let\'s wait till Ocean gets up to date and participate in this conversation.\n\nI\'ll hold onto my argument until Ocean gets involved.","1366663415","28","0","0");
INSERT INTO forum_posts VALUES("517","1","Well, we have to much staff members.","1366664094","28","0","0");
INSERT INTO forum_posts VALUES("518","1","Snake has returned from the land of the dead!","1366664299","28","0","0");
INSERT INTO forum_posts VALUES("519","2","The current number of staff members is a lot better than what it used to be before. I think we should focus on the ranks before we start focusing on the numbers again.\n","1366664367","28","0","0");
INSERT INTO forum_posts VALUES("520","1","Yeah.","1366664484","28","0","0");
INSERT INTO forum_posts VALUES("521","1","yeah, I\'m about to add that in.","1366665061","50","0","0");
INSERT INTO forum_posts VALUES("522","2","There should also be something that clearly tells you what page you\'re on.","1366665381","50","0","0");
INSERT INTO forum_posts VALUES("523","1","Calm your tits.","1366666236","50","0","0");
INSERT INTO forum_posts VALUES("524","2","Lol [angel]","1366666796","50","0","0");
INSERT INTO forum_posts VALUES("525","4","Your tits don\'t sound calm.","1366669014","50","0","0");
INSERT INTO forum_posts VALUES("526","1","Lol who?","1366673650","50","0","0");
INSERT INTO forum_posts VALUES("527","1","I have done your second request link, Now I shall do your first.\nBoth request have now been made.","1366689917","50","0","0");
INSERT INTO forum_posts VALUES("531","2","[angel]\nAlright, can I make a new request?\n\nWhile I was on [url=http://forum.pirate-zone.com/?area=forum&amp;s=topic&amp;t=28&amp;page=5]this[/url] thread, I noticed this: http://prntscr.com/11s0p9\nWe should have something like this instead http://prntscr.com/11s0td\n\nI hope you understand what I mean, we need the &quot;...&quot; in between.","1366690459","50","0","0");
INSERT INTO forum_posts VALUES("532","1","Idk why, but I like that, but i will try both ways.","1366690662","50","0","0");
INSERT INTO forum_posts VALUES("533","2","Well you cant see the last or first page, nor can you see how many pages there is in total.\nTbh, I don\'t like the &quot;...&quot; either, but it\'s better than not having it :/","1366690961","50","0","0");
INSERT INTO forum_posts VALUES("534","1","All you have to do is press last, I think they will be able to get that.","1366691048","50","0","0");
INSERT INTO forum_posts VALUES("538","2","Hmm, that\'s true\n\nWell I probably assumed that &quot;last&quot; would mean page 5 here: [img]http://i.imgur.com/9k3pn0r.png[/img]\nJust like how last would mean page 3 in this case: [img]http://i.imgur.com/FE8OzNB.png[/img]\n\nWell I think we probably should have the &quot;Last&quot; and &quot;First&quot; option when the first/last page number isn\'t visible? ","1366691595","50","0","0");
INSERT INTO forum_posts VALUES("539","1","Well, If you\'re on the last page, the last option will go away and if your on the first page, it won;t show,","1366691672","50","0","0");
INSERT INTO forum_posts VALUES("540","2","^ Above, yeah I noticed that..\n\n\nAlso, editing a post should redirect you to the page that the post was in..","1366691716","50","0","0");
INSERT INTO forum_posts VALUES("543","1","Iight lemme fix that now.testasdf","1366691975","50","0","0");
INSERT INTO forum_posts VALUES("544","2","Clicking on the thread &quot;Ideas&quot; on the Latest Activity box thingy, I was redirected to the 3rd page, not the last page..","1366692378","50","0","0");
INSERT INTO forum_posts VALUES("545","1","I know I gotta fix that.","1366692459","50","0","0");
INSERT INTO forum_posts VALUES("546","2","Lol now it\'s working?\n\nAlright I\'ll leave this strangeness to you. Thanks :)","1366692509","50","0","0");
INSERT INTO forum_posts VALUES("547","1","lol np, Hey I\'ma go to sleep. ttyl!","1366692561","50","0","0");
INSERT INTO forum_posts VALUES("548","2","Alright good night :)\n\nJust wondering if you\'ve done the post ID thing?","1366692641","50","0","0");
INSERT INTO forum_posts VALUES("552","1","I\'m working on that now","1366732838","50","0","0");
INSERT INTO forum_posts VALUES("554","6","I have time to read all of this tomorrow, but from what I\'ve seen so far, I think that we should keep one admin if he\'s active. It\'s like what you said with Testers; it\'s unfair to kick them just like that. Sure, for inactivity, Dayo should be out soon, but we should keep one.","1366740970","28","0","0");
INSERT INTO forum_posts VALUES("556","6","There\'s still a lot of things to do here, and you wanted to move on Oreo.","1366741083","50","0","0");
INSERT INTO forum_posts VALUES("557","2","Well what Oreo is doing is important. I\'ll wait till you\'re done before giving anymore suggestions regarding posting.","1366742779","50","0","0");
INSERT INTO forum_posts VALUES("558","2","We\'re not really kicking them like the examples with the testers. I want to demote them to their proper ranks, not kick them completely by firing them.","1366742944","28","0","0");
INSERT INTO forum_posts VALUES("565","1","That is extra stuff, you really don\'t need to number the post. That is extra stuff that you can add in, so anything else?","1366753853","50","0","0");
INSERT INTO forum_posts VALUES("566","1","They why did we kick Ary?","1366753921","28","0","0");
INSERT INTO forum_posts VALUES("568","2","After my exam tomorrow, I\'m able to fully test all of the functions of the forums and give you a big list of what to fix and some suggestions. ","1366755372","50","0","0");
INSERT INTO forum_posts VALUES("569","2","We simply wanted to demote him because of his inactivity. But how he reacted and conversation with him made me want to start a conversation about firing him. He quit anyways.","1366755450","28","0","0");
INSERT INTO forum_posts VALUES("583","4","@links~ i edited the first page for you.\n\nAlso, on a slightly off-topic note, I think we need to push to hire more members like lawyers. Because we are getting quite close to release, and we really should have any legal requirements sorted before we finish. \nOther positions could be:\n•finance (probably not needed cos we\'re not doing much with the money)\n•advertisers (someone who really knows where the best places to advertise our site are)\n•translators (hopefully we can get some Japanese fans; something NA &amp; SA seem to be lacking)\n•.....\nSo? Is it a good idea?","1366825939","27","0","0");
INSERT INTO forum_posts VALUES("585","1","[quote]@links~ i edited the first page for you.\n\nAlso, on a slightly off-topic note, I think we need to push to hire more members like lawyers. Because we are getting quite close to release, and we really should have any legal requirements sorted before we finish. \nOther positions could be:\n•finance (probably not needed cos we\'re not doing much with the money)\n•advertisers (someone who really knows where the best places to advertise our site are)\n•translators (hopefully we can get some Japanese fans; something NA &amp; SA seem to be lacking)\n•.....\nSo? Is it a good idea?[/quote]Some of these things are need, the staff and members can do the advertising, webmaster handle the financing, Mods and Gms can translate, we just need more staff members that are bilingual and lawyers aren\'t really need, they can\'t sue since we don\'t have any of their files. And lawyers are expensive, so it will cost a lot, and I don\'t have money to waste for lawyers like that.","1366837493","27","0","0");
INSERT INTO forum_posts VALUES("588","6","We can always ask someone to translate Jap.\n\nI won\'t be learning it for another year or so, that\'s for sure.","1366842271","27","0","0");
INSERT INTO forum_posts VALUES("590","6","[quote]I think we should reset everything completely. \nI also don\'t want most of the staff to see some of our threads. Everyone who currently has access here is the people I trust most. [/quote]\n\nWell, ok then. I see where you\'re coming from, and I agree.","1366842395","49","0","0");
INSERT INTO forum_posts VALUES("593","1","I was gonna reset everything anyway. If you haven\'t notice we have less then 10 topics and the topic ids are in the 50s","1366842509","49","0","0");
INSERT INTO forum_posts VALUES("595","1","You also have Google translator ","1366842636","27","0","0");
INSERT INTO forum_posts VALUES("608","1","Okay, I updated the paging, now if you\'re not on the last page they\'re will be an Arrow and the word Last. I think everyone should know from that that they\'re not on the last page. \nAnd I\'m going to add something to the latest activity so when you click on it will bring you to the most recent post","1366847514","50","0","0");
INSERT INTO forum_posts VALUES("619","6","[quote]Okay, I updated the paging, now if you\'re not on the last page they\'re will be an Arrow and the word Last. I think everyone should know from that that they\'re not on the last page. \nAnd I\'m going to add something to the latest activity so when you click on it will bring you to the most recent post[/quote]\n\nFinally. Lol.","1366853014","50","0","0");
INSERT INTO forum_posts VALUES("622","1","Why didn\'t you say anything if it bothered you [groggy]","1366854346","50","0","0");
INSERT INTO forum_posts VALUES("625","6","[quote]Why didn\'t you say anything if it bothered you [groggy][/quote]\n\nI forgot about that part. Lol.","1366855558","50","0","0");
INSERT INTO forum_posts VALUES("669","2","Deleted spam, testing not necessary anymore for now :P","1366867472","50","0","0");
INSERT INTO forum_posts VALUES("666","6","May I test too? ","1366866701","50","0","0");
INSERT INTO forum_posts VALUES("672","2","It looks like someone deleted my thread (grumpy)","1366907289","57","0","0");
INSERT INTO forum_posts VALUES("673","2","[img]http://i.imgur.com/tJopx.png[/img]\n\n[b]++ ~ Welcome to One-Piece Arena! ~ ++ [/b]\n\n[quote=Introduction]Hello, to One Piece Arena, the Official Project! Is about 6 months that we are at N-B doing this project and we are all finished. Now, we just need one more thing, the [b]Testers[/b].[/quote]\n[quote=Rules]\n1. Follow all N-A/N-B Rules.\n2. Do not spam. flame, complain or anything else.\n3. Do not ask when this will be released, because we certainly don\'t know (yet) [/quote]\n[quote=Links]\n[b] ONE PIECE SCREEN SHOTS:\n[url=http://www.onepiece.de/media/pictures/?dir=Episoden]HD Screenshots[/url]\n[b] ONE PIECE OFFICIAL BOARDS:\n[url=http://w11.zetaboards.com/Pirate_Boards/index]One Piece-Boards ~ Take a Look[/url]\n[b]HQ EPISODES:\n[url=http://www.watchcartoononline.com/anime/one-piece-english-dubbed-guide]HQ One Piece Episodes[/url]\n[b] HQ Face Pics, Skills, and Information: [/b]\n[url=http://onepiece.wikia.com/wiki/Main_Page]One Piece Wikipedia[/url]\n[b] Download One-Piece Episodes HQ: [/b]\n[url=http://download-komik.blogspot.com/2011/04/download-one-piece-all-episode-english.html]Download HQ One-Piece Episodes[/url]\n[B]HQ Images/Stocks: [/b]\n[url=http://randomc.net/category/one-piece]HQ Images/Stocks[/url]\n[b] Goals: [/b]\n[url=http://i44.tinypic.com/ftnb5d.png]Missions[/url], [url=http://i43.tinypic.com/344rqs7.png]Start Page[/url], [url=http://i43.tinypic.com/2iaaqll.png]Character Selection[/url], and [url=http://i42.tinypic.com/9a3eas.png]In-Game[/url][/QUOTE]\n\n[quote=Worker Application]\nWe\'re currently looking for the following positions:\nTesters\nCoders\nLawyer\n\nIf you want to apply,  first make an account [url=http://w11.zetaboards.com/Pirate_Boards/index/ ]here[/url]. After you\'ve registered to our forum, click  [url=http://w11.zetaboards.com/Pirate_Boards/forum/3553120/]here[/url] and check out [url=http://w11.zetaboards.com/Pirate_Boards/topic/8309423/1/#new ]&quot;How to apply&quot;[/url]. Follow the instructions and post your application by making a new thread [url=http://w11.zetaboards.com/Pirate_Boards/forum/3553155/]here[/url]\nThanks.\n[/quote]","1366907298","57","0","0");
INSERT INTO forum_posts VALUES("674","2","[IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/Banner3-1.png[/IMG]\n\n[b] Character Templates (Photoshop): [/B]\n[url=http://www.mediafire.com/?8kqhjoo4c21zpdc]4 Skills Template[/url]\n[url=http://www.mediafire.com/?oaaksoa1anf1iwg]5 Skills Template[/url]\n[url=http://www.mediafire.com/?cw14p5dicnjzcwc]6 Skills Template[/url]\n\n[b] Character Templates (GIMP): [/B]\n[url=http://www.mediafire.com/?ag6788j728k0h24]4 Skills Template[/url]\n[url=http://www.mediafire.com/?opr6zdovthc7j26]5 Skills Template[/url]\n[url=http://www.mediafire.com/?15o0p0r1tlr94wy]6 Skills Template[/url]\n\n[b] Mission Templates (Photoshop): [/b]\n[url=http://www.mediafire.com/?r5221g1h7s1r6yo]Mission In Template[/url]\n[url=http://www.mediafire.com/?b82jym5b4ajg7be]Mission Out Template[/url]\n\n[b] Mission Templates (GIMP): [/b]\n[url=http://www.mediafire.com/?wy9mcm50pjjd53q]Mission In Template[/url]\n[url=http://www.mediafire.com/?c0fm1wcd793rb73]Mission Out Template[/url]\n\n[b]Player Card Template (Photoshop): [/b]\n[url=http://www.mediafire.com/?8975cw10669fn1x]Player Card Template[/url]\n\n\n[quote=Energy System:]\n[IMG]http://i1195.photobucket.com/albums/aa389/bqnkaidato/GFX/Physical.png[/IMG] ~ Physical - Attacks with Punches or Legs ~                                          \n[IMG]http://i1195.photobucket.com/albums/aa389/bqnkaidato/GFX/DevilF.png[/IMG] ~ Devil Fruit - Devil Fruits Powers like Luffy \n[IMG]http://i1195.photobucket.com/albums/aa389/bqnkaidato/GFX/Unique.png[/IMG] ~ Special - Skills Unique for a Character ~         \n[IMG]http://i1195.photobucket.com/albums/aa389/bqnkaidato/GFX/Weapon.png[/IMG] ~ Weapon - Skill that has an weapon to attack or defense ~\n[IMG]http://i1195.photobucket.com/albums/aa389/bqnkaidato/GFX/Random.png[/IMG] ~ Random Energy ~[/quote]                        \n\n[quote=Class List]\nAttack: Physical/Weapon/Devil Fruit/Mental\nRange: Melee/Ranged\nTime: Instant/Action/Control\nAdditional Classes: Affliction/Unique\n[/quote]\n\n[quote=Official Ranks]\n[img]http://i1128.photobucket.com/albums/m493/dracodino300/th_PirateApprenticecopy.png[/img] 1-5: Pirate Apprentice\n[img]http://i1128.photobucket.com/albums/m493/dracodino300/th_Piratecopy.png[/img] 6-10: Pirate\n[img]http://i1128.photobucket.com/albums/m493/dracodino300/th_MarineSoldiercopy.png[/img] 11-15: Marine Soldier\n[img]http://i1128.photobucket.com/albums/m493/dracodino300/th_Commodorecopy.png[/img] 16-20: Commodore\n[img]http://i1128.photobucket.com/albums/m493/dracodino300/th_BountyHuntercopy.png[/img] 21-25: Bounty Hunter\n[img]http://i1128.photobucket.com/albums/m493/dracodino300/th_PirateCaptaincopy.png[/img] 26-30: Pirate Captain\n31-35: [b]Marine Commander [/b]\n[img]http://i1128.photobucket.com/albums/m493/dracodino300/th_Supernovacopy.png[/img] 36-40: Super Nova\n[img]http://i1128.photobucket.com/albums/m493/dracodino300/th_Shichibukaicopy.png[/img] 41-45: Shichibukai\n[img]http://i1128.photobucket.com/albums/m493/dracodino300/th_Admiralcopy.png[/img] 46-50: Admiral\n#1 Ladder Rank: [b]Pirate King [/b]\n[/quote]\n\n[quote=OP-A Task Types:]\n1-5: Pirate Apprentice: [b]Pratice Session[/b]\n6-10: Pirate: [b]Pirate Task [/b]\n11-15: Marine Soldier: [b]Hard Task[/b]\n16-20: Commodore: [b]Master Task [/b]\n21-25: Bounty Hunter: [b]Hunter Task[/b]\n26-30: Pirate Captain: [b]Pirate Leader Task Task [/b]\n31-35: Marine Commander: [b]Commander Task[/b]\n36-40: Super Nova: [b]Super Nova Task [/b]\n41-45: Shichibukai: [b]Warlord Task [/b]\n[/quote]\n\n[quote=Other Informations:]\n[url=http://w11.zetaboards.com/Pirate_Boards/topic/7808535/]Cick to See all Player Cards[/url]\n[url=http://w11.zetaboards.com/Pirate_Boards/topic/7808552/1/]Click to See all Site Backgrounds[/url]\n[/quote]\n\n[i]One Piece-Arena, Your #1 One Piece Online Multiplayer Game\n","1366907320","57","0","0");
INSERT INTO forum_posts VALUES("675","2","                               [b] ++ ~ ~ GFX Place ~ ~ ++ [/b]\n\nSigs:\n\n[img]http://i1176.photobucket.com/albums/x332/sanninsasuke/OnePiece-ArenaSig.png[/img]\n[img]http://i1176.photobucket.com/albums/x332/sanninsasuke/OnePiece-Arena2.png[/img]\n[img]http://img204.imageshack.us/img204/5571/sdkjihfd.png[/img]\n[img]http://i731.photobucket.com/albums/ww318/umba_bucket/Staff-Sigcopy-1.png[/img]\n[img]http://i731.photobucket.com/albums/ww318/umba_bucket/Franky-Sig.png[/img]\n[img]http://i1245.photobucket.com/albums/gg596/Goo36/retewrtwert.jpg[/img]\n[img]http://i731.photobucket.com/albums/ww318/umba_bucket/Official-Project-1.png[/img]\n\nIcons:\n\n[img]http://i928.photobucket.com/albums/ad125/Triggerpulling/zoroic.png[/img]  [img]http://i928.photobucket.com/albums/ad125/Triggerpulling/zoroic2.png[/img] [img] http://desmond.imageshack.us/Himg190/scaled.php?server=190&amp;filename=fdgbfdh.png&amp;res=gal[/img] [img]http://desmond.imageshack.us/Himg441/scaled.php?server=441&amp;filename=97593738.png&amp;res=gal[/img] [img]http://desmond.imageshack.us/Himg17/scaled.php?server=17&amp;filename=45507063.png&amp;res=gal[/img] [IMG]http://i731.photobucket.com/albums/ww318/umba_bucket/Op-Icon-1.png[/IMG] [IMG]http://i731.photobucket.com/albums/ww318/umba_bucket/Op-Icon.png[/IMG] [img]http://desmond.imageshack.us/Himg263/scaled.php?server=263&amp;filename=gfhfgsh.png&amp;res=gal [/img] [img] http://desmond.imageshack.us/Himg855/scaled.php?server=855&amp;filename=gdasgfdsh.png&amp;res=gal[/img] [img]http://desmond.imageshack.us/Himg267/scaled.php?server=267&amp;filename=gdsafads.png&amp;res=gal [/img] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/Luffy-2.png[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/Untitled-1-2.png[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/Nami.png[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/Usopp.png[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/Sanji2.png[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/Robin.png[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/SAldk.png[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/ZoroXLuffy.png[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/Zorororo.png[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/Sanjiii.png[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/Sanji3.png[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/Robin-1.png[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/Law.png[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/Dile.png[/IMG]\n\nTags:\n\nBy Vinny: \n[img]http://i.imgur.com/Myd0y.png[/img] [img]http://i.imgur.com/EV7vd.png[/img] [img]http://i.imgur.com/juEF0.png[/img] [img]http://i.imgur.com/96hwb.png[/img] [img]http://i.imgur.com/wRCNi.png[/img] [img]http://i.imgur.com/e74Ve.png[/img] [img]http://i.imgur.com/umjF2.png[/img] [img]http://i.imgur.com/GomUY.png[/img] \n\nOther: \n[img]http://img189.imageshack.us/img189/7172/baords.png[/img] [IMG]http://i1245.photobucket.com/albums/gg596/Goo36/fsadfasdfsa-1.jpg[/IMG] [IMG]http://i1245.photobucket.com/albums/gg596/Goo36/sdgfdgds.jpg[/IMG] [IMG]http://i1245.photobucket.com/albums/gg596/Goo36/dsafsdf.jpg[/IMG] \n\nAvys:\n\n[IMG]http://i731.photobucket.com/albums/ww318/umba_bucket/CC%20Pics/Avy-6.png[/IMG] [IMG]http://i731.photobucket.com/albums/ww318/umba_bucket/CC%20Pics/Avy-4.png[/IMG] [IMG]http://i731.photobucket.com/albums/ww318/umba_bucket/CC%20Pics/Avy-3.png[/IMG] [IMG]http://i731.photobucket.com/albums/ww318/umba_bucket/CC%20Pics/Avy-2.png[/IMG] [IMG]http://i731.photobucket.com/albums/ww318/umba_bucket/CC%20Pics/Avy-1.png[/IMG] [IMG]http://i731.photobucket.com/albums/ww318/umba_bucket/CC%20Pics/Avy-9.png[/IMG] [IMG]http://i731.photobucket.com/albums/ww318/umba_bucket/CC%20Pics/Avy-8.png[/IMG] [IMG]http://i731.photobucket.com/albums/ww318/umba_bucket/CC%20Pics/Avy-7.png[/IMG] [IMG]http://i947.photobucket.com/albums/ad311/klajdifan/Icons/480px-Zoroportrait.jpg[/IMG] [IMG]http://i947.photobucket.com/albums/ad311/klajdifan/Icons/Brookportrait.jpg[/IMG] [IMG]http://i947.photobucket.com/albums/ad311/klajdifan/Icons/548px-Roger_in_his_youth.jpg[/IMG] [IMG]http://i947.photobucket.com/albums/ad311/klajdifan/Icons/480px-Franky_new_hair_anime_2.jpg[/IMG] [IMG]http://i947.photobucket.com/albums/ad311/klajdifan/Icons/one_piece_597__the_cover_by_lord_nadjib-d2xjgt4.jpg[/IMG] [img]http://www.naruto-arena.com/images/avatars/uploaded/4277579.jpg?1327425992[/img] [img]http://i1104.photobucket.com/albums/h330/OceanMaster1998/One%20Piece%20Characters/1.png[/img] [img]http://desmond.imageshack.us/Himg204/scaled.php?server=204&amp;filename=jdghf.jpg&amp;res=gal[/img] [img]http://desmond.imageshack.us/Himg819/scaled.php?server=819&amp;filename=dfse.jpg&amp;res=gal[/img] [IMG]http://i731.photobucket.com/albums/ww318/umba_bucket/Avys-7.jpg[/IMG] [IMG]http://i731.photobucket.com/albums/ww318/umba_bucket/Avys-6.jpg[/IMG] [IMG]http://i731.photobucket.com/albums/ww318/umba_bucket/Avys-5.jpg[/IMG] [IMG]http://i731.photobucket.com/albums/ww318/umba_bucket/Avys-3.jpg[/IMG] [IMG]http://i731.photobucket.com/albums/ww318/umba_bucket/Avys-2.jpg[/IMG] [IMG]http://i731.photobucket.com/albums/ww318/umba_bucket/Avys-1.png[/IMG] [IMG]http://i731.photobucket.com/albums/ww318/umba_bucket/Avys-8.jpg[/IMG] [img]http://i.imgur.com/zsYu6.jpg[/img] [img]http://i.imgur.com/iNoZ0.jpg[/img] [img]http://i.imgur.com/QMbOt.jpg[/img] [img]http://desmond.imageshack.us/Himg826/scaled.php?server=826&amp;filename=fdsghfdjhs.jpg&amp;res=gal[/img] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/1-8.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/2-6.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/3-7.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/4-6.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/5-7.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/6-7.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/7-5.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/8-5.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/9-5.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/10-5.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/11-3.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/12-2.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/13-2.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/14-2.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/15-2.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/16-2.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/17-3.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/18-3.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/19-3.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/20-1.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/21-3.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/22-3.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/23-2.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/24-2.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/25-2.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/26.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/27-2.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/28-2.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/29-2.jpg[/IMG] [IMG]http://i1245.photobucket.com/albums/gg596/Goo36/zxvxc-1.jpg[/IMG] [IMG]http://i1245.photobucket.com/albums/gg596/Goo36/xfdbxc-1.jpg[/IMG] [IMG]http://i1245.photobucket.com/albums/gg596/Goo36/vdxfsada.jpg[/IMG] [IMG]http://i1245.photobucket.com/albums/gg596/Goo36/sdgasf.jpg[/IMG] [IMG]http://i1245.photobucket.com/albums/gg596/Goo36/gfbnfgd-1.jpg[/IMG] [IMG]http://i1245.photobucket.com/albums/gg596/Goo36/dzfgasdfg.jpg[/IMG] [IMG]http://i1245.photobucket.com/albums/gg596/Goo36/xvbncgfd.jpg[/IMG] [IMG]http://i1245.photobucket.com/albums/gg596/Goo36/sdafs.jpg[/IMG] [IMG]http://i1245.photobucket.com/albums/gg596/Goo36/werytete.jpg[/IMG] [IMG]http://i1245.photobucket.com/albums/gg596/Goo36/czxcvxvs.jpg[/IMG] [IMG]http://i1245.photobucket.com/albums/gg596/Goo36/fsadfasdfasd.jpg[/IMG] [IMG]http://i1245.photobucket.com/albums/gg596/Goo36/xdfgadsfgsd.jpg[/IMG] [IMG]http://i1245.photobucket.com/albums/gg596/Goo36/fsdaf.jpg[/IMG] [IMG]http://i1245.photobucket.com/albums/gg596/Goo36/asdfsdfs.jpg[/IMG] [IMG]http://i1245.photobucket.com/albums/gg596/Goo36/fasdfasd-1.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/30-3.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/31-3.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/32-3.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/33-1.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/34-1.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/35-1.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/36-1.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/37-1.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/38-1.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/39-1.jpg[/IMG] [IMG]http://i1245.photobucket.com/albums/gg596/Goo36/x.jpg[/IMG] [IMG]http://i1245.photobucket.com/albums/gg596/Goo36/fsadfasdfas-1.jpg[/IMG] [IMG]http://i1245.photobucket.com/albums/gg596/Goo36/fasdfsdfas.jpg[/IMG] [IMG]http://i1245.photobucket.com/albums/gg596/Goo36/fasdfsad-1.jpg[/IMG] [IMG]http://i1245.photobucket.com/albums/gg596/Goo36/fasdfs.jpg[/IMG] [IMG]http://i1245.photobucket.com/albums/gg596/Goo36/asdfrasd.jpg[/IMG] [IMG]http://i1245.photobucket.com/albums/gg596/Goo36/dfasfa.jpg[/IMG] [IMG]http://i1220.photobucket.com/albums/dd442/metalic1/ghdfgdsf.jpg[/IMG] [IMG]http://i1220.photobucket.com/albums/dd442/metalic1/gfdshfdhdf.jpg[/IMG] [IMG]http://i1220.photobucket.com/albums/dd442/metalic1/gfdsdfsdf.jpg[/IMG] [IMG]http://i1220.photobucket.com/albums/dd442/metalic1/gfdsasdas.jpg[/IMG] [IMG]http://i1220.photobucket.com/albums/dd442/metalic1/gfdhjg.jpg[/IMG] [IMG]http://i1220.photobucket.com/albums/dd442/metalic1/gfdhgfhsdf.jpg[/IMG] [IMG]http://i1220.photobucket.com/albums/dd442/metalic1/gfdhgfhfsd.jpg[/IMG] [IMG]http://i1220.photobucket.com/albums/dd442/metalic1/gfdhgfhdf.jpg[/IMG] [IMG]http://i1220.photobucket.com/albums/dd442/metalic1/gfdhgfh.jpg[/IMG] [IMG]http://i1220.photobucket.com/albums/dd442/metalic1/gfdhg.jpg[/IMG] [IMG]http://i1220.photobucket.com/albums/dd442/metalic1/gfdhfhgfh.jpg[/IMG] [IMG]http://i1220.photobucket.com/albums/dd442/metalic1/gfdgsdgsdf.jpg[/IMG] [IMG]http://i1220.photobucket.com/albums/dd442/metalic1/gfdgfdhfg.jpg[/IMG] [IMG]http://i1220.photobucket.com/albums/dd442/metalic1/gfdfsdfsd.jpg[/IMG] [IMG]http://i1220.photobucket.com/albums/dd442/metalic1/gdssfds.jpg[/IMG] [IMG]http://i1220.photobucket.com/albums/dd442/metalic1/gdfsfsdfdsfds.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/1-12.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/3-10.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/4-9.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/5-10.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/6-9.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/7-7.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/8-7.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/9-7.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/10-7.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/12-4.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/2-11.jpg[/IMG] [IMG]http://i1245.photobucket.com/albums/gg596/Goo36/fsadfas.jpg[/IMG] [IMG]http://i1245.photobucket.com/albums/gg596/Goo36/fsdfsd-3.jpg[/IMG] [IMG]http://i1245.photobucket.com/albums/gg596/Goo36/dasdASD-8.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/1-15.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/12-5.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/111.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/1111.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/4345.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/111114.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/Untitled-1-3.jpg[/IMG] [IMG]http://i1172.photobucket.com/albums/r563/iGoBlaze/Untitled-2-1.jpg[/IMG] ","1366907354","57","0","0");
INSERT INTO forum_posts VALUES("676","1","Lol, my bad, I pressed the wrong button","1366920149","57","0","0");
INSERT INTO forum_posts VALUES("719","1","I edited the thing, now it should redirect you and the thing to the latest page with no glitches, if there is a problem pm me.","1366929070","50","0","0");
INSERT INTO forum_posts VALUES("683","2","No problem. I\'m going to gather some gfxers to help with new images.\n\nWe should start with making new links and introductions.\n\nI think I should post this thread on zetaboards aswell? ","1366922158","57","0","0");
INSERT INTO forum_posts VALUES("718","1","If you wan\'t I really don\'t see a point of a n-a topic; n-a is dead as fuck","1366928983","57","0","0");
INSERT INTO forum_posts VALUES("740","4","What about the first thing you wrote on this topic: the other 2 themes?","1367020882","50","0","3");
INSERT INTO forum_posts VALUES("739","1","What else needs to be done or what else do you guys want","1367011024","50","0","3");
INSERT INTO forum_posts VALUES("741","1","Ima work on them later, I\'m going finish one theme and make sure everything is working first.","1367020939","50","0","3");
INSERT INTO forum_posts VALUES("794","6","Who the heck is Ipsum? That\'s like one gay ass name. ","1367621844","57","0","8");
INSERT INTO forum_posts VALUES("795","1","So you\'re calling a language gay? Also, Stay on topic.[dry] And make the avatars icons.","1367621927","57","0","8");
INSERT INTO forum_posts VALUES("797","1","[b]Staff Section Rules: [/b]\n\nSame thing as [url=http://forum.pirate-zone.com/?area=forum&amp;s=topic&amp;t=1]These right here[/url] but more added.\n11. Do not ignore a staff member\'s request if they\'re a higher ranking then you. \n12. Do not make your topic names exactly what you\'re talking about so members that ponder around won\'t know what we\'re talking about.","1367624881","63","0","0");
INSERT INTO forum_posts VALUES("798","6","#12 is weird. Doesn\'t make sense to me.\n\nMaybe what you\'re saying is &quot;Don\'t make misleading Topic Names&quot;?","1367656749","63","0","6");
INSERT INTO forum_posts VALUES("799","6","[quote]So you\'re calling a language gay? Also, Stay on topic.[dry] And make the avatars icons.[/quote]\n\nWhat do you mean? We\'re not aloud avies? Why? Let\'s have a vote, like we said earlier. You doing things the way you like \'em isn\'t the best at times.\n\n@Topic:\n\nWe need to delete like 75% of that.","1367656861","57","0","0");
INSERT INTO forum_posts VALUES("800","3","No. What he means is, if you\'re planning to create a topic about new characters, don\'t entitle the Thread as &quot;New Characters&quot;. Instead make a jargon title such as &quot;Blue Waffles&quot; so that only people who are working on those characters will know about them. If you have ever stalked staff on NB or SB, they have a few examples. \n\nAlso, the Forum rules topic doesn\'t work [confused]","1367665411","63","0","6");
INSERT INTO forum_posts VALUES("801","1","[quote]No. What he means is, if you\'re planning to create a topic about new characters, don\'t entitle the Thread as &quot;New Characters&quot;. Instead make a jargon title such as &quot;Blue Waffles&quot; so that only people who are working on those characters will know about them. If you have ever stalked staff on NB or SB, they have a few examples. \n\nAlso, the Forum rules topic doesn\'t work [confused][/quote]This and yeah, there is no topic 1 id","1367687078","63","0","0");
INSERT INTO forum_posts VALUES("802","1","Look at the forum. Icons look much better with this forum and icons are higher quality then avatars. They are also bigger,so you can see the actually thing.","1367687176","57","0","8");
INSERT INTO forum_posts VALUES("803","4","Why do that though? If its in the staff section, other members can\'t see it anyway.","1367745852","63","0","6");
INSERT INTO forum_posts VALUES("804","4","But they don\'t fit with the ingame designs.","1367746126","57","0","8");
INSERT INTO forum_posts VALUES("805","1","If you go on your profile you can see the topic name ","1367748079","63","0","6");
INSERT INTO forum_posts VALUES("806","1","Resize Em then links has the code for that ","1367748178","57","0","8");
INSERT INTO forum_posts VALUES("807","1","This guide was created for you and the other people who browse it to understand and swim freely through the Pirate Zone forums community within the rules\' limitation and, as such, everyone is expected to read it and follow it thoroughly or else the risk of getting banned will be rather high. \n\n\n\nForum Rules: \n\n1. Do not spam, flame nor harass others while on the forum. \n2. Do not post multiple comments one after another. \n3. Do not post pornographic/racist/disturbing material, the same goes for linking to such materials. \n4. Do not rip avatars/signatures/names without the user\'s approval. \n5. Do not act like a moderator/admin if you are not one and do not impersonate any staff member (back-seat modding). \n6. Do not evade your bans, it is a violation of rules of the highest grade and the ones caught will be severely punished. \n7. Do not trade accounts on the forums. \n8. Do not cause unnecessary drama (trolling). \n9. Do not advertise or promote other websites on the forum. \n10. Do not ignore a staff member\'s request. \n\n\nScamming: \n\nScamming is when another person persuades or intimidates you to give them your account information so he/she can steal it. Many cases of scamming exist in many sites and we CAN help you get your account back, the problem is who you are and what you do. If you have helped and done a lot for our community, you shall be rewarded, if all you do is play games all day and incidentally get scammed, we cannot help you, however, even if you have done a lot for the community, but fail to provide solid proof, then it is all to naught. It does sound a little biased, help us and we\'ll help you. If you do believe you are getting scammed, report the user and we will take care of it. \n\nWarning System: \n\nThe warning system isn\'t complex and is an easy way to keep track of your warnings. If you break the rules and you get reported, you will receive a warning from a staff member. At three warnings, you will receive an one week ban. If you already had a ban, but receive another three warnings afterwards, you will receive an one month ban. The next ban you receive after that will be a permanent one which is irreversible. Bans can not only be earned by having three warnings, if you have abusively broken a rule so much that the staff feels like they need to shut you down, you will be banned for as much time as it is needed. Remember this, a good user will always know how to keep track of their posts and warnings. \n\nReporting System: \n\nThe reporting system is fairly easy and effective. If you see that an user has broken a rule, you are free to either report them or look away, however, reporting rule breakers will give you our favor too. We always receive reports and keep track of them, if the report is nothing more than just spam or flame, you will receive your warnings accordingly. Flooding will result in a one week ban, continuing after that will result in a permanent ban. Bad/sarcastic reports don\'t usually get warned, but when you excessively do it, you will be punished. Trust the staff and we will trust you. ","1367791225","1","0","1");



DROP TABLE forum_reports;

CREATE TABLE `forum_reports` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `post_id` int(100) NOT NULL,
  `account_id` int(100) NOT NULL,
  `reporter_id` int(100) NOT NULL,
  `date` varchar(200) NOT NULL,
  `reason` text NOT NULL,
  `resolved` int(10) NOT NULL DEFAULT '0',
  `content` text NOT NULL,
  `outcome` text NOT NULL,
  `actioned` int(10) NOT NULL DEFAULT '0',
  `action_id` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

INSERT INTO forum_reports VALUES("1","33","2","5","1362768278","he spammed","0","[quote]asasf[/quote]asdfasdf","This report has been closed with no action.\n\n","1","5");
INSERT INTO forum_reports VALUES("2","0","2","12","1362964175","Sexual Harassment ","0","","This report has been closed with no action.\n\nWhat?","1","2");
INSERT INTO forum_reports VALUES("3","73","5","3","1363122849","Challenge accepted","0","Moved to project forum.\n\n@Links, come at me bro.","Accused issued with a temporary ban untill March 13, 2013, 4:15 PM\n\nChallenge accepted :D","1","3");
INSERT INTO forum_reports VALUES("4","49","2","2","1363137954","test","0","I am going to disable the reputation system for a while and fix it or add another system. It should be complete in 2-3 months. If you have any questions, [url=http://forum.pirate-zone.com/?area=mail&amp;amp;mode=new&amp;amp;user=2]pm me.[/url]","","0","0");
INSERT INTO forum_reports VALUES("5","231","1","1","1364623891","dasd","0","[quote]ssd[/quote]sdsdsdzxzxxzxzxzzxzx","","0","0");
INSERT INTO forum_reports VALUES("6","231","1","1","1364624839","test","0","[quote]ssd[/quote]sdsdsdzxzxxzxzxzzxzx","","0","0");
INSERT INTO forum_reports VALUES("7","349","1","1","1366233515","test","0","[IMG]http://i1090.photobucket.com/albums/i377/CemPwnz/Kyouki/KyoukiMainBannerNewEra.png[/IMG]\n\n\n[b]Post Away &amp;lt;3[/b]\n\n[url=http://www.naruto-boards.com/868200][IMG]http://i1149.photobucket.com/albums/o590/xyz_madara_tobi/Gifs/Main.gif[/IMG][/url] [url=http://www.naruto-arena.com/clan/profile/383862/][IMG]http://i1149.photobucket.com/albums/o590/xyz_madara_tobi/Gifs/KyoukiLove.gif[/IMG][/url] [url=http://www.naruto-boards.com/867950][img]http://i1149.photobucket.com/albums/o590/xyz_madara_tobi/Gifs/Recruitment.gif[/img][/url] ","Accused issued with a temporary ban untill April 25, 2013, 7:12 PM\n\n","1","1");
INSERT INTO forum_reports VALUES("8","666","6","1","1366931348","test","0","May I test too? ","Accused issued with a temporary ban untill May 2, 2013, 6:09 PM\n\n","1","1");
INSERT INTO forum_reports VALUES("9","0","2","2","1367172304","He\'s too amazing to be here","0","","","0","0");



DROP TABLE forum_reputation;

CREATE TABLE `forum_reputation` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `post_id` int(100) NOT NULL,
  `account_id` int(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

INSERT INTO forum_reputation VALUES("1","5","3");
INSERT INTO forum_reputation VALUES("2","1","3");
INSERT INTO forum_reputation VALUES("3","11","2");
INSERT INTO forum_reputation VALUES("4","9","5");
INSERT INTO forum_reputation VALUES("5","11","5");
INSERT INTO forum_reputation VALUES("6","5","6");
INSERT INTO forum_reputation VALUES("7","13","2");
INSERT INTO forum_reputation VALUES("8","12","2");
INSERT INTO forum_reputation VALUES("9","33","5");
INSERT INTO forum_reputation VALUES("10","37","2");
INSERT INTO forum_reputation VALUES("11","14","10");
INSERT INTO forum_reputation VALUES("12","39","2");
INSERT INTO forum_reputation VALUES("13","2","5");
INSERT INTO forum_reputation VALUES("14","2","11");
INSERT INTO forum_reputation VALUES("15","19","5");
INSERT INTO forum_reputation VALUES("16","15","5");
INSERT INTO forum_reputation VALUES("17","16","5");
INSERT INTO forum_reputation VALUES("18","17","5");
INSERT INTO forum_reputation VALUES("19","18","5");
INSERT INTO forum_reputation VALUES("20","20","5");
INSERT INTO forum_reputation VALUES("21","53","3");
INSERT INTO forum_reputation VALUES("22","48","3");
INSERT INTO forum_reputation VALUES("23","73","3");
INSERT INTO forum_reputation VALUES("24","68","5");
INSERT INTO forum_reputation VALUES("25","52","5");
INSERT INTO forum_reputation VALUES("26","55","5");
INSERT INTO forum_reputation VALUES("27","57","5");
INSERT INTO forum_reputation VALUES("28","59","5");
INSERT INTO forum_reputation VALUES("29","48","2");



DROP TABLE forum_topics;

CREATE TABLE `forum_topics` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `author_id` int(100) NOT NULL,
  `forum_id` int(100) NOT NULL,
  `date` varchar(100) NOT NULL,
  `sticky` int(10) NOT NULL,
  `locked` int(10) NOT NULL DEFAULT '0',
  `locked_by` int(100) NOT NULL,
  `parent_id` int(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=65 DEFAULT CHARSET=latin1;

INSERT INTO forum_topics VALUES("27","Who should have access to the forums?","2","7","1366842636","0","0","0","0");
INSERT INTO forum_topics VALUES("28","The Wizards Hat!","1","7","1367446024","1","0","1","0");
INSERT INTO forum_topics VALUES("49","Move Characters/Tasks?","6","8","1366842509","0","0","0","0");
INSERT INTO forum_topics VALUES("50","Ideas ","1","3","1367024754","0","0","0","0");
INSERT INTO forum_topics VALUES("57","Offical thread","2","8","1367748178","0","0","0","0");
INSERT INTO forum_topics VALUES("63","Books","1","6","1367748079","1","0","0","0");
INSERT INTO forum_topics VALUES("1","Rules and regulations","1","1","1367791225","0","0","0","0");



DROP TABLE forum_watching;

CREATE TABLE `forum_watching` (
  `id` int(200) NOT NULL AUTO_INCREMENT,
  `account_id` varchar(100) NOT NULL,
  `topic_id` int(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

INSERT INTO forum_watching VALUES("7","6","42");
INSERT INTO forum_watching VALUES("8","1","153");



DROP TABLE online;

CREATE TABLE `online` (
  `id` int(200) NOT NULL AUTO_INCREMENT,
  `time` int(200) NOT NULL,
  `account_id` int(250) NOT NULL,
  `session` varchar(100) NOT NULL,
  `site` varchar(100) NOT NULL,
  `ip` varchar(100) NOT NULL,
  `location` varchar(100) NOT NULL,
  `group_id` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=32014 DEFAULT CHARSET=latin1;

INSERT INTO online VALUES("32013","1367791581","1","557bffc74245628c8e7d7270c2c46598","","76.105.109.168","/acp/?s=system&module=backup","2");
INSERT INTO online VALUES("32005","1367791554","-1","4a497d0844ed88f50d01e0a1322c8d60","","173.206.9.51","/","1");



DROP TABLE ranks;

CREATE TABLE `ranks` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `special` varchar(100) NOT NULL,
  `count` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

INSERT INTO ranks VALUES("1","Chore Boy","0","0");
INSERT INTO ranks VALUES("2","Commander ","1","0");
INSERT INTO ranks VALUES("3","Admiral","1","0");
INSERT INTO ranks VALUES("4","Fishman","0","101");
INSERT INTO ranks VALUES("5","Skypiean","0","201");
INSERT INTO ranks VALUES("6","Demon Guard","0","501");
INSERT INTO ranks VALUES("7","Sea King","0","801");
INSERT INTO ranks VALUES("8","Cyborg","0","2001");
INSERT INTO ranks VALUES("9","Warden","0","6001");
INSERT INTO ranks VALUES("10","Celestial Dragon","0","1000");
INSERT INTO ranks VALUES("11","Hero","0","18000");
INSERT INTO ranks VALUES("12","ĶļŇĢ Daiske","1","");
INSERT INTO ranks VALUES("13","Marine","1","");
INSERT INTO ranks VALUES("14","あなたの友人","1","");
INSERT INTO ranks VALUES("15","The Master of Oceans","1","");
INSERT INTO ranks VALUES("16","Ipsum the White Lightning Dragon","1","");



