<?php


$tpl = $STYLE->open('report/createreport.tpl');
$page_titles = ' Create Report - TeleMedical';
// Is Registration Open?
$logtext = 'Create Report';

// Error reporting:
error_reporting(E_ALL ^ E_NOTICE);


if (isset($_POST['submit'])) {
    if (isset($_POST['title'])) {
        $title = $secure->clean($_POST['title']);
    } else {
        $title = '';
    }
    if (isset($_POST['for_id'])) {
        $for_id = $secure->clean($_POST['for_id']);
    } else {
        $for_id = '';
    }

    if (isset($_POST['creator_id'])) {
        $creator_id = $secure->clean($_POST['creator_id']);
    } else {
        $creator_id = '';
    }
    if (isset($_POST['url'])) {
        $url = $secure->clean($_POST['url']);
    } else {
        $url = '';
    }



    if (!isset($title)) {
        $system->showAlert(L_REGISTER_ERROR_NAME);
        exit("error email");
    }
    if (!isset($for_id)) {
        $system->showAlert(L_REGISTER_ERROR_EMAIL);
        exit("error email");
    }

    if (!isset($creator_id)) {
    $system->showAlert(L_REGISTER_ERROR_PASSWORD);
}

    if (!isset($url)) {
        $system->showAlert(L_REGISTER_ERROR_PASSWORD);
    }




    $insert_user = $reports->createUser($title, $for_id , $new_password, $ip,$activation_code, $activated);
    if ($insert_user) {
        $get_user = $user->getUserByEmail($for_id);
        $id = $get_user['id'];
        echo $id . " asdfasf";
        $permission_group = $db->query("INSERT INTO forum_groups_members (account_id, group_id) VALUES ('" . $get_user['id'] . "','2')");

    } else {
        $system->message(L_ERROR, "There was an error creating your account.", '?account=register', L_CONTINUE);
    }

    if ($system->confdata('activation') == '1') {
        $email_message = str_replace(array('[USER]', '[URL]', '[UID]', '[ACTIVATION_CODE]'), array($title, $siteaddress . '?s=activate&u=' . $get_user['id'] . '&code=' . $activation_code, $get_user['id'], $activation_code), L_REGISTER_EMAIL);
        $system->email($for_id, L_REGISTER_SUBJECT, $email_message);
    }
    $system->message(L_REGISTER, L_REGISTER_MESSAGE, './?account=login', L_LOGIN);
} else {
    // Show Registration Form
    $output .= $STYLE->tags($tpl, array("L_CAPTCHA" => L_CAPTCHA,
        "TOS" => $system->confdata('tos'),
        "L_PASSWORD" => L_PASSWORD,
        "L_CONFIRM_PASSWORD" => L_CONFIRM_PASSWORD,
        "L_NAME" => L_NAME, "L_EMAIL" => L_EMAIL,
        "L_MALE" => L_MALE, "L_FEMALE" => L_FEMALE,
        "L_HIDDEN" => L_HIDDEN,
        "L_TIMEZONE" => L_TIMEZONE,
        "L_GENDER" => L_GENDER,
        "L_LOCATION" => L_LOCATION,
        "L_SUBMIT" => L_SUBMIT,
        "L_ACC_DETAILS" => L_ACCOUNT_DETAILS,
        "L_PROFILE_DETAILS" => L_PROFILE_DETAILS,
        "L_AGREEMENT_STATEMENT" => L_AGREEMENT_STATEMENT,
        "L_AGREEMENT" => L_AGREEMENT

    ));
}

