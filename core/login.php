<?php

$page_titles = 'Login - TeleMed';
if (!$account) {
    if (isset($_POST['submit'])) {
        if (!isset($_POST['email'])) {
            $email = '';
        } else {
            $email = $secure->clean($_POST['email']);
        }

        if (!isset($_POST['password'])) {
            $pass = '';
        } else {
            $pass = $secure->clean(($_POST['password']));
        }

//        if (!isset($_POST['ref'])) {
//            $ref = '';
//        } else {
//            $ref = $secure->clean($_POST['ref']);
//        }

        if ($email && $pass) {
            // CHECK EMAIL AND DEFINE LOGIN SQL
            $check_email = $secure->verify_email($email);
            if ($check_email == 'exist') {

                if(password_verify($pass, $user->getPasswordForUser($email))) {
                    $login = true;
                } else {
                    $login = false;
                }

            } else {
                $system->message(L_ERROR, L_LOGIN_ERROR, '/?account=login', L_CONTINUE);
            }

            if (!$login) {
                $system->message(L_ERROR, L_LOGIN_ERROR, '/?account=login', L_CONTINUE);
            } else {
                $_SESSION['email'] = $_POST['email'];
                $lpip = rand(500, 20000);
                $_SESSION['lpip'] = "$lpip";

                $tokenEmail = urldecode($user->getEmailString($email));


                $update = $user->updateLPIP($email, $lpip);

                if ($update) {
                    // set cookie for an hour
                    $year = time() + 3600;
//                    $_SESSION['email'] = $email;
//                    $lpip = rand(500, 20000);
//                    $_SESSION['lpip'] = $lpip;

                    $_SESSION['email'] = $user->getEmailString($email);


                    setcookie('email', $tokenEmail , $year, '/', '.senior.walentsoftware.com');
                    setcookie('lpip', $_SESSION['lpip'], $year, '/', '.senior.walentsoftware.com');

                    //$system->message(L_LOGIN, L_LOGIN_MESSAGE, './', L_CONTINUE);
                    $system->redirect('./');
                } else {
                    $system->message(L_ERROR, L_LOGIN_ERROR, '/?account=login', L_CONTINUE);
                }

            }


        } else {
            // ERROR MESSAGE FOR NO DETAILS PROVIDED
            $system->message(L_ERROR, L_LOGIN_ERROR, '/?account=login', L_CONTINUE);
        }

    } else {
        // LOGIN FORM
        if (isset($_SERVER['HTTP_REFERER'])) {
            $referer = parse_url($_SERVER['HTTP_REFERER']);
            if (isset($referer['query'])) {
                $referer = '?' . $referer['query'];
                $referer = str_replace(array('acc=login', 'acc=register', 'acc=logout'), '', $referer);
            } else {
                $referer = './';
            }
        } else {
            $referer = './';
        }
        $tpl = $STYLE->open('auth/login.tpl');
        $output .= $STYLE->tags($tpl, array("REF" => $referer, "L_NAME" => '' . L_NAME . ' / ' . L_EMAIL . '', "L_PASSWORD" => L_PASSWORD, "L_LOSTPASSWORD" => L_PASSWORD_LOST_LINK, "L_LOGIN" => L_LOGIN));
    }
}



