<?php
$tpl = $STYLE->open('mail.tpl');
$limiter = $system->confdata('topiclimit');
// Generate Global Menu
$global_menu = $STYLE->getcode('menu', $tpl);
$tpl = str_replace($global_menu, '', $tpl);
if (isset($_GET['action'])) {
    $action = $secure->clean($_GET['action']);
} else {
    $action = '';
}
$page_titles .= 'Private Messages - TeleMEdical';
if ($action == 'view') {
    if (!isset($_GET['m'])) {
        $system->message(L_ERROR, L_MAIL_ERROR_ID, './?page=mail', L_CONTINUE);
    }
    $tpl = $STYLE->open('view-mail.tpl');
    $mail_id = $secure->clean($_GET['m']);
    $mail = $db->fetch("SELECT * FROM mail WHERE id = '$mail_id'");
    // Update Page Title
    $page_title .= ' <img src="./template/default/images/bread_arrow.png" style="margin: 0 5px 0 5px; width 9px; height:9px;"/>' . $system->present($mail['title']) . '';
    // Generate Page Title For Profiles

    $page_title_pro .= ' > <a href="./?page=mail&amp;action=view&amp;m=' . $mail_id . '" >' . $system->present($mail['title']) . '</a>';
    if (!$mail) {
        $system->message(L_ERROR, 'This message does not exist or you do not have permission to view it.', './?page=mail', L_CONTINUE);
    }
    if ($mail['to_id'] !== $account['id'] && $mail['from_id'] !== $account['id']) {
        $system->message(L_ERROR, L_MAIL_ERROR_PERMISSION, './?page=mail', L_CONTINUE);
    }
    // Hide Reply from Author
    if ($mail['to_id'] != $account['id']) {
        $tpl = str_replace($STYLE->getcode('reply', $tpl), '', $tpl);
    }
    // Mark As Read
    if ($mail['to_id'] == $account['id'] && $mail['marked'] == '0') {

        $result = $db->query("UPDATE mail SET marked = '1' WHERE id = '$mail_id';");
    }
    if ($mail['to_id'] != $account['id']) {
        $author_id = $mail['to_id'];
    } else {
        $author_id = $mail['from_id'];
    }
    // Send Reply
    $mail_tit = $secure->clean($mail['title']);
    $from = $mail['from_id'];
    if (isset($_POST['Submit']) && isset($_POST['message'])) {


        $db->query("INSERT INTO mail (to_id,from_id,title,text,date) VALUE ('" . $mail['from_id'] . "','" . $account['id'] . "','$mail_tit','" . $secure->clean($_POST['message']) . "',UNIX_TIMESTAMP())");

        $system->message(L_MAIL, L_MAIL_REPLY, './?page=mail', L_CONTINUE);
    }
    $user_data = $db->fetch("SELECT * FROM accounts WHERE id = '" . $mail['from_id'] . "'");
    $output .= $STYLE->tags($tpl, array(
        "AVATAR" => $user->avatar($from),
        "AUTHOR" => $user->name($from),
        "TEXT" => $system->bbcode($mail['text']),
        "RANK" => $user->rank($from),
        "STATUS" => $user->status($from),
        "ID" => $system->present($mail_id),
        "DATE" => $system->time($mail['date']),
        "TITLE" => $system->present($mail['title']),
        "SIGNATURE" => $system->bbcode($user_data['signature']),
        "L_SUBMIT" => L_SUBMIT,
        "L_REPLY" => L_REPLY,
        "L_STATUS" => L_STATUS
    ));
} else if ($action == 'new') {
    $page_title .= ' <img src="./template/default/images/bread_arrow.png" style="margin: 0 5px 0 5px; width 9px; height:9px;"/> ' . L_NEW_MAIL . '';
    // Generate Page Title For Profiles
    $page_title_pro .= ' > <a href="./?page=mail&amp;action=new">' . L_NEW_MAIL . '</a>';
    $tpl = str_replace(array($STYLE->getcode('messages', $tpl), $STYLE->getcode('view', $tpl)), '', $tpl);
    if (isset($_POST['Submit'])) {
        // Is Everything Present?
        if (!isset($_POST['title']) || !isset($_POST['message']) || !isset($_POST['user'])) {
            $system->message(L_ERROR, L_INFORMATION_MISSING, './?page=mail', L_CONTINUE);
        }
        // Nothing is Blank
        if ($_POST['title'] == '' || $_POST['message'] == '' || $_POST['user'] == '') {
            $system->message(L_ERROR, L_INFORMATION_MISSING, './?page=mail', L_CONTINUE);
        }
        // Find User
        $user_account = $db->fetch("SELECT * FROM accounts WHERE name LIKE '" . $secure->clean($_POST['user']) . "'");

        if (!$user_account) {
            $system->message(L_ERROR, L_USER_NOT_FOUND, './?page=mail', L_CONTINUE);
        }

        $system->mail($user_account['id'], $account['id'], $secure->clean($_POST['title']), $secure->clean($_POST['message']));
        $system->message(L_SENT, L_MAIL_SENT, './?page=mail', L_CONTINUE);
    }
    if (isset($_GET['user'])) {
        $username = strip_tags($user->name($secure->clean($_GET['user'])));
    } else {
        $username = '';
    }
    $output .= $STYLE->tags($tpl, array("L_SUBMIT" => L_SUBMIT, "L_USER" => L_USER, "L_NEW_MAIL" => L_NEW_MAIL, "L_TITLE" => L_TITLE, "USERNAME" => $username
    ));
} else {
    $tpl = str_replace(array($STYLE->getcode('new', $tpl), $STYLE->getcode('view', $tpl)), '', $tpl);
    if (isset($_POST['delete']) && isset($_POST['checkbox'])) {
        $checkbox = $_POST['checkbox'];
        for ($i = 0; $i < count($checkbox); $i++) {
            $delete_id = $checkbox[$i];

            $result = $db->query("DELETE FROM mail WHERE id = '$delete_id' AND to_id = '" . $account['id'] . "'");
        }
        $system->message(L_DELETED, L_MAIL_SELECTED_DELETED, './?page=mail', L_CONTINUE);
    }
    if ($action == 'sent') {
        $page_title .= ' <img src="./template/default/images/bread_arrow.png" style="margin: 0 5px 0 5px; width 9px; height:9px;"/> ' . L_SENTBOX . '';
        // Generate Page Title For Profiles
        $page_title_pro .= ' > <a href="./?page=mail&amp;action=sent">' . L_SENTBOX . '</a>';
        $tpl = $STYLE->open('sent.tpl');
        $limiter = '15';
        $sql = "SELECT * FROM mail WHERE from_id = " . $account['id'] . " ";
        if (isset($_GET['page_num'])) {
            $page = $_GET['page_num'];
        } else {
            $page = 1;
        }
        if ($page != 1) {
            $start = ($page - 1) * $limiter;
        } else {
            $start = 0;
        }
        $relay = "?page=mail";
        $paginate = $system->paginate("$sql", "$limiter", "$relay");

        // Generate the member list
        $mail_sql = $db->query("SELECT * FROM mail WHERE from_id = " . $account['id'] . " ORDER BY id DESC LIMIT $start, $limiter;");
        if (!$mail_sql == 0) {
            $tpl = str_replace(array($STYLE->getcode('none', $tpl)), '', $tpl);
            $message_list = '';
            while ($messages = mysqli_fetch_array($mail_sql)) {
                $message_tpl = $STYLE->getcode('messages', $tpl);
                $from = $db->fetch("SELECT * FROM accounts WHERE id = " . $messages['from_id'] . "");
                if ($messages['marked'] == '0') {
                    $icon = 'icon-envelope-alt';
                } else {
                    $icon = 'icon-envelope';
                }
                if (strlen($messages['title']) > 4) {
                    $tname = '' . $system->present(substr($messages['title'], 0, 3)) . '...';
                } else {
                    $tname = $system->present($messages['title']);
                }
                $message_list .= $STYLE->tags($message_tpl, array(
                    "ID" => $messages['id'],
                    "ICON" => $icon,
                    "AVATAR" => $user->avatar($messages['to_id']),
                    "CLASS" => $class,
                    "TITLE" => '' . $system->present($messages['title']) . '',
                    "TITLE1" => $tname,
                    "DATE" => $system->time($messages['date']),
                    "NAME" => '' . $system->present($from['name']) . '',
                    "BY" => $messages['to_id']));
            }
            $tpl = str_replace($message_tpl, $message_list, $tpl);
        }
    } else {
        $page_title .= '<img src="./template/default/images/bread_arrow.png" style="margin: 0 5px 0 5px; width 9px; height:9px;"/>' . L_INBOX . '';

        // Generate Page Title For Profiles
        $page_title_pro .= ' > <a href="./?page=mail">' . L_INBOX . '</a>';
        $limiter = '15';
        $sql = "SELECT * FROM mail WHERE to_id = " . $account['id'] . " ";
        if (isset($_GET['page_num'])) {
            $page = $_GET['page_num'];
        } else {
            $page = 1;
        }
        if ($page != 1) {
            $start = ($page - 1) * $limiter;
        } else {
            $start = 0;
        }
        $relay = "?page=mail";
        $paginate = $system->paginate("$sql", "$limiter", "$relay");

        // Generate the member list
        $mail_sql = $db->query("SELECT * FROM mail WHERE to_id = " . $account['id'] . " ORDER BY id DESC LIMIT $start, $limiter;");
        $any_mail = $mail_sql->num_rows;
        if ($any_mail == 0) {
            $tpl = str_replace(array($STYLE->getcode('messages', $tpl)), '', $tpl);
            $STYLE->getcode('no-mail', $tpl);
        } else {
            $tpl = str_replace(array($STYLE->getcode('no-mail', $tpl)), '', $tpl);
        }
        $message_list = '';
        while ($messages = mysqli_fetch_array($mail_sql)) {
            $message_tpl = $STYLE->getcode('messages', $tpl);
            $from = $db->fetch("SELECT * FROM accounts WHERE id = " . $messages['from_id'] . "");
            if ($messages['marked'] == '0') {
                $icon = 'icon-envelope-alt';
            } else {
                $icon = 'icon-envelope';
            }
            if (strlen($messages['title']) > 4) {
                $tname = '' . $system->present(substr($messages['title'], 0, 3)) . '...';
            } else {
                $tname = $system->present($messages['title']);
            }
            $message_list .= $STYLE->tags($message_tpl, array(
                "ID" => $messages['id'],
                "ICON" => $icon,
                "AVATAR" => $user->avatar($messages['from_id']),
                "CLASS" => $class,
                "TITLE" => '' . $system->present($messages['title']) . '',
                "TITLE1" => $tname,
                "DATE" => $system->time($messages['date']),
                "NAME" => '' . $system->present($from['name']) . '',
                "BY" => $messages['from_id']));
        }
        $tpl = str_replace($message_tpl, $message_list, $tpl);
    }




    $output .= $STYLE->tags($tpl, array("NNAME" => $system->present($newstmem['name']), "NID" => $system->present($newstmem['id']), "TOT" => $total, "ONLINE_STATS" => $users, "W" => $webmaster, "A" => $admin, "M" => $mods + $gm, "ME" => $members, "G" => $guest, "PAGES" => $paginate, "L_TITLE" => $title, "L_TO_FROM" => $to_from, "L_DATE" => L_DATE, "L_MESSAGE" => L_MESSAGE, "L_DELETE" => L_DELETE));
}

