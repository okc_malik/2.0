<?php
$tpl = $STYLE->open('memberlist.tpl');
$page_title .= ' Member List';
$page_titles .= ' Member List - Twilight Domain';
$page_title_pro .= ' > Member List';

// Paginate
$limiter = '15';
$sql = "SELECT * FROM accounts WHERE id > 0 ";
if (isset($_GET['page_num'])) {
    $page = $_GET['page_num'];
} else {
    $page = 1;
}
if ($page != 1) {
    $start = ($page - 1) * $limiter;
} else {
    $start = 0;
}
$relay = "?page=memberlist";
$paginate = $system->paginate("$sql", "$limiter", "$relay");

// Generate the member list
$number = 1;
$member_sql = $db->query("" . $sql . " ORDER BY id LIMIT $start, $limiter;");
$member_list = '';
$member_tpl = $STYLE->getcode('row', $tpl);
while ($members = mysqli_fetch_array($member_sql)) {


    $member_list .= $STYLE->tags($member_tpl, array(
        "AVATAR" => $user->avatar($members['id']),
        "NAME" => $system->present($members['name']),
        "ID" => $members['id'],
        "STATUS" => $user->status($members['id']),
        "LEVEL" => $system->present($members['level']),
        "JOINED" => $system->time($members['joined']),
        "LASTLOGIN" => $system->time($members['lastlogin']),
        "GENDER" => $user->gender($members['id']),
//        "POSTCOUNT" => $user->postcount($members['id']),
        "SITERANK" => $user->groupname($members['id']),
//        "WIN" => $system->present($members['wins']),
//        "LOSS" => $system->present($members['loss']),
//        "EXP" => $system->present($members['exp']),
//        "WINP" => $winp,
//        "FL" => $user->flocation($user_id),
//        "PAGE" => $user->page($user_id),
//        "RANKIMG" => $user->groupimg($user_id)
));
}
$tpl = str_replace($member_tpl, $member_list, $tpl);
include 'left.php';

$output .= $STYLE->tags($tpl, array("NNAME" => $system->present($newstmem['name']), "NID" => $system->present($newstmem['id']), "TOT" => $total, "ONLINE_STATS" => $users, "W" => $webmaster, "A" => $admin, "M" => $mods + $gm, "ME" => $members, "G" => $guest, "PAGES" => $paginate));



