<?php

if (isset($_GET['user'])) {
    $user_id = $secure->clean($_GET['user']);

    if ($user_id == 0) {
        if ($id == '-1') {
            $system->redirect('./');
        } else {
            $system->redirect('http://senior.walentsoftware.com/?page=profile&user=' . $account['id'] . '');
        }
    }


    $result = $db->fetch("SELECT * FROM accounts WHERE id = " . $user_id . "");

    if ($result) {
        $tpl = $STYLE->open('profile.tpl');
        // Generate Global Menu

        $page_title = $page_title . ' <img
                    src="./template/default/images/bread_arrow.png" style="margin: 0 5px 0 5px; width 9px; height:9px;"/> ' . $user->proname($user_id);
        $page_titles .= ' ' . $user->proname($user_id) . ' - Twilight Domain';
        $page_title_pro = ' > Profile > ' . $user->proname($user_id);

        $group_id = $user->group($account['id']);
        $forums = $db->query("SELECT * FROM forum_forums_permission WHERE group_id = '$group_id'");
        $allowed_forums = "forum_id = '0'";
        while ($thisrow = mysqli_fetch_array($forums)) {
            $forum_id = $thisrow['forum_id'];
            if ($forum->forum_permission($forum_id, $group_id, 'view') == '1') {
                $allowed_forums .= " OR forum_id = '" . $forum_id . "' && author_id = '" . $user_id . "'";
            }
        }
        //For the Recent Topics
        $recent_topics = $db->query("SELECT *  FROM forum_topics WHERE (" . $allowed_forums . ") ORDER BY id DESC LIMIT 10;");
        $recent_topics_tpl = $STYLE->getcode('recent_topics', $tpl);

        while ($row = mysqli_fetch_array($recent_topics)) {

            $topic_post = $db->query("SELECT * FROM forum_posts WHERE topic_id = '" . $row['id'] . "'");
            $posts = $topic_post->num_rows;
            while ($post = mysqli_fetch_array($topic_post)) {
                $text1 = $post['text'];
                if (strlen($text1) > 50) {
                    $text = '' . $system->bbcode(substr($post['text'], 0, 55)) . '...';
                } else {
                    $text = $system->bbcode($post['text']);
                }
            }
            //For the topic name
            if (strlen($row['title']) > 17) {
                $tname = '' . $system->present(substr($row['title'], 0, 17)) . '...';
            } else {
                $tname = $system->present($row['title']);
            }

            $recent_style .= $STYLE->tags($recent_topics_tpl, array(
                "ID" => $row['id'],
                "URLS" => $row['url'],
                "BY" => $user->name($row['author_id']),
                "DATE" => $system->time1($row['date']),
                "TITLE" => $tname,
                "TEXT" => $text,
                "POSTS" => $posts - 1
            ));

        }
        $tpl = str_replace($recent_topics_tpl, $recent_style, $tpl);


        //For the Recent Posts!
        $recent_posts = $db->query("SELECT *  FROM forum_posts WHERE (" . $allowed_forums . ") ORDER BY id DESC LIMIT 10;");
        $recent_post_tpl = $STYLE->getcode('recent_posts', $tpl);

        while ($posts_row = mysqli_fetch_array($recent_posts)) {

            //For the posts content
            $texts = $posts_row['text'];
            if (strlen($texts) > 109) {
                $text = '' . $system->present(substr($posts_row['text'], 0, 109)) . '...';
            } else {
                $text = $system->present($posts_row['text']);
            }

            $recent_post_style .= $STYLE->tags($recent_post_tpl, array(
                "TOPICID" => $posts_row['topic_id'],
                "DATE" => $system->time1($posts_row['date']),
                "TEXT" => $text
            ));

        }
        $tpl = str_replace($recent_post_tpl, $recent_post_style, $tpl);

        if ($result['hide'] == 0) {
            $email = $system->present($result['email']);
        } else {
            $email = 'N/A';
        }

        $gamesplayed = $result['wins'] + $result['loss'];
        $winp = round($result['wins'] / $gamesplayed * 100);


        $limit = $result['exp'] + 1;
        $rank = $db->fetch("SELECT * FROM exp_ranks WHERE exp < '$limit' ORDER BY `exp` DESC ");
        $umm = $rank['id'] + 1;
        $nextlevel = $db->fetch("SELECT * FROM exp_ranks WHERE id ='$umm' ");
        $neededexp = $system->present($nextlevel['exp']);
        $current_exp_percent = round($result['exp'] / $neededexp * 100);


        $output .= $STYLE->tags($tpl, array(
            "ID" => $user_id,
            "AVATAR" => $user->avatar($user_id),
            "EMAIL" => $email,
            "NAME" => $system->present($result['name']),
            "STATUS" => $user->status($user_id),
            "RANK" => $user->ladder_rank($user_id),
            "FORUM_RANK" => $user->rank($user_id),
            "ERANK" => $user->getUserRank($user_id),
            "LEVEL" => $system->present($result['level']),
            "JOINED" => $system->time($result['joined']),
            "LASTLOGIN" => $system->time($result['lastlogin']),
            "GENDER" => $user->gender($user_id),
            "LOCATION" => $system->present($result['location']),
            "POSTCOUNT" => $user->postcount($user_id),
            "SITERANK" => $user->groupname($user_id),
            "NICKN" => $system->present($result['nickn']),
            "NA" => $system->present($result['na']),
            "SA" => $system->present($result['sa']),
            "WON" => $system->present($result['won']),
            "LOL" => $system->present($result['lol']),
            "PSN" => $system->present($result['psn']),
            "XBL" => $system->present($result['xbl']),
            "FB" => $system->present($result['fb']),
            "TW" => $system->present($result['tw']),
            "TB" => $system->present($result['tb']),
            "Twitch" => $system->present($result['twitch']),
            "WEBSITE" => $system->present($result['website']),
            "WIN" => $system->present($result['wins']),
            "LOSS" => $system->present($result['loss']),
            "EXP" => $system->present($result['exp']),
            "NEEDEDEXP" => $neededexp,
            "CURRENTEXP" => $current_exp_percent,
            "WINP" => '' . $winp . '%',
            "FL" => $user->flocation($user_id),
            "PAGE" => $user->page($user_id),
            "RANKIMG" => $user->groupimg($user_id),
            "FRIEND" => $user->friend($user_id)
        ));
    } else {
        $system->message(L_ERROR, L_PROFILE_ERROR, './', L_CONTINUE);
    }

} else {
    $system->message(L_ERROR, L_PROFILE_ERROR, './', L_CONTINUE);
}

