<?php


$tpl = $STYLE->open('auth/register.tpl');
$page_titles = ' Register - TeleMedical';
// Is Registration Open?
$logtext = 'Registration';
if ($system->confdata('userreg') != '1') {
    $system->page(L_ERROR, L_REGISTERATION_CLOSED);
}

// Error reporting:
error_reporting(E_ALL ^ E_NOTICE);


if (isset($_POST['submit'])) {
    echo 'accounts';
    if (isset($_POST['name'])) {
        $name = $secure->clean($_POST['name']);
    } else {
        $name = '';
    }
    if (isset($_POST['email'])) {
        $email = $secure->clean($_POST['email']);
    } else {
        $email = '';
    }

    if (isset($_POST['password'])) {
        $password = $secure->clean($_POST['password']);
    } else {
        $password = '';
    }
    if (isset($_POST['confirm_password'])) {
        $password_confirm = $secure->clean($_POST['confirm_password']);
    } else {
        $password_confirm = '';
    }

//    if (isset($_POST['captcha'])) {
//        $captcha = $secure->clean($_POST['captcha']);
//    } else {
//        $captcha = '';
//    }
    // Check Captcha
//    if (md5($captcha) != $_SESSION['captcha']) {
//        $system->showAlert(L_CAPTCHA_ERROR);
//
//    }
    // Make sure passwords match each other
    if ($password != $password_confirm) {
        $system->showAlert(L_CONFIRM_PASSWORD_ERROR);
    }
    // Prevent Nullifying of Password
    if ($password == '') {
        $system->showAlert(L_INFORMATION_MISSING);
        exit("error missing pass");
    }
    // Generate ACTIVATION_CODE
    $activation_code = rand(1000, 5000);
    // Check if fields are blank
    if (!isset($name)) {
        $system->showAlert(L_REGISTER_ERROR_NAME);
        exit("error email");
    }
    if (!isset($email)) {
        $system->showAlert(L_REGISTER_ERROR_EMAIL);
        exit("error email");
    }

    if (!isset($password)) {
        $system->showAlert(L_REGISTER_ERROR_PASSWORD);
        exit("error reg  pass");
    }

    if (!isset($password_confirm)) {
        $system->showAlert(L_REGISTER_ERROR_PASSWORD);
        exit("error pass conf");
    }

    if ($secure->verify_email($email) == 'exist') {
        $system->showAlert(L_REGISTER_ERROR_EMAIL_EXISTS);
        $system->message(L_REGISTER, L_REGISTER_ERROR_EMAIL_EXISTS, './?account=register', L_LOGIN);
    }

    if ($secure->verify_email($email) == 'false') {
        $system->showAlert(L_REGISTER_ERROR_EMAIL_FALSE);
        //exit("error verify_email");
    }

    if ($secure->verify_email($email) == 'banned') {
        $system->showAlert(L_REGISTER_ERROR_EMAIL_BANNED);
        //exit("error verify_email");
    }
    // Create This Account
    $new_password = $secure->hashedPassword($password);
    $ip = $secure->clean($_SERVER['REMOTE_ADDR']);

    if ($system->confdata('activation') == '1') {
        $activated = '0';
    } else {
        $activated = '1';
    }

    $insert_user = $user->createUser($name, $email , $new_password, $ip,$activation_code, $activated);
    if ($insert_user) {
        $get_user = $user->getUserByEmail($email);
        $id = $get_user['id'];
        echo $id . " asdfasf";
        $permission_group = $db->query("INSERT INTO forum_groups_members (account_id, group_id) VALUES ('" . $get_user['id'] . "','2')");

    } else {
        $system->message(L_ERROR, "There was an error creating your account.", '?account=register', L_CONTINUE);
    }

    if ($system->confdata('activation') == '1') {
        $email_message = str_replace(array('[USER]', '[URL]', '[UID]', '[ACTIVATION_CODE]'), array($name, $siteaddress . '?s=activate&u=' . $get_user['id'] . '&code=' . $activation_code, $get_user['id'], $activation_code), L_REGISTER_EMAIL);
        $system->email($email, L_REGISTER_SUBJECT, $email_message);
    }
    $system->message(L_REGISTER, L_REGISTER_MESSAGE, './?account=login', L_LOGIN);
} else {
    // Show Registration Form
    $output .= $STYLE->tags($tpl, array("L_CAPTCHA" => L_CAPTCHA,
        "TOS" => $system->confdata('tos'),
        "L_PASSWORD" => L_PASSWORD,
        "L_CONFIRM_PASSWORD" => L_CONFIRM_PASSWORD,
        "L_NAME" => L_NAME, "L_EMAIL" => L_EMAIL,
        "L_MALE" => L_MALE, "L_FEMALE" => L_FEMALE,
        "L_HIDDEN" => L_HIDDEN,
        "L_TIMEZONE" => L_TIMEZONE,
        "L_GENDER" => L_GENDER,
        "L_LOCATION" => L_LOCATION,
        "L_SUBMIT" => L_SUBMIT,
        "L_ACC_DETAILS" => L_ACCOUNT_DETAILS,
        "L_PROFILE_DETAILS" => L_PROFILE_DETAILS,
        "L_AGREEMENT_STATEMENT" => L_AGREEMENT_STATEMENT,
        "L_AGREEMENT" => L_AGREEMENT

    ));
}

