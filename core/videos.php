<?php

$tpl = $STYLE->open('main/videos.tpl');
$page_titles = ' Videos - TeleMedical';

$report = new videos();
print_r("User id:");
print_r($id);
if (isset($_GET['mode'])) {
    $mode = $secure->clean($_GET['mode']);
} else {
    $mode = '';
}


// Paginate
$limiter = '50';
if ($system->group_permission($user->group($account['id']), 'acp') == '1') {
    $STYLE->getcode('is_admin', $tpl);
    $sql = "SELECT * FROM videos";
} else {
    $tpl = str_replace(array($STYLE->getcode('is_admin', $tpl)), '', $tpl);
    $sql = "SELECT * FROM videos WHERE id = " . $account['id'] . "";
}

if ($mode == 'create') {



    $tpl = str_replace(array($STYLE->getcode('default', $tpl), $STYLE->getcode('account', $tpl), $STYLE->getcode('avatar', $tpl), $STYLE->getcode('settings', $tpl), $STYLE->getcode('details', $tpl)), '', $tpl);
    if(isset($_POST["submit"]) && !empty($_FILES["file"]["name"])) {

        if(!is_dir("./videos/".$id. "/")) {
            mkdir("./videos/".$id. "/");
        }

        //allow certain file formats

        if (isset($_POST['name'])) {
            $name = $secure->clean($_POST['name']);
        } else {
            $name = '';
        }
        if (isset($_POST['forid'])) {
            $forID = $_POST["forid"];
        } else {
            $system->message1("Error",  "The user this report is for must be inputted.", './?page=videos', L_CONTINUE);

        }

        $targetDir = "./videos/".$id. "/";
        $orgFileName = basename($_FILES["file"]["name"]);
        $fileName = $secure->cleanForURL(pathinfo(basename($_FILES["file"]["name"]), PATHINFO_FILENAME));
        $extension = pathinfo($orgFileName, PATHINFO_EXTENSION);
        $targetFilePath = $targetDir . $fileName.".".$extension;
        $fileType = pathinfo($targetFilePath,PATHINFO_EXTENSION);
        $allowTypes = array('mp4','MPEG-4','mpeg-4','MP4');
        if(in_array($fileType, $allowTypes)){
            //upload file to server
            if (file_exists($targetFilePath)) {
                unlink($targetFilePath );
            }
            if(move_uploaded_file( $_FILES["file"]["tmp_name"], $targetFilePath)){
                $statusMsg = "The file ".$fileName. " has been uploaded.";
                $uploadToDB = $report->createVideo($orgFileName, $id, $forID, $targetFilePath, $desc);
                if ($uploadToDB) {
                    $system->message1("Success",  "The file ".$fileName. " has been uploaded.", './?page=videos', L_CONTINUE);

                } else {
                    $system->message1(L_ERROR,  "The file ".$fileName. " was not uploaded. Error Code: FDB-RP", './?page=videos&amp;mode=create', L_CONTINUE);

                }

            }else{
                $statusMsg = "Sorry, there was an error uploading your file.";
                $system->message1(L_ERROR, "Not Uploadd", './?page=videos&amp;mode=create', L_CONTINUE);

            }
        }else{
            $statusMsg = 'Sorry, only JPG, JPEG, PNG, GIF, & PDF files are allowed to upload.';
            //$system->message1(L_ERROR, $statusMsg, './?page=videos&amp;mode=create', L_CONTINUE);

        }
    } else {
        $tpl = $STYLE->tags($tpl, array("AVATAR" => $user->avatar($account['id']), "L_DELETE" => L_DELETE));
    }
} else {
    $tpl = str_replace(array($STYLE->getcode('create', $tpl), $STYLE->getcode('signature', $tpl), $STYLE->getcode('avatar', $tpl), $STYLE->getcode('settings', $tpl), $STYLE->getcode('details', $tpl)), '', $tpl);
    if (isset($_GET['page_num'])) {
        $page = $_GET['page_num'];
    } else {
        $page = 1;
    }
    if ($page != 1) {
        $start = ($page - 1) * $limiter;
    } else {
        $start = 0;
    }


    $relay = "?page=videos";
    $paginate = $system->paginate("$sql", "$limiter", "$relay");


    $number = 1;

    $reportSQL = $report->fetchVideosForUser($account['id']);
    //$reportSQL = $db->query("SELECT * FROM `videos` WHERE `for_id` = 24");
    $videos_list = '';
    $member_tpl = $STYLE->getcode('row', $tpl);
    while ($videos = mysqli_fetch_array($reportSQL)) {



        $videos_list .= $STYLE->tags($member_tpl, array(
            "ID" => $videos['id'],
            "TITLE" => $system->present($videos['title']),
            "DATECREATED" => $system->time($videos['date_created']),
            "CREATORID" => $videos['creator_id'],
            "FORID" => $videos['for_id'],
            "REFH" => $videos['url'],
            "FORNAME" => $user->name($videos['for_id']),
            "CREATORNAME" => "DR. " . $user->name($videos['creator_id'])));
    }
    $tpl = str_replace($member_tpl, $videos_list, $tpl);
}



$output .= $STYLE->tags($tpl, array("NNAME" => $system->present($newstmem['name']), "NID" => $system->present($newstmem['id']), "TOT" => $total, "ONLINE_STATS" => $users, "W" => $webmaster, "A" => $admin, "M" => $mods + $gm, "ME" => $videos, "G" => $guest, "PAGES" => $paginate));




