<?php

error_reporting(0);
require("db.php");
require("function.php");
$system = new system();
$secure = new secure();
$user = new user();
$forum = new forum();
// CHECK CONFIG
include("config.php");
// LOAD DATABASE
$db = new db();
$db->connect($dbhost, $dbuser, $dbpassword, $dbmaster);
// SESSION
session_name($system->confdata('session'));
session_start();

date_default_timezone_set('GMT');
// CHECK IF USER IS LOGGED IN
if (isset($_COOKIE["email"]) && isset($_COOKIE["lpip"])) {

    $email = $_COOKIE["email"];
    $lpip = $_COOKIE['lpip'];


} else {
    $email = '';
    $lpip = '';
}
//if (!isset($_SESSION['email'])) {
//    $email = '';
//} else {
//   $email = $_SESSION['email'];
//}
//if (!isset($_SESSION['lpip'])) {
//    $lpip = '';
//} else {
//   $lpip = $_SESSION['lpip'];
//}
$account = $db->fetch("SELECT * FROM accounts WHERE email = '$email' AND lpip = '$lpip'");
$url = $system->confdata('furl');
$path = $system->confdata('path');
if ($path) {
    $siteaddress = "$url/";
} else {
    $siteaddress = "$url/";
}
$document_root = $_SERVER['DOCUMENT_ROOT'];
$root = "$document_root/";
// SESSION
$session_location = $_SERVER['REQUEST_URI'];
$session_id = session_id();
$ip = $_SERVER['REMOTE_ADDR'];
if ($account['id']) {
    $id = $account['id'];
} else {
    $id = '-1';
}
$user_id = $secure->clean($_GET['user']);
$group_id = $user->group($account['id']);
if ($system->confdata('siteclosed') == '1' && $system->group_permission($user->group($account['id']), 'acp') != '1') {
    $system->page(L_CLOSED, L_SITE_CLOSED);
}

$db->query("DELETE FROM online WHERE session= '$session_id';");
$db->query("INSERT INTO online ( time, account_id , session , ip , location , group_id ) VALUES  (unix_timestamp(), '$id' , '$session_id' , '$ip','$session_location' ,'$group_id' );");
$db->query("DELETE FROM online WHERE time < unix_timestamp()-90;");
$db->query("UPDATE accounts SET lastlogin = unix_timestamp() WHERE id = '$id'");
$onlinelist = $db->fetch("SELECT * FROM online WHERE account_id = '$id'");


//setcookie(session_name(),session_id(),time()+1000*100*24*15);
// STYLES SYSTEM
if ($account['tpl'] && $system->confdata('usertemplate') == '1') {
    $template = $account['tpl'];
    if (file_exists("$root/template/$template/header.tpl")) {
        $template = $template;
    } else {
        $template = $system->confdata('ftemplate');
    }
} else {
    $template = $system->confdata('ftemplate');
}
$temp = 'default';
require("parser.php");
$STYLE = new style();
// LANGUAGE
$language = $system->confdata('language');
require("" . $root . "/lang/$language/index.php");
// PARSE STYLE
$output = '';
$tpl = $STYLE->open('header.tpl');
// Find Your Group

if ($account['bantime'] > time()) {
    $system->page(L_TEMPORARY_BAN, str_replace('[TIME]', $system->time($account['bantime']), L_TEMPORARY_BAN_MSG));
}

$url = $system->confdata('url') . '/' . $system->confdata('path') . '';
if ($system->group_permission($user->group($account['id']), 'acp') == '1') {
    $admin_link = '<a href="' . $furl . 'acp/" class="notification_links">ACP</a> ';
} else {
    $admin_link = '';
}
if ($system->group_permission($user->group($account['id']), 'rc') == '1') {
    $gu = mysql_num_rows($db->query("SELECT * FROM " . $prefix . "_reports WHERE actioned='0'"));
    $rc_link = '<a href="' . $furl . 'rc/" class="notification_links">(' . $gu . ') Reports</a> ';
} else {
    $rc_link = '';
}

// LATEST AND RECENT
$group_id = $user->group($account['id']);
$sql = "SELECT * FROM " . $prefix . "_forums_permission WHERE group_id = '$group_id'";
$result = $db->query("" . $sql . "");
$allowed_forums = "forum_id = '0'";
while ($thisrow = mysql_fetch_array($result)) {
    $forum_id = $thisrow['forum_id'];

    if ($forum->forum_permission($forum_id, $group_id, 'view') == '1') {
        $allowed_forums .= " OR forum_id = '" . $forum_id . "'";
    }
}

$recent_post = "SELECT *  FROM " . $prefix . "_late_topic WHERE (" . $allowed_forums . ") ORDER BY date DESC LIMIT 10;";
$recent_row = $db->query($recent_post);

$recent_tpl = $STYLE->getcode('recent', $tpl);
$number = '1';
while ($row = mysql_fetch_array($recent_row)) {

    $topic_data = $db->fetch("SELECT * FROM " . $prefix . "_topics WHERE id = '" . $row['topic_id'] . "'");
    $name = $system->present($topic_data['title']);
    if (strlen($name) > 10) {
        $tname = '' . $system->present(substr($topic_data['title'], 0, 15)) . '...';
    } else {
        $tname = $system->present($topic_data['title']);
    }

    $recent_style .= $STYLE->tags($recent_tpl, array(
        "URLS" => $row['url'],
        "BY" => $user->name($row['author_id']),
        "DATE" => $system->time($row['date']),
        "NAME" => $tname,
    ));

}
$tpl = str_replace($recent_tpl, $recent_style, $tpl);

if (!$topic) {
    $topic = L_NONE;
}


$new_topics = "SELECT *  FROM " . $prefix . "_topics WHERE (" . $allowed_forums . ") ORDER BY id DESC LIMIT 10;";
$new_row = $db->query($new_topics);

$new_tpl = $STYLE->getcode('new', $tpl);
$number = '1';
while ($row = mysql_fetch_array($new_row)) {


    $name1 = $system->present($row['title']);
    if (strlen($name1) > 9) {
        $tname1 = '' . $system->present(substr($row['title'], 0, 15)) . '...';
    } else {
        $tname1 = $system->present($row['title']);
    }

    $new_style .= $STYLE->tags($new_tpl, array(
        "URLS" => 'index.php?area=forum&s=topic&amp;t=' . $row['id'] . '',
        "BY" => $user->name($row['author_id']),
        "DATE" => $system->time($row['date']),
        "NAME" => $tname1,
    ));

}
$tpl = str_replace($new_tpl, $new_style, $tpl);

// CHANGE NAVIGATION

$mail = mysql_num_rows($db->query("SELECT id FROM _mail WHERE to_id = '" . $account['id'] . "' AND marked = '0'"));
$mail = str_replace('[MAIL]', $mail, L_PANEL_MAIL);
$panel = 'Welcome back ' . $user->name($account['id']) . ', ' . $mail . ' ';
$tpl = $STYLE->tags($tpl, array("PANEL" => $panel));
$post_row = $db->fetch("SELECT * FROM " . $prefix . "_news ORDER BY id DESC");
$output .= $STYLE->tags($tpl, array("L_HOME" => L_HOME, "LINKS" => L_LINKS, "L_REGISTER" => L_REGISTER, "L_MAIL" => L_MAIL, "L_LOGIN" => L_LOGIN, "L_LOGOUT" => L_LOGOUT, "L_ACCOUNT" => L_ACCOUNT, "L_ANN" => $system->bbcode($post_row['text']), "TOPIC_STUFF" => $topic, "NEW" => $new,
    "UID" => $id,
    "MESS" => $mail,
    "TOPP" => $topp,
    "AVATAR" => $user->avatar($account['id']),
    "NAME" => $user->name($account['id']),
    "STATUS" => $user->status($account['id']),
    "RANK" => $user->rank($account['id']),
    "JOINED" => $system->time($account['joined']),
    "LASTLOGIN" => $system->time($account['lastlogin']),
    "GENDER" => $user->gender($account['id']),
    "LOCATION" => $system->present($account['location']),
    "SIGNATURE" => $system->bbcode($account['signature']),
    "POSTCOUNT" => $user->postcount($account['id']),
    "PRONAME" => $user->proname($account['id']),
    "SITERANK" => $user->groupname($account['id']),
    "NICKN" => $user->nickn($account['id']),
    "WEBSITE" => $user->website($account['id']),
    "FL" => $user->flocation($account['id']),
    "PAGE" => $user->page($account['id']),
    "RC" => $rc_link, "ACP" => $admin_link,
    "LINKS" => L_LINKS,
    "ACPLINK" => AL_LINKS,
    "L_RANK" => L_RANK,
    "L_JOINED" => L_JOINED,
    "L_LAST_LOGIN" => L_LAST_LOGIN,
    "L_GENDER" => L_GENDER,
    "L_LOCATION" => L_LOCATION,
    "L_DETAILS" => L_DETAILS,
    "L_SIGNATURE" => L_SIGNATURE,
    "L_POST_COUNT" => L_POST_COUNT));
// Check Banlist
$user_ip = $secure->clean($_SERVER['REMOTE_ADDR']);
$ban_sql = $db->fetch("SELECT * FROM " . $prefix . "_banlist WHERE value = '$user_ip'");
if ($ban_sql) {
    $system->page(L_BANNED, L_BANNED_IP);
}