<?php

// PARSE STYLE
$link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ?
        "https" : "http") . "://" . $_SERVER['HTTP_HOST'] .
    $_SERVER['REQUEST_URI'];

//echo $link;
switch ($link) {
    case 'https://senior.walentsoftware.com/?account=login' :
        $tpl = $STYLE->open('auth/login-footer.tpl');
        break;
    case 'https://senior.walentsoftware.com/?account=register':
        $tpl = $STYLE->open('auth/register-footer.tpl');
        break;
    default:
        $tpl = $STYLE->open('footer.tpl');
}
//if ($link == 'https://senior.walentsoftware.com/?account=login') {
//
//    $tpl = $STYLE->open('auth/login-footer.tpl');
//} else {
//
//    $tpl = $STYLE->open('footer.tpl');
//}

$output .= $STYLE->tags($tpl, array("TPL" => $template));
if (!isset($page_title)) {
    $page_title = '';
}
if (!isset($page_title_pro)) {
    $page_title_pro = '';
}
if (!isset($page_titles)) {
    $page_titles = 'Twilight Domain';
}
if (!isset($global_menu)) {
    $global_menu = '';
}
$session_location = $_SERVER['REQUEST_URI'];
if ($account['id']) {
    $id = $account['id'];
} else {
    $id = '-1';
}
$val1 = $db->real_escape_string($session_location);
$val2 = $db->real_escape_string($page_title_pro);
$val3 = $db->real_escape_string($id);
//$db->query("UPDATE accounts SET flocation = '$session_location', page = 'Twilight Domain " . $val2 . "'  WHERE id = '$id'");
$system = new system();
$version = $system->confdata('version');
$sitename = $system->confdata('sitename');

$report = '<a href="' . $url . '?area=report" class="normfont">' . L_REPORT . '</a> | ';
$tos = ' <a href="' . $url . '?area=tos" class="normfont">' . L_TERMS_OF_SERVICE . '</a>';
$groups = '<a href="' . $url . '?area=groups" class="normfont">' . L_GROUPS . '</a> | ';
$output = $STYLE->tags($output, array("URL" => $url, "REPORT" => $report, "TOS" => $tos, "GROUPS" => $groups, "L_POWERED_BY" => L_POWERED_BY, "RC" => $rc_link, "ACP" => "admin_link", "GLOBAL_MENU" => $global_menu, "AREA" => $page_title, "TITLE" => $page_titles, "SITELINK" => '<a href="senior.walentsoftware.com" class="navfont">' . $sitename . '</a>', "PAGETITLE" => '' . $sitename . ' - ' . strip_tags($page_title) . ' - ' . L_POWERED_BY . ' ' . $version . '', "VERSION" => $version));
if (!$account) {
    $output = preg_replace('/\<!-- BEGIN logged_in -->(.*?)\<!-- END logged_in -->/is', '', $output);
} else {
    $output = preg_replace('/\<!-- BEGIN logged_out -->(.*?)\<!-- END logged_out -->/is', '', $output);
}
//Log user activity
$ip = ($_SERVER['REMOTE_ADDR']);

//$insert = $db->query("INSERT INTO logs (account_id, ip, date, location, text) VALUES ('$id', '$ip' , UNIX_TIMESTAMP(),  '$session_location', '$log')");


print $output;
$STYLE->close();
$db->close();

