<?php
// Error reporting:
error_reporting(E_ALL ^ E_NOTICE);

mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
class system
{

    function showAlert($message) {
        echo "<script>alert('$message');</script>";
    }

    function getUnixTime() {
        return UNIX_TIMESTAMP;
    }

    function current_url()
    {
        $pageURL = 'https://';
        $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
        return $pageURL;
    }

    function message($title, $message, $linkurl, $link)
    {
        global $output, $STYLE, $db, $template, $page_title, $global_menu, $account, $user, $system;
        $tpl = $STYLE->open('message.tpl');
        $output .= $STYLE->tags($tpl, array("TITLE" => $title, "MESSAGE" => $message, "LINK" => $linkurl, "LINK-TEXT" => $link));

        //include('./core/online.php');


        include("footer.php");
        echo '<META HTTP-EQUIV="refresh" CONTENT="5;URL=' . $linkurl . '">';
        exit;
    }

    function message1($title, $message, $linkurl, $link)
    {
        global $output, $STYLE, $db, $template, $page_title, $global_menu, $account, $user, $system;
        $tpl = $STYLE->open('message1.tpl');
        $output .= $STYLE->tags($tpl, array("TITLE" => $title, "MESSAGE" => $message, "LINK" => $linkurl, "LINK-TEXT" => $link));

        include('./core/online.php');


        include("footer.php");
        echo '<META HTTP-EQUIV="refresh" CONTENT="5;URL=' . $linkurl . '">';
        exit;
    }

    function nonone($title, $message, $linkurl, $link)
    {
        global $output, $STYLE, $db, $template, $page_title, $global_menu, $account, $user, $system;
        $tpl = $STYLE->open('message.tpl');
        $output .= $STYLE->tags($tpl, array("TITLE" => $title, "MESSAGE" => $message, "LINK" => $linkurl, "LINK-TEXT" => $link));

        include('./core/online.php');


        include("footer.php");
        exit;
    }

    function freindss($title, $message, $linkurl, $link)
    {
        global $output, $STYLE, $db, $template, $page_title, $global_menu, $account, $user, $system;
        $tpl = $STYLE->open('message.tpl');
        $output .= $STYLE->tags($tpl, array("TITLE" => $title, "MESSAGE" => $message, "LINK" => $linkurl, "LINK-TEXT" => $link));


        include("footer.php");
        exit;
    }

    function notopic($title, $message, $linkurl, $link)
    {
        global $output, $STYLE, $db, $template, $page_title, $global_menu, $account, $user, $system;
        $tpl = $STYLE->open('notopic.php');
        $output .= $STYLE->tags($tpl, array("TITLE" => $title, "MESSAGE" => $message, "LINK" => $linkurl, "LINK-TEXT" => $link));
        include("./core/online.php");
        include("footer.php");
        echo '';
        exit;
    }

    function confirm($title, $message, $link, $vars = '')
    {
        global $output, $STYLE, $db, $template, $page_title, $global_menu, $account, $user, $system;
        $tpl = $STYLE->open('confirm.tpl');


        $hidden_fields = '';
        while (list($key, $val) = each($vars)) {
            $hidden_fields .= '<input type="hidden" name="' . $key . '" value="' . $val . '">';
        }
        $output .= $STYLE->tags($tpl, array("TITLE" => $title, "MESSAGE" => $message, "LINK" => $link, "L_CONFIRM" => L_CONFIRM, "L_CANCEL" => L_CANCEL, "HIDDEN_FIELDS" => $hidden_fields));
        include("footer.php");
        exit;
    }

    function page($title, $message)
    {
        global $output, $STYLE, $db, $temp, $page_title, $global_menu, $account, $user, $system;

        $tpl = $STYLE->open('page.tpl');
        $output .= $STYLE->tags($tpl, array("TITLE" => $title, "MESSAGE" => $system->bbcode($message)));
        include("./core/online.php");
        include("footer.php");
        exit;
    }

    function redirect($url)
    {
        echo '<script language="javascript">
window.location="' . $url . '";
</script>';
        unset($url);
        exit;
    }

    function confdata($value)
    {
        global $db;
        $result = $db->fetch("SELECT value FROM forum_confdata WHERE name = '$value'");
        return $result['value'];
        unset($result);
    }

    function mail($to_id, $from_id, $title, $message)
    {
        global $db, $prefix;

        $db->query("INSERT INTO mail (to_id,from_id,title,text,date) VALUE ('$to_id','$from_id','$title','$message',UNIX_TIMESTAMP())");
    }



    function email($email, $subject, $message)
    {
        global $system;
        $from = $system->confdata('adminemail');
        $sitename = $system->confdata('sitename');
        $siteaddress = $system->confdata('url') . $system->confdata('path') . '/';
        $message = str_replace(array("[SITENAME]", "[URL]", "[CONTENT]"), array($sitename, $siteaddress, $message), L_EMAIL_WRAP);
        $message = "\n---------------------------------\n$message\n\nThe Management\n$siteaddress";
        $headers = 'From: ' . $from . '' . "\r\n" . 'X-Mailer: PHP/' . phpversion();
        mail($email, $subject, $message, $headers);
    }

    function time($timestamp, $string = 'F j, Y, g:i a')
    {
        global $account;
        $EST = $timestamp + 14400;
       $time = date($string . '', $EST);
        return $time;
    }

    function time1($timestamp, $string = 'M d, Y')
    {
        global $account;
        $real = $timestamp + $account['timezone'];
        $time = date($string . '', $real);
        return $time;
    }

    function present($string)
    {
        $string = stripslashes($string);
        return $string;
        unset($string);
    }


    function strlen($string)
    {
        $string = strlen($string);
        return $string;
        unset($string);
    }


    function group_permission($id, $value)
    {
        global $db, $prefix, $account;

        $result = $db->fetch("SELECT * FROM forum_groups WHERE id = '$id'");
        if ($result) {
            $value = $result['' . $value . ''];
        } else {
            $value = L_ERROR;
        }

        return $value;
    }


    function paginate($sql, $amount, $relay)
    {
        global $db, $page;
        $query = $db->query("$sql");
        $number = mysqli_num_rows($query);
        $number_two = $number;
        $count = '0';
        $value = '';
        $final_number = $number / $amount;
        if ($number) {
            if ($page > 1) {
                $down = $page - 1;
                $value .= '<a href="' . $relay . '" class="paginate">' . L_FIRST . '</a> <a href="' . $relay . '&amp;page=' . $down . '" class="paginate"><</a>';
            } else {

            }
            while ($number_two > 0) {
                $number_two -= $amount;
                $count = $count + 1;
                $up = $page + 1;

                $low = $page - 5;
                $high = $page + 5;
                if ($count == $page) {
                    $value .= '<a href="' . $relay . '&amp;page=' . $count . '" class="paginate" style="background: #666;">' . $count . ' </a>';
                } else {
                    if ($count > $low && $count < $high) {
                        $value .= '<a href="' . $relay . '&amp;page=' . $count . '" class="paginate" >' . $count . ' </a>';
                    }
                }

            }
            if ($page < $count) {
                $value .= '<a href="' . $relay . '&amp;page=' . $up . '" class="paginate">></a> <a href="' . $relay . '&amp;page=' . $count . '" class="paginate">' . L_LAST . '</a>';
            }
            if ($page > $count) {
                echo '<script language="javascript">
window.location="' . $relay . '"
</script>';
            }
            if ($page < 0) {
                echo '<script language="javascript">
window.location="' . $relay . '"
</script>';

            }
        }
        return $value;
    }

    function rpaginate($sql, $amount, $relay)
    {
        global $db, $page;
        $query = $db->query("$sql");
        $number = mysqli_num_rows($query);
        $number_two = $number;
        $count = '0';
        $value = '';
        $final_number = $number / $amount;
        if ($number) {
            if ($page > 1) {
                $down = $page - 1;
                $value .= '<a href="' . $relay . '" class="formcss3">' . L_FIRST . '</a> <a href="' . $relay . '?page=' . $down . '" class="formcss3"><</a>';
            } else {

            }
            while ($number_two > 0) {
                $number_two -= $amount;
                $count = $count + 1;
                $up = $page + 1;

                $low = $page - 5;
                $high = $page + 5;
                if ($count == $page) {
                    $value .= '<a href="' . $relay . '&amp;page=' . $count . '" class="formcss3" style="background: #3b697e;">' . $count . ' </a>';
                } else {
                    if ($count > $low && $count < $high) {
                        $value .= '<a href="' . $relay . '?page=' . $count . '" class="formcss3" >' . $count . ' </a>';
                    }
                }

            }
            if ($page < $count) {
                $value .= '<a href="' . $relay . '?page=' . $up . '" class="formcss3">></a> <a href="' . $relay . '?page=' . $count . '" class="formcss3">' . L_LAST . '</a>';
            }
        }
        return $value;
    }


    function paginate1($sql, $amount, $relay, $posts_id)
    {
        global $db, $page;
        $query = $db->query("$sql");
        $post = "$posts_id";
        $number = mysqli_num_rows($query);
        $number_two = $number;
        $count = '0';
        $value = '';
        $final_number = $number / $amount;
        if ($number) {
            if ($page > 1) {
                $down = $page - 1;
                $value .= '<a href="' . $relay . '" class="formcss3">' . L_FIRST . '</a> <a href="' . $relay . '&amp;page=' . $down . '" class="formcss3"><</a>';
            } else {

            }
            while ($number_two > 0) {
                $number_two -= $amount;
                $count = $count + 1;
                $up = $page + 1;

                $low = $page - 5;
                $high = $page + 5;
            }
            if ($page < $count) {
                $value1 .= '' . $relay . '&page=' . $count . '';
            } else {
                $value1 .= '' . $relay . '&page=' . $count . '';
            }
        }
        return $value1;
    }


    function viewing($location)
    {
        global $db, $user;
        $user_sql = $db->query("SELECT * FROM online WHERE account_id != '-1' AND location = '$location'");
        $users = '';
        while ($user_row = mysqli_fetch_array($user_sql)) {
            $users[] = $user->name($user_row['account_id']);
        }
        $users = implode(", ", $users);
        if (!$users) {
            $users = L_NONE;
        }
        return $users;
    }

}

class secure
{

    function clean($content)
    {
        global $db;
        $content = $db->real_escape_string(htmlspecialchars($content));
        return $content;
    }

    function cleanForURL($string) {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

    function verify_email($email)
    {
        global $db;
        $email_check = strstr($email, '@');
        $ban_sql = $db->fetch("SELECT id FROM  forum_banlist WHERE value LIKE '$email_check'");
        if ($ban_sql) {
            $check = 'banned';
        } else if (preg_match("/^[a-z0-9]+([_\\.-][a-z0-9]+)*@([a-z0-9]+([\.-][a-z0-9]+)*)+\\.[a-z]{2,}$/i", $email)) {
            $email_sql = $db->fetch("SELECT id FROM accounts WHERE email LIKE '$email'");
            if ($email_sql) {
                $check = "exist";
            } else {
                $check = "true";
            }
        } else {
            $check = "false";
        }

        return $check;
        unset($email, $email_sql, $check);
    }

    function verify_name($name)
    {
        global $prefix, $db;
        $name = $this->clean($name);
        $name_sql = $db->fetch("SELECT id FROM accounts WHERE name LIKE '$name'");
        $ban_sql = $db->fetch("SELECT id FROM " . $prefix . "_banlist WHERE value LIKE '$name'");
        if ($ban_sql) {
            $check = 'banned';
        } else if ($name_sql) {
            $check = 'exist';
        } else {
            $check = 'false';
        }

        return $check;
        unset($name, $name_sql, $check);
    }



    function password()
    {

        $chars = "abcdefghijkmnopqrstuvwxyz023456789";
        srand((double)microtime() * 1000000);
        $i = 0;
        $pass = '';
        while ($i <= 7) {
            $num = rand() % 33;
            $tmp = substr($chars, $num, 1);
            $pass = $pass . $tmp;
            $i++;
        }
        return $pass;
    }

    function hashedPassword($password) {
        return password_hash($password, PASSWORD_DEFAULT);
    }

}


//User Functions
class user
{

    public function createUser($name, $email, $pass, $ip, $activation_code, $activated)
    {
        global $db;
        if (!$this->isUserExist($email)) {


            $stmt = $db->prep()->prepare("INSERT INTO accounts (name, email, password, ip , joined, lastlogin, activation_code, activated) VALUES (?, ?, ?, ?, UNIX_TIMESTAMP(), UNIX_TIMESTAMP(), ?, ?)");
            $stmt->bind_param("sssssi", $name, $email, $pass, $ip, $activation_code, $activated);
            if ($stmt->execute()) {
                return 1; //USER_CREATED;
                echo'created';
            } else {
                return 0; //USER_NOT_CREATED;
                echo'error';
            }
        } else {
            return 2; //USER_ALREADY_EXIST;
        }
    }

    public function getPasswordForUser($email) {
        global $db;
        $stmt = $db->prep()->prepare("SELECT password FROM accounts WHERE email = ?  ORDER BY id DESC LIMIT 1");
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($pass);
        $stmt->fetch();
        //$result = $stmt->get_result(); // get the mysqli result
        return $pass;
    }

    function avatar($id, $path = './')
    {
        global $system, $siteaddress;
        if ($system->confdata('avatar') == '0') {
            $avatar = '<img src="./images/avatars/default.png" alt=""/>';
        } else {
            $file = $path . "./images/avatars/" . $id . "";
            $avatar_url = './images/avatars/' . $id . '';
            if (file_exists("$file.png")) {
                $avatar = '' . $avatar_url . '.png';
            } else
                if (file_exists("$file.gif")) {
                    $avatar = '<img src="' . $avatar_url . '.gif" alt=""/>';
                } else
                    if (file_exists("$file.jpg")) {
                        $avatar = '<img src="' . $avatar_url . '.jpg" alt=""/>';
                    } else
                        if (file_exists("$file.jpeg")) {
                            $avatar = '<img src="' . $avatar_url . '.jpeg" alt=""/>';
                        } else {
                            $avatar = './images/avatars/default.png';
                        }
        }
        return $avatar;
        unset($avatar);
    }


    public function getUserByEmail($email)
    {
        global $db;
        $stmt = $db->prep()->prepare("SELECT id FROM accounts WHERE email = ?  ORDER BY id DESC LIMIT 1");
        $stmt->bind_param("s", $email);
        $stmt->execute();
       // $stmt->store_result();
        //$result = $stmt->get_result(); // get the mysqli result
        return $stmt->get_result()->fetch_assoc();
        $stmt->close();
    }

    private function isUserExist($email)
    {
        global $db;
        $stmt = $db->prep()->prepare("SELECT id FROM accounts WHERE email = ?");
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $stmt->store_result();
        return $stmt->num_rows > 0;
        $stmt->close();
    }

    public function getEmailString($email) {
        global $db;
        $stmt = $db->prep()->prepare("SELECT email FROM accounts WHERE email = ?");
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $stmt->store_result();
        $stmt->bind_result($emaill);
        $stmt->fetch();
        //$result = $stmt->get_result(); // get the mysqli result
        return $emaill;
    }

    public function updateLPIP($email, $lpip) {
        global $db;
        $stmt = $db->prep()->prepare("UPDATE accounts SET lpip = ?, lastlogin = UNIX_TIMESTAMP() WHERE email = ?");
        $stmt->bind_param("is", $lpip, $email);
        if ($stmt->execute()) {
            return 1; //Sucessful;
        } else {
            return 0; //Not Sucessful;
        }
        $stmt->close();
    }

    public function updateEmail($UserID, $email)
    {
        global $db;
        $stmt = $db->prepare("UPDATE users SET email = ? WHERE UserID = ?");
        $stmt->bind_param("ss", $email, $UserID);
        $stmt->execute();
        //$stmt->fetch();
        if ($stmt->execute()) {
            return 1; //Sucessful;
        } else {
            return 0; //Not Sucessful;
        }
        $stmt->close();
    }
    public function updatePassword($UserID, $password)
    {
        global $db;
        $stmt = $db->prepare("UPDATE users SET password = ? WHERE UserID = ?");
        $stmt->bind_param("ss", $password, $UserID);
        $stmt->execute();
        //$stmt->fetch();
        if ($stmt->execute()) {
            return 5; //USER_CREATED;
        } else {
            return 6; //USER_NOT_CREATED;
        }

        $stmt->close();
    }

    public function fetchAllUserInfoForUser($email, $lpip) {
        global $db;
        $stmt = $db->prep()->prepare("SELECT * FROM accounts WHERE email = ? AND lpip = ?");
        $stmt->bind_param("si", $email, $lpip);
        $stmt->execute();
        $result = $stmt->get_result();
        $assoc = $result->fetch_assoc();

        return $assoc;
    }

    function name($id)
    {
        global $db, $siteaddress, $prefix;
        $result = $db->fetch("SELECT name FROM accounts WHERE id = '$id'");
        if ($result) {
            $username = $result['name'];
        } else {
            $username = L_GUEST;
        }
        $result = $db->fetch("SELECT * FROM accounts WHERE id = '$id'");

        $username = '' . $username . '';
        return $username;
        unset($username);
    }

    function cname($id)
    {
        global $db, $siteaddress, $prefix;
        $result = $db->fetch("SELECT name FROM crews WHERE id = '$id'");
        if ($result) {
            $username = $result['name'];
        } else {
            $username = 'N/A';
        }
        $result = $db->fetch("SELECT * FROM crews WHERE id = '$id'");
        if ($result) {
            $css = $db->fetch("SELECT class FROM crews WHERE id = '" . $result['id'] . "'");
            $css = $css['class'];
        } else {
            $css = '#000';
        }
        $username = '<a href="http://senior.walentsoftware.com/?page=profile&amp;crew=' . $id . '">' . $username . '</a>';
        return $username;
        unset($username);
    }


    function status($user_id)
    {
        global $db;
        $result = $db->fetch("SELECT id FROM online WHERE account_id = '$user_id'");

        if ($result) {
            $status = '<img src="./template/default/images/online.png" style="vertical-align: middle;" />';
        } else {
            $status = '<img src="./template/default/images/offline.png" style="vertical-align: middle;" />';
        }

        return $status;
    }

    function friend($user_id)
    {
        global $db, $id, $prefix;


        $check = $db->fetch("SELECT * FROM forum_friends WHERE account_id='$id' AND friend_id='$user_id'");
        if ($check) {
            $link = '<a href="?area=friends&amp;remove=' . $user_id . '" class="normfont"><img style="max-width:120px;" src="./template/default/images/removefriend.png" alt="" border="0"/></a>';
        } else {
            $link = '<a href="?area=friends&amp;add=' . $user_id . '" class="normfont"><img style="max-width:120px;" src="./template/default/images/addfriend.png" alt="" border="0"/></a>';
        }


        return $link;
    }



    function gender($user_id)
    {
        global $db;
        $result = $db->fetch("SELECT gender FROM accounts WHERE id = '$user_id'");
        if ($result) {
            if ($result['gender'] == '1') {
                $gender = L_MALE;
            } else if ($result['gender'] == '2') {
                $gender = L_FEMALE;
            } else {
                $gender = L_HIDDEN;
            }
        } else {
            $gender = L_HIDDEN;
        }
        return $gender;
    }




    function group($id)
    {
        global $db, $prefix, $account;

        if (isset($account['id'])) {
            $result = $db->fetch("SELECT * FROM forum_groups_members WHERE account_id = '$id'");
            if ($result) {
                $value = $result['group_id'];
            }
        } else {
            $value = '1';
        }
        return $value;
    }

    function groupname($id)
    {
        global $db, $prefix, $account, $system;
        $ids = $db->fetch("SELECT * FROM forum_groups_members WHERE account_id = '$id'");
        $gid = $ids['group_id'];
        $result = $db->fetch("SELECT * FROM forum_groups WHERE id = '$gid'");
        if ($result) {
            $value = '' . $system->present($result['title']) . '';
        } else {
            $value = L_ERROR;
        }

        return $value;
    }


    function proname($user_id)
    {
        global $db;
        $result = $db->fetch("SELECT name FROM accounts WHERE id = '$user_id'");
        if ($result) {
            $proname = $result['name'];
        } else {
            $proname = L_GUEST;
        }
        return $proname;
    }




    function value($id, $value)
    {
        global $db, $prefix, $account;
        $result = $db->fetch("SELECT * FROM accounts WHERE id = '$id'");
        $value = $result['' . $value . ''];
        return $value;
    }

}

class forum
{

    function forum_permission($forum_id, $group_id, $value)
    {
        global $db, $prefix;
        $result = $db->fetch("SELECT " . $value . " FROM forum_forums_permission WHERE forum_id = '$forum_id' AND group_id = '$group_id'");
        if ($result) {
            $value = $result['' . $value . ''];
        } else {
            $value = '0';
        }
        return $value;
    }

    function category_permission($category_id, $group_id, $value)
    {
        global $db, $prefix;
        $result = $db->fetch("SELECT " . $value . " FROM " . $prefix . "_categories_permission WHERE category_id = '$category_id' AND group_id = '$group_id'");
        if ($result) {
            $value = $result['' . $value . ''];
        } else {
            $value = '0';
        }
        return $value;
    }

    function event($event)
    {
        global $db, $account;
        if ($event == 'reply1' || $event == 'reply' || $event == 'quote') {
            $db->query("UPDATE accounts SET postcount = postcount + 1 , lastpost = UNIX_TIMESTAMP() WHERE id = '" . $account['id'] . "'");
        }
    }

    function paginate($sql, $amount, $relay)
    {
        global $page, $db;
        $query = $db->query("$sql");
        $num = mysqli_num_rows($query);
        $num2 = $num;
        $count = 0;
        $pagestext = '';
        $final = $num / $amount;

        if ($num > $amount) {
            $pagestext .= '<br /><a href="' . $relay . '" class="normfont" style="color: #fff;">Pages: </a>';
            while ($num2 > 0) {
                $num2 -= $amount;
                $count++;
                $low = $page - 5;
                $high = $page + 5;

                if ($count > $low && $count < $high) {
                    $pagestext .= '<a href="' . $relay . '&amp;page_num=' . $count . '" class="normfont" style="margin-right: 4px; text-decoration: underline;">' . $count . '</a>';
                }
            }

            $pagestext .= '<a href="' . $relay . '&amp;page_num=' . $count . '" class="normfont"> </a>';
        }

        return $pagestext;
    }

    function attachment($file, $area, $id)
    {
        global $db, $forum, $system, $prefix, $account, $forum_data, $group_id;

        if ($area == 'forum') {
            $link = './?area=viewforum&amp;f=' . $id . '';
        } else {
            $link = './?area=viewtopic&amp;t=' . $id . '';
        }

        if ($forum->forum_permission($forum_data['id'], $group_id, 'upload') != '1') {
            $system->message(L_ERROR, L_PERMISSION_ERROR_ACTION, $link, L_CONTINUE);
        }

        $filename = stripslashes($file);

        // Get Extention
        $i = strrpos($filename, ".");
        if (!$i) {
            return "";
        }
        $l = strlen($filename) - $i;
        $extension = substr($filename, $i + 1, $l);


        $extension = strtolower($extension);

        $ext = $db->fetch("SELECT * FROM " . $prefix . "_extensions WHERE value LIKE '" . $extension . "' ;");

        // Is Extention Allowed
        if (!$ext) {
            $system->message(L_ERROR, L_ATTACHMENT_ERROR_EXTENTION, $link, L_CONTINUE);
        }

        $newname = "uploads/" . $account['id'] . "-" . time() . "--$filename";
        $copied = copy($_FILES['attachment']['tmp_name'], $newname);
        $size = filesize($newname);

        if ($size > $system->confdata('attach_filesize')) {
            unlink("$newname");
            $system->message(L_ERROR, L_ATTACHMENT_ERROR_FILESIZE, $link, L_CONTINUE);
        }

        if (!$copied) {
            unlink("$newname");
            $system->message(L_ERROR, L_ATTACHMENT_ERROR, $link, L_CONTINUE);
        }


        $db->query("INSERT INTO " . $prefix . "_attachments (account_id,file,date) VALUES ('" . $account['id'] . "','$newname',UNIX_TIMESTAMP())");

        $attachment = $db->fetch("SELECT * FROM " . $prefix . "_attachments WHERE account_id = '" . $account['id'] . "' ORDER BY date DESC LIMIT 1 ;");
        $attachment_id = $attachment['id'];
        return $attachment_id;
    }

}

class reports {

    public function fetchAllReportsForDoctor() {

        global $db;
        $stmt = $db->prep()->prepare("SELECT * FROM reports");
        $stmt->execute();
        $stmt->store_result();
        $result = $stmt->get_result()->fetch_all();
        return $result;
    }

    public function fetchAllReportsByDoctor($id) {

        global $db;
        $stmt = $db->prep()->prepare("SELECT * FROM reports WHERE creator_id = ?");
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $stmt->store_result();
        $result = $stmt->get_result()->fetch_all();
        return $result;
    }

    public function fetchReportsForUser($id) {

        global $db;
        $stmt = $db->prep()->prepare("SELECT * FROM `reports` WHERE `for_id` = ?");
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $stmt->execute() ;
           // $stmt->store_result();
            return $stmt->get_result();




    }

    public function createReport($name, $creator_id, $for_id, $url, $desc)
    {
        global $db;



            $stmt = $db->prep()->prepare("INSERT INTO reports (title, creator_id, for_id, url , date_created, description) VALUES (?, ?, ?, ?, UNIX_TIMESTAMP(), ?)");
            $stmt->bind_param("siiss", $name, $creator_id, $for_id, $url, $desc);
            if ($stmt->execute()) {
                return 1; //USER_CREATED;
            } else {
                return 0; //USER_NOT_CREATED;

            }

    }

}


class videos {

    public function fetchAllVideosForDoctor() {

        global $db;
        $stmt = $db->prep()->prepare("SELECT * FROM videos");
        $stmt->execute();
        $stmt->store_result();
        $result = $stmt->get_result()->fetch_all();
        return $result;
    }

    public function fetchAllVideosByDoctor($id) {

        global $db;
        $stmt = $db->prep()->prepare("SELECT * FROM videos WHERE creator_id = ?");
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $stmt->store_result();
        $result = $stmt->get_result()->fetch_all();
        return $result;
    }

    public function fetchVideosForUser($id) {

        global $db;
        $stmt = $db->prep()->prepare("SELECT * FROM `videos` WHERE `for_id` = ?");
        $stmt->bind_param("i", $id);
        $stmt->execute();
        $stmt->execute() ;
        // $stmt->store_result();
        return $stmt->get_result();




    }

    public function createVideo($name, $creator_id, $for_id, $url, $desc)
    {
        global $db;

        $stmt = $db->prep()->prepare("INSERT INTO videos (title, creator_id, for_id, url , date_created, description) VALUES (?, ?, ?, ?, UNIX_TIMESTAMP(), ?)");
        $stmt->bind_param("siiss", $name, $creator_id, $for_id, $url, $desc);
        if ($stmt->execute()) {
            return 1; //USER_CREATED;
        } else {
            return 0; //USER_NOT_CREATED;

        }

    }

}
