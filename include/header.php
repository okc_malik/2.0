<?php
// Error reporting:
error_reporting(E_ALL ^ E_NOTICE);
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
// Check if the info is good and ready.
include('./system/config.php');

//Require the db file.
include('./system/db.php');

// Load the seleceted database.
$db = new db($dbhost, $dbuser, $dbpassword, $dbmaster);

//Require the functions.
require('function.php');

//Start new classes
$system = new system();
$secure = new secure();
$user = new user();
$forum = new forum();

// List session name, and start the session
session_name('session');
session_start();

//Set time-zone.
date_default_timezone_set('GMT');

// Check if the user is logged in by their cookies.
if (isset($_COOKIE["email"]) && isset($_COOKIE["lpip"])) {

    $email =  $secure->clean($_COOKIE["email"]);
    $lpip = $_COOKIE['lpip'];
} else {
    $email = '';
    $lpip = '';
    //If there are no cookies, clear the past cookies if logged on on different device.
    $year = time() - 3600;
    //setcookie('email', $_SESSION['email'], $year, '/', '.senior.walentsoftware.com');
    //setcookie('lpip', $_SESSION['lpip'], $year, '/', '.senior.walentsoftware.com');
}

//Get the account iformation of the user(s) or guest.
$account = $user->fetchAllUserInfoForUser($email, $lpip);


//Get the website url
$url = $system->confdata('url');

$siteaddress = "$url/";

$document_root = $_SERVER['DOCUMENT_ROOT'];
$root = "$document_root/";

// SESSION
$session_location = $_SERVER['REQUEST_URI'];
$session_id = session_id();
$ip = $_SERVER['REMOTE_ADDR'];
if ($account['id']) {
    $id = $account['id'];
} else {
    $id = '-1';

}
//$user_id = $secure->clean($_GET['user']);
$group_id = $user->group($account['id']);

// Delete/instert people from the online table
//$db->query("DELETE FROM online WHERE session= '$session_id';");
//$db->query("INSERT INTO online ( time, account_id , session , ip , location , group_id ) VALUES  (unix_timestamp(), '$id' , '$session_id' , '$ip','$session_location' ,'$group_id' );");
//$db->query("DELETE FROM online WHERE time < unix_timestamp()-90;");
//$db->query("UPDATE accounts SET lastlogin = unix_timestamp() WHERE id = '$id'");
//$onlinelist = $db->fetch("SELECT * FROM online WHERE account_id = '$id'");


// Get the template of the website
if ($account['tpl'] && $system->confdata('usertemplate') == '1') {
    $template = $account['tpl'];
    if (file_exists("$root/template/$template/header.tpl")) {
        $template = $template;
    } else {
        $template = $system->confdata('template');
    }
} else {
    $template = $system->confdata('template');
}

//Start new style
require("./system/parser.php");
$STYLE = new style();
// Get the index language file for the main index.
$language = $system->confdata('language');
require("" . $root . "/lang/$language/index.php");
// PARSE STYLE
$output = '';

$link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ?
        "https" : "http") . "://" . $_SERVER['HTTP_HOST'] .
    $_SERVER['REQUEST_URI'];

//echo $link;
switch ($link) {
    case 'https://senior.walentsoftware.com/?account=login' :
        $tpl = $STYLE->open('auth/login-header.tpl');
        break;
    case 'https://senior.walentsoftware.com/?account=register':
        $tpl = $STYLE->open('auth/register-header.tpl');
        break;
    default:
        if (!$account['id']) {
            header("Location: ./?account=login"); /* Redirect browser */

            /* Make sure that code below does not get executed when we redirect. */
            exit;
        }
        $tpl = $STYLE->open('header.tpl');
}


// Is the user alllowed to see acp/rc?

if ($system->group_permission($user->group($account['id']), 'acp') == '1') {
    $STYLE->getcode('is_admin', $tpl);
} else {
    $tpl = str_replace(array($STYLE->getcode('is_admin', $tpl)), '', $tpl);
}
if ($system->group_permission($user->group($account['id']), 'rc') == '1') {
    $gu = mysqli_num_rows($db->query("SELECT * FROM forum_reports WHERE actioned='0'"));
    $rc_link = '<a href="' . $url . '/rc/" class="ui">(' . $gu . ') Reports</a> ';
} else {
    $rc_link = '';
}


// CHANGE NAVIGATION

$mail = mysqli_num_rows($db->query("SELECT id FROM forum_mail WHERE to_id = '" . $account['id'] . "' AND marked = '0'"));
//$mail = str_replace('[MAIL]', $mail, '<a href="/?area=forum&s=mail"  class="ui">&nbsp; '.$mail.' new messages</a>');
$panel = '' . $user->name($account['id']) . '';
$tpl = $STYLE->tags($tpl, array("PANEL" => $panel));
$post_row = $db->fetch("SELECT * FROM announcments ORDER BY id DESC");
$output .= $STYLE->tags($tpl, array(
    "L_ANN" => ($post_row['text']),
    //"TOPIC_STUFF" => $topic,
    //"NEW" => $new,
    "PANEL" => $account['id'],
    "PANEL1" => $system->present($account['name']),
    "EMAIL" => $system->present($account['email']),
    "UID" => $id,
    "MAIL" => $mail,
    //"TOPP" => $topp,
    "AVATAR" => $user->avatar($account['id']),
    "NAME" => $system->present($account['name']),
    "STATUS" => $user->status($account['id']),
    "JOINED" => $system->time($account['joined']),
    "LASTLOGIN" => $system->time($account['lastlogin']),
    "GENDER" => $user->gender($account['id']),
    "RC" => $rc_link, 
    //"ACP" => $admin_link,
    "LINKS" => L_LINKS,
    "ACPLINK" => AL_LINKS,
    "L_RANK" => L_RANK,
    "L_JOINED" => L_JOINED,
    "L_LAST_LOGIN" => L_LAST_LOGIN,
    "L_GENDER" => L_GENDER,
    "L_LOCATION" => L_LOCATION,
    "L_DETAILS" => L_DETAILS,
    "L_SIGNATURE" => L_SIGNATURE));
// Check Banlist
$user_ip = $secure->clean($_SERVER['REMOTE_ADDR']);
$ban_sql = $db->fetch("SELECT * FROM forum_banlist WHERE value = '$user_ip'");
if ($ban_sql) {
    $system->page(L_BANNED, L_BANNED_IP);
}

// Check if activated
$current_url = preg_replace('/^([^&]*).*$/', '$1', str_replace(array($siteaddress), '', $system->current_url()));
//if ($system->confdata('activation') == '1' && $account && $account['activated'] != '1' && $current_url != '?area=activate' && $current_url != '?account=logout') {
//    $system->redirect("http://forum.senior.walentsoftware.com/?area=activate&u=" . $account['id'] . "");
//}
$ref = 'javascript:window.history.go(-1)';
$ref1 = 'javascript:window.history.go(-2)';
if (isset($_POST['login'])) {
    if (!isset($_POST['email'])) {
        $email = '';
    } else {
        $email = $secure->clean($_POST['email']);
    }

    if (!isset($_POST['pass'])) {
        $pass = '';
    } else {
        $pass = $secure->clean(md5($_POST['pass']));
    }


    if ($email && $pass) {
        // CHECK EMAIL AND DEFINE LOGIN SQL
        $check_email = $secure->verify_email($email);
        if ($check_email == 'exist') {
            $login = $db->fetch("SELECT email FROM accounts WHERE `email` = '$email' AND `password` = '$pass'");
        } else {
            $login = $db->fetch("SELECT email FROM accounts WHERE `name` LIKE '$email' AND `password` = '$pass'");
            $email = $login['email'];
        }

        if (!$login) {
            $system->message1(L_ERROR, L_LOGIN_ERROR, '/?account=login', L_CONTINUE);
        } else {
            $_SESSION['email'] = $login['email'];
            $lpip = rand(500, 20000);
            $_SESSION['lpip'] = "$lpip";


            $update = $db->query("UPDATE accounts SET lpip = '$lpip', lastlogin = UNIX_TIMESTAMP() WHERE `email` = '$email' AND `password` = '$pass'");

            $year = time() + 31536000;
            if ($_POST['remember']) {

                setcookie('email', $_SESSION['email'], $year, '/', '.senior.walentsoftware.com');
                setcookie('lpip', $_SESSION['lpip'], $year, '/', '.senior.walentsoftware.com');
            } else {
                setcookie('email', $_SESSION['email'], NULL, '/', '.senior.walentsoftware.com');
                setcookie('lpip', $_SESSION['lpip'], NULL, '/', '.senior.walentsoftware.com');

            }

            //setcookie(session_name(),session_id(),time()+1000*100*24*15);
            //setcookie(email,$email,time()+1000*100*24*15);

            // LOGGED IN MESSAGE
            $system->message(L_LOGIN, L_LOGIN_MESSAGE, $ref, L_CONTINUE);
            $system->redirect('./');
        }


    } else {
        // ERROR MESSAGE FOR NO DETAILS PROVIDED
        $system->message1(L_ERROR, L_LOGIN_ERROR, $ref, L_CONTINUE);
    }
} else {
    // LOGIN FORM

}

