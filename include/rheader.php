<?php

require("db.php");
require("function.php");
$system = new system();
$secure = new secure();
$user = new user();
$forum = new forum();
// CHECK CONFIG
include("config.php");
// LOAD DATABASE
$db = new db();
$db->connect($dbhost, $dbuser, $dbpassword, $dbmaster);
// SESSION
session_name($system->confdata('session'));
session_start();

date_default_timezone_set('GMT');
// CHECK IF USER IS LOGGED IN
if (isset($_COOKIE["email"]) && isset($_COOKIE["lpip"])) {

    $email = $_COOKIE["email"];
    $lpip = $_COOKIE['lpip'];


} else {
    $email = '';
    $lpip = '';
}
//if (!isset($_SESSION['email'])) {
//    $email = '';
//} else {
//   $email = $_SESSION['email'];
//}
//if (!isset($_SESSION['lpip'])) {
//    $lpip = '';
//} else {
//   $lpip = $_SESSION['lpip'];
//}
$account = $db->fetch("SELECT * FROM accounts WHERE email = '$email' AND lpip = '$lpip'");
$url = $system->confdata('url');
$path = $system->confdata('path');
if ($path) {
    $siteaddress = "$url/";
} else {
    $siteaddress = "$url/";
}
$document_root = $_SERVER['DOCUMENT_ROOT'];
$root = "$document_root/";
// SESSION
$session_location = $_SERVER['REQUEST_URI'];
$session_id = session_id();
$ip = $_SERVER['REMOTE_ADDR'];
if ($account['id']) {
    $id = $account['id'];
} else {
    $id = '-1';
}
$user_id = $secure->clean($_GET['user']);
$group_id = $user->group($account['id']);
if ($system->confdata('siteclosed') == '1' && $system->group_permission($user->group($account['id']), 'acp') != '1') {
    $system->page(L_CLOSED, L_SITE_CLOSED);
}

$db->query("DELETE FROM online WHERE session= '$session_id';");
$db->query("INSERT INTO online ( time, account_id , session , ip , location , group_id ) VALUES  (unix_timestamp(), '$id' , '$session_id' , '$ip','$session_location' ,'$group_id' );");
$db->query("DELETE FROM online WHERE time < unix_timestamp()-90;");
$db->query("UPDATE accounts SET lastlogin = unix_timestamp() WHERE id = '$id'");
$onlinelist = $db->fetch("SELECT * FROM online WHERE account_id = '$id'");


//setcookie(session_name(),session_id(),time()+1000*100*24*15);
// STYLES SYSTEM

$template = 'default';

$temp = 'default';
require("parser.php");
$STYLE = new style();
// LANGUAGE
$language = $system->confdata('language');
require("" . $root . "/lang/$language/index.php");
// PARSE STYLE
$output = '';
$tpl = $STYLE->open('rheader.tpl');
// Find Your Group

if ($account['bantime'] > time()) {
    $system->page(L_TEMPORARY_BAN, str_replace('[TIME]', $system->time($account['bantime']), L_TEMPORARY_BAN_MSG));
}

$url = $system->confdata('url') . '/' . $system->confdata('path') . '';
if ($system->group_permission($user->group($account['id']), 'acp') == '1') {
    $admin_link = '<a href="' . $url . 'acp/" class="notification_links">ACP</a> ';
} else {
    $admin_link = '';
}
if ($system->group_permission($user->group($account['id']), 'rc') == '1') {
    $rc_link = '<a href="' . $url . 'rc/" class="notification_links">Reports</a> ';
} else {
    $rc_link = '';
}

// LATEST AND RECENT
$group_id = $user->group($account['id']);
$sql = "SELECT * FROM " . $prefix . "_forums_permission WHERE group_id = '$group_id'";
$result = $db->query("" . $sql . "");
$allowed_forums = "forum_id = '0'";
while ($thisrow = mysql_fetch_array($result)) {
    $forum_id = $thisrow['forum_id'];

    if ($forum->forum_permission($forum_id, $group_id, 'view') == '1') {
        $allowed_forums .= " OR forum_id = '" . $forum_id . "'";
    }
}

$timer = time() + 86400;

$topic_sql = $db->query("SELECT *  FROM " . $prefix . "_late_topic WHERE (" . $allowed_forums . ") ORDER BY date DESC LIMIT 10;");
$users = '';
while ($topic_row = mysql_fetch_array($topic_sql)) {
    $topic_data = $db->fetch("SELECT * FROM " . $prefix . "_topics WHERE id = '" . $topic_row['topic_id'] . "'");
    $name = $system->present($topic_data['title']);
    if (strlen($name) > 9) {
        $tname = '' . $system->present(substr($topic_data['title'], 0, 15)) . '...';
    } else {
        $tname = $system->present($topic_data['title']);
    }

    if ($account['tpl'] == 'Ichi' && $system->confdata('usertemplate') == '1') {
        $topic .= '<li>&#x2717;      <a href="http://forum.senior.walentsoftware.com' . $topic_row['url'] . '#post' . $topic_row['post_id'] . '"><u>' . $tname . '</u></a> on <span> ' . $system->time1($topic_row['date']) . '  </span> - ' . $user->name($topic_row['author_id']) . ' </li>';

    } else {
        $topic .= '<div id="activity_items"><span style="margin-right: 10px; vertical-align: middle;">&#x2717; </span><a href="http://forum.senior.walentsoftware.com' . $topic_row['url'] . '">' . $tname . '</a> on <span style="color: #2e2e2e;"> ' . $system->time($topic_row['date']) . '  </span> - ' . $user->name($topic_row['author_id']) . ' </div>';
    }
}
if (!$topic) {
    $topic = L_NONE;
}

$new_sql = $db->query("SELECT *  FROM " . $prefix . "_topics WHERE (" . $allowed_forums . ") ORDER BY id DESC LIMIT 10;");
$users = '';
while ($row = mysql_fetch_array($new_sql)) {

    $name1 = $system->present($row['title']);
    if (strlen($name1) > 9) {
        $tname1 = '' . $system->present(substr($row['title'], 0, 15)) . '...';
    } else {
        $tname1 = $system->present($row['title']);
    }

    if ($account['tpl'] == 'Ichi' && $system->confdata('usertemplate') == '1') {
        $new .= '<li>&#x2717;        <a href="http://forum.senior.walentsoftware.com?area=forum&s=topic&amp;t=' . $row['id'] . '"><u>' . $tname1 . '</u></a> on <span> ' . $system->time1($row['date']) . '  </span> - ' . $user->name($row['author_id']) . ' </li>';

    } else {
        $new .= '<div id="activity_items"<span style="margin-right: 10px; vertical-align: middle;">&#x2717; </span><a href="http://forum.senior.walentsoftware.com?area=forum&s=topic&amp;t=' . $row['id'] . '">' . $tname1 . '</a> on <span style="color: #2e2e2e;"> ' . $system->time($row['date']) . '  </span> - ' . $user->name($row['author_id']) . ' </div>';
    }

}
if (!$new) {
    $new = L_NONE;
}

// CHANGE NAVIGATION

$mail = mysql_num_rows($db->query("SELECT id FROM " . $prefix . "_mail WHERE to_id = '" . $account['id'] . "' AND marked = '0'"));
$mail = str_replace('[MAIL]', $mail, L_PANEL_MAIL);
$panel = 'Welcome back ' . $user->name($account['id']) . ', ' . $mail . ' ';
$tpl = $STYLE->tags($tpl, array("PANEL" => $panel));

$post_row = $db->fetch("SELECT * FROM " . $prefix . "_news ORDER BY id DESC");
$output .= $STYLE->tags($tpl, array("L_HOME" => L_HOME, "LINKS" => L_LINKS, "L_REGISTER" => L_REGISTER, "L_MAIL" => L_MAIL, "L_LOGIN" => L_LOGIN, "L_LOGOUT" => L_LOGOUT, "L_ACCOUNT" => L_ACCOUNT, "L_ANN" => $system->bbcode($post_row['text']), "TOPIC_STUFF" => $topic, "NEW" => $new,
    "UID" => $id,
    "AVATAR" => $user->avatar($account['id']),
    "NAME" => $user->name($account['id']),
    "STATUS" => $user->status($account['id']),
    "RANK" => $user->rank($account['id']),
    "JOINED" => $system->time($account['joined']),
    "LASTLOGIN" => $system->time($account['lastlogin']),
    "GENDER" => $user->gender($account['id']),
    "LOCATION" => $system->present($account['location']),
    "SIGNATURE" => $system->bbcode($account['signature']),
    "POSTCOUNT" => $user->postcount($account['id']),
    "PRONAME" => $user->proname($account['id']),
    "SITERANK" => $user->siterank($account['id']),
    "NICKN" => $user->nickn($account['id']),
    "WEBSITE" => $user->website($account['id']),
    "FL" => $user->flocation($account['id']),
    "PAGE" => $user->page($account['id']),
    "RC" => $rc_link, "ACP" => $admin_link,
    "LINKS" => L_LINKS,
    "ACPLINK" => AL_LINKS,
    "L_RANK" => L_RANK,
    "L_JOINED" => L_JOINED,
    "L_LAST_LOGIN" => L_LAST_LOGIN,
    "L_GENDER" => L_GENDER,
    "L_LOCATION" => L_LOCATION,
    "L_DETAILS" => L_DETAILS,
    "L_SIGNATURE" => L_SIGNATURE,
    "L_POST_COUNT" => L_POST_COUNT));
// Check Banlist
$user_ip = $secure->clean($_SERVER['REMOTE_ADDR']);
$ban_sql = $db->fetch("SELECT * FROM " . $prefix . "_banlist WHERE value = '$user_ip'");
if ($ban_sql) {
    $system->page(L_BANNED, L_BANNED_IP);
}

// Check if activated
$current_url = preg_replace('/^([^&]*).*$/', '$1', str_replace(array($siteaddress), '', $system->current_url()));
if ($system->confdata('activation') == '1' && $account && $account['activated'] != '1' && $current_url != '?area=activate' && $current_url != '?area=logout') {
    $system->redirect("" . $siteaddress . "?area=activate&u=" . $account['id'] . "");
}

