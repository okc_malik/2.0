<?php
//Main index page. Does all the urls, and gets the php files.

//Turn off all error reportings


//require header.php 
require('./include/header.php');

// Require language file, so we can shorten items.
require("" . $root . "/lang/$language/forum.php");

//Tite of the page, leave blank, we will continue it in the header, footer, and files in the core directory.
$page_title = '';

//Url styles
if (isset($_GET['page'])) {
    $area = $secure->clean($_GET['page']);
} else {
    $area = '';
}

if (!isset($_GET['section'])) {
    $section = '';
} else {
    $section = $_GET['section'];
}

if (!isset($_GET['account'])) {
    $ac = '';
} else {
    $ac = $_GET['account'];
}

//Shorten the urls & include the files.

if ($ac == 'login' && !$account['id']) {
    $page_title = '<a href="' . $siteaddress . '?account=login" class="navfont">' . L_LOGIN . '</a>';
    $page_title_pro = ' > ' . L_LOGIN . '';
    include("./core/login.php");
} else if ($ac == 'logout' && $account['id']) {
    $page_title = '<a href="' . $siteaddress . '?account=logout" class="navfont">' . L_LOGOUT . '</a>';
    $page_title_pro = ' > ' . L_LOGOUT . '';
    include("./core/logout.php");
} else if ($ac == 'register' && !$account['id']) {
    $page_title = '<a href="' . $siteaddress . '?account=register" class="navfont">' . L_REGISTER . '</a>';
    $page_title_pro = ' > ' . L_REGISTER . '';
    include("./core/register.php");
} else if ($area == 'activate') {
    $page_title = '<a href="' . $siteaddress . '?page=activate" class="navfont">' . L_ACTIVATION . '</a>';
    $page_title_pro = ' > ' . L_ACTIVATION . '';
    include("./core/activate.php");
} else if ($ac == 'ucp' && $account['id']) {
    $page_title = '<a href="' . $siteaddress . '?account=ucp" class="navfont">' . L_USER_CONTROL_PANEL . '</a>';
    $page_title_pro = ' > ' . L_USER_CONTROL_PANEL . '';
    include("./core/ucp.php");
} else if ($area == 'crew' && $account['id']) {
    $page_title = '<a href="' . $siteaddress . '?page=crew" class="navfont">Crew</a>';
    $page_title_pro = ' > Faction';
    include("./core/crew.php");
} else if ($ac == 'lostpassword' && !$account['id']) {
    $page_title = '<a href="' . $siteaddress . '?account=lostpassword" class="navfont">' . L_LOST_PASSWORD . '</a>';
    $page_title_pro = ' > ' . L_LOST_PASSWORD . '';
    include("./core/lostpassword.php");
} else if ($area == 'profile') {
    $page_title = '<a href="' . $siteaddress . '?page=profile&user=' . $account['id'] . '" class="navfont">' . L_PROFILE . '</a>';

    include("./core/profile.php");
} else if ($area == 'characters') {
    $page_title = '<a href="' . $siteaddress . '?page=characters" class="navfont">Character List</a>';

    include("./core/characters.php");
} else if ($area == 'news') {
    $page_title = '<a href="' . $siteaddress . '?page=news" class="navfont">News</a>';

    include("./core/news.php");
} else if ($area == 'missions') {
    $page_title = '<a href="' . $siteaddress . '?page=missions" class="navfont">missions</a>';

    include("./core/missions.php");
} else if ($area == 'category') {
    $page_title = '<a href="' . $siteaddress . '?page=category" class="navfont">Mission Categories</a>';

    include("./core/category.php");
} else if ($area == 'privacy') {
    $page_title = '<a href="' . $siteaddress . '?page=privacy" class="navfont"> Private Policy</a>';

    include("./core/privacy.php");
} else if ($area == 'search') {
    $page_title = '<a href="' . $siteaddress . '?page=search" class="navfont">' . L_SEARCH . '</a>';
    $page_title_pro = ' > ' . L_SEARCH . '';
    include("./core/search.php");
} else if ($area == 'mail' && $account) {
    $page_title = '<a href="' . $siteaddress . '?page=mail" class="navfont">' . L_MAIL . '</a>';
    $page_title_pro = ' > Private Messages';
    include("./core/mail.php");
} else if ($area == 'friends' && $account) {
    include("./core/freinds.php");
} else if ($area == 'memberlist' && $account) {
    include("./core/members.php");
} else if ($area == 'report' && $account) {
    include("./core/report.php");
} else if ($area == 'tos') {
    $page_title = '<a href="' . $siteaddress . '?page=pos" class="navfont">' . L_TERMS_OF_SERVICE . '</a>';
    $page_title_pro = ' > ' . L_TERMS_OF_SERVICE . '';
    $system->page(L_TERMS_OF_SERVICE, $system->confdata('tos'));
} else if ($area == 'reports') {
    include("./core/reports.php");
} else if ($area == 'videos') {
    include("./core/videos.php");
} else {
    $page_title = '<a href="' . $siteaddress . '" class="navfont">' . L_HOME . '</a>';
    $page_title_pro = ' > ' . L_HOME . '';
    include('./core/home.php');
}

//Require the footer.
require("./include/footer.php");
