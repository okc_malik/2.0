<?php
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
class db
{

    protected $db;

    public function __construct($host, $username, $password, $database)
    {
        $this->db = new mysqli($host, $username, $password, $database);

        if ($this->db->connect_errno) {
            throw new Exception('Unable to connect to database [' . $this->db->connect_error . ']');
        }
    }

    //Execute a query  
    public function fetch($query)
    {
        $res = $this->db->query($query);
        return $res->fetch_assoc();
    }

    public function query($query)
    {
        $res = $this->db->query($query);
        return $res;
    }

    public function real_escape_string($query)
    {
        $res = $this->db->real_escape_string($query);
        return $res;
    }


    public function close()
    {
        mysqli_close($this->db);
    }

    public function prep()  {
        return $this->db ;
    }


}  


