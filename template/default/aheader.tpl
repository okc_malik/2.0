<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/html" xmlns="http://www.w3.org/1999/html"><head>
<meta name="Description" http-equiv="Description" content="{METAINFO}" />
<meta name="Keywords" http-equiv="Keywords" content="{METAKEYWORDS}" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>{TITLE}</title>
<link rel="stylesheet" type="text/css" href="./template/{TPL}/inc/style.css" />
<script type="text/javascript" language="javascript" src="./template/Nelo/inc/jquery.js"></script>
<script type="text/javascript" language="javascript" src="./template/Nelo/inc/function.js"></script>
 <script src="./template/Luffy/inc/mootools-1.2-core-yc.js" type="text/javascript"><!--mce:0--></script>
 <!--Toggle effect (show/hide login form) -->
 <script src="./template/Luffy/inc/mootools-1.2-more.js" type="text/javascript"><!--mce:1--></script>
 <script src="./template/Luffy/inc/fx.slide.js" type="text/javascript"><!--mce:2--></script>
<link rel="icon" type="image/png" href="{URL}favicon.ico" />
</head>

<body>
 <!-- BEGIN logged_in -->	<div id="login">
		<div class="loginContent">
		<div class="content clearfix">
			<div class="left" style="border-left: 3px solid #bebebe;">
			    <div style="text-align: center;">
				<span style="font-size:18px">Welcome Message</span>
			    </div>
			    <p class="top_panel_text" style="font-family: 'Familian', Arial, sans-serif !important; font-size: 18px; padding: 10px 10px 6px 10px !important;color: #fff;">
				Welcome to Pirate Zone's Community. Here you can talk everything related to the game and much more. We kindly ask you to read the rules in order to have a nice stay here and avoid a conflict with the moderating team. After you do that you can roam and enjoy!
			    </p>
			</div>
			<div class="left" style="border-left: 3px solid #bebebe;">
			    <div style="text-align: center; margin-top: -4px;">
				<span style="font-size:18px">Account Information</span>
			    </div>
			    <div style="padding: 10px 10px; 6px 10px !important; margin-top: 5px;">
				<p class="top_panel_stats"><span style="vertical-align: middle; margin-right: 10px;">&#x2717; </span> Post count: {POSTCOUNT}</p>
				<p class="top_panel_stats"><span style="vertical-align: middle; margin-right: 10px;">&#x2717; </span> Amount of exp: 0</p>
				<p class="top_panel_stats"><span style="vertical-align: middle; margin-right: 10px;">&#x2717; </span> Site rank: {SITERANK}</p>
				<p class="top_panel_stats"><span style="vertical-align: middle; margin-right: 10px;">&#x2717; </span> Current activity: Pirate-Zone Forum <span style="color: fff">{PAGE}</span></p>
			        <p class="top_panel_stats" style="padding-top: 4px; font-size: 17px; font-style: italic; text-align: center;">You're currently logged in.</p>	
			    </div>
			</div>
			<div class="left right" style="border-left: 3px solid #bebebe; border-right: 3px solid #bebebe;">
			    <div style="text-align: center; margin-bottom: 17px; margin-top: -4px;">
				
			    </div>
			    <p class="top_panel_stats" style="font-size: 21px; letter-spacing: 1px; text-align: center;">Welcome back <u>{PRONAME}</u></p>
			    <div class="clearfix" style="margin-top: 10px;">
				<div style="float: left; padding-left: 15px;">
				     <div style="margin-top: 7px; width: 100px; border: 2px solid #bcbcbc; overflow: hidden; float:left">{AVATAR}</div>
				</div>
				<div style="float: right; padding-right: 15px; text-align: left; padding-right: 0 !important; margin-top: 10px;">
				    <p class="top_panel_stats" style="letter-spacing: 1px;"><span style="vertical-align: middle; margin-right: 10px;">&#x2717; </span> <a href="http://forum.pirate-zone.com/?area=profile&user={UID}" class="top_panel_stats_text">View profile</a></p>
				    <p class="top_panel_stats" style="letter-spacing: 1px;"><span style="vertical-align: middle; margin-right: 10px;">&#x2717; </span> <a href="http://forum.pirate-zone.com/?acc=ucp&mode=details" class="top_panel_stats_text">Edit information</a></p>
				    <p class="top_panel_stats" style="letter-spacing: 1px;"><span style="vertical-align: middle; margin-right: 10px;">&#x2717; </span> <a href="http://forum.pirate-zone.com/?acc=ucp&mode=avatar" class="top_panel_stats_text">Change avatar</a></p>
				    <p class="top_panel_stats" style="letter-spacing: 1px;"><span style="vertical-align: middle; margin-right: 10px;">&#x2717; </span> <a href="http://forum.pirate-zone.com/?acc=ucp&mode=account" class="top_panel_stats_text">Change password</a></p>
				</div>
			    </div>
			</div>
		</div>
	</div>	
		<div class="loginClose"><a href="#" id="closeLogin">Close Panel</a></div>
	</div> 

 
		<div id="top">
	
			<ul class="login">
		    	<li class="left">&nbsp;</li>
		        <li>Hello {NAME}!</li>
				<li>|</li>
				<li><a id="toggleLogin" href="#">Open Panel</a></li>
			</ul> 
		</div> 

<br class="clear" />
<div id="container">
<center><script type="text/javascript"><!--
google_ad_client = "ca-pub-7489288941326797";
/* Header */
google_ad_slot = "8148639462";
google_ad_width = 728;
google_ad_height = 90;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script></center>
<div id="layout"> <a href="./" style="text-decoration: none;"><div id="main_banner">&nbsp;</div></a>
<div id="banner" >
								<center>{ACPLINK}
						
								
								
								</center></div>
<br />
<div id="announcement"><span style="color: #913a42; font-size: 15px; font-weight: bold; margin: -20px -10px -30px 10px;">Announcement:</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{L_ANN}</div>

<div style="float: left; margin-left: 25px;">
<div style="width: 332px;">
<div style="background: url(./template/{TPL}/images/active.png) no-repeat; width: 332px; height: 42px;">
<p class="activity_text">Latest Activity</p></div>
<div class="activity_container" style="margin-top:-2px;"><div>
	
				{TOPIC_STUFF}
				</div></div></div></div>

<div style="float: right; margin-right: 25px;">
<div style="width: 332px;">
<div style="background: url(./template/{TPL}/images/active.png) no-repeat; width: 332px; height: 42px;">
<p class="activity_text">Latest Topics</p></div>
<div class="activity_container" style="margin-top:-2px;"><div>
	
				{NEW}</div></div></div></div>
	    <div style="clear:both">&nbsp;</div>

<div id="breadcrumb">{SITELINK} > {AREA}</div>
<div class="padding_b"></div>

        
          <div id="second_menu">
             <p>{PANEL}<a href="{URL}?area=forum&s=mail" class="notification_links">Mail</a><a href="?area=forum&s=friends" class="notification_links">Friends</a><a href="/?area=forum&s=memberlist" class="notification_links">Member List</a><a href="{URL}?acc=ucp" class="notification_links">User Control Panel</a><a href="/?area=forum&s=search" class="notification_links">Search</a><a href="/?area=forum&s=search&mode=new" class="notification_links">New Posts</a><a href="{URL}?acc=logout" class="notification_links">{L_LOGOUT}</a> <a href="./?area=report" class="abuse_link">Report</a>{ACP} {RC} </p></div>
            <!-- END logged_in -->
           
 <!-- BEGIN logged_out -->
 
	<div id="login">
		<div class="loginContent">
			<form action="#" method="post">
				<label for="email"><b>Username or Email: </b></label>
				<input class="field" type="text" name="email" id="email" value="" size="23" />
				<label for="pass"><b>Password:</b></label>
				<input class="field" type="password" name="pass" id="pass" size="23" />
				<input type="submit" name="submit" value="Login" class="formcss1" />
				<input type="hidden" name="redirect_to" value=""/>
			
			<div class="left">
            	<label for="rememberme"><input name="remember" id="remember" class="rememberme" type="checkbox"  value="1" /> Remember me</label></div></form>
			<div class="right">Not a member? <a href="?acc=register">Register</a> | <a href="?acc=lostpassword">Lost your password?</a></div>
		</div>
		<div class="loginClose"><a href="#" id="closeLogin">Close Panel</a></div>
	</div> 

 
		<div id="top">
	
			<ul class="login">
		    	<li class="left">&nbsp;</li>
		        <li>Hello Guest!</li>
				<li>|</li>
				<li><a id="toggleLogin" href="#">Log In</a></li>
			</ul> 
		</div> 

<br class="clear" />
<div id="container">
<center><script type="text/javascript"><!--
google_ad_client = "ca-pub-7489288941326797";
/* Header */
google_ad_slot = "8148639462";
google_ad_width = 728;
google_ad_height = 90;
//-->
</script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script></center>
<div id="layout"> <a href="./" style="text-decoration: none;"><div id="main_banner">&nbsp;</div></a>
<div id="banner" >
								<center><a href="http://forum.pirate-zone.com" class="large red button">Forum</a>
                                <a href="http://forum.pirate-zone.com/?area=forum&s=topic&t=1" class="large red button">Forum Guide</a>
                                  <a href="http://forum.pirate-zone.com/?area=staff" class="large red button"> Sections and Staff</a>
                                  <a href="http://forum.pirate-zone.com/?acc=ucp" class="large red button">User Control Panel</a>
							<a href="http://forum.pirate-zone.com/?acc=login" class="large red button">Login</a>
						
								
								
								</center></div>
<br />
<div id="announcement"><span style="color: #913a42; font-size: 15px; font-weight: bold; margin: -20px -10px -30px 10px;">Announcement:</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{L_ANN}</div>

<div style="float: left; margin-left: 25px;">
<div style="width: 332px;">
<div style="background: url(./template/{TPL}/images/active.png) no-repeat; width: 332px; height: 42px;">
<p class="activity_text">Latest Activity</p></div>
<div class="activity_container" style="margin-top:-2px;" ><div>
	
				{TOPIC_STUFF}
				</div></div></div></div>

<div style="float: right; margin-right: 25px;">
<div style="width: 332px;">
<div style="background: url(./template/{TPL}/images/active.png) no-repeat; width: 332px; height: 42px;">
<p class="activity_text">Latest Topics</p></div>
<div class="activity_container" style="margin-top:-2px;"><div>
	
				{NEW}</div></div></div></div>
	    <div style="clear:both">&nbsp;</div>

<div id="breadcrumb">{SITELINK} > {AREA}</div>
<div class="padding_b"></div>

        
          <div id="second_menu">
             <p><a href="{URL}?acc=login" class="notification_links">Login</a> or <a href="?acc=register" class="notification_links">Register</a></p></div>
            <!-- END logged_out --> 
           
            <br/>