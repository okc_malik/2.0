<div style="margin-top:10px; width:1000px"></div>
<div id="left">
    <div class="h1" style='cursor:pointer;'>Category Section 1</div>
    <div id="main_list" style="margin-bottom:10px;">
        <!-- BEGIN recent-news -->
        <div class="list_left">
            <img src="./{AVATAR}"
                 style="width: 55px; height: 55px; vertical-align: top; margin-top: 2.4px; float: left; margin-right: 5px; border: solid 3px #fff"/>

            <p class="list_news">{TITLE}</p>

            <p>{TEXT}</p>

            <div style="clear:both">&nbsp;</div>
            <div style="float:left; margin-top: -6px;" class="icon-calendar">&nbsp;&nbsp;{DATE}</div>
            <div style="float:left; margin-top: -6px; margin-left: 20px;" class="icon-comments">&nbsp;&nbsp;12
                Comments
            </div>
            <div style="float:left; margin-top: -6px; margin-left: 20px;" class="icon-eye-open">&nbsp;&nbsp;20 Hits
            </div>
        </div>

        <!-- END recent-news --></div>
    <div style="clear:both">&nbsp;</div>
    <div style="clear:both">&nbsp;</div>

    <div class="h1" style='cursor:pointer;'>Category Section 2</div>
    <div id="main_list" style="margin-bottom:15px">
        <!-- BEGIN recent-news -->
        <div class="list_left">
            <img src="./{AVATAR}"
                 style="width: 55px; height: 55px; vertical-align: top; margin-top: 2.4px; float: left; margin-right: 5px; border: solid 3px #fff"/>

            <p class="list_news">{TITLE}</p>

            <p>{TEXT}</p>

            <div style="clear:both">&nbsp;</div>
            <div style="float:left; margin-top: -6px;" class="icon-calendar">&nbsp;&nbsp;{DATE}</div>
            <div style="float:left; margin-top: -6px; margin-left: 20px;" class="icon-comments">&nbsp;&nbsp;12
                Comments
            </div>
            <div style="float:left; margin-top: -6px; margin-left: 20px;" class="icon-eye-open">&nbsp;&nbsp;20 Hits
            </div>
        </div>

        <!-- END recent-news --></div>

    <div style="clear:both">&nbsp;</div>
    <div style="clear:both">&nbsp;</div>

    <div class="h1" style='cursor:pointer;'>Category Section 3</div>
    <div id="main_list" style="display:none; margin-bottom:15px;">
        <!-- BEGIN recent-news -->
        <div class="list_left">
            <img src="./{AVATAR}"
                 style="width: 55px; height: 55px; vertical-align: top; margin-top: 2.4px; float: left; margin-right: 5px; border: solid 3px #fff"/>

            <p class="list_news">{TITLE}</p>

            <p>{TEXT}</p>

            <div style="clear:both">&nbsp;</div>
            <div style="float:left; margin-top: -6px;" class="icon-calendar">&nbsp;&nbsp;{DATE}</div>
            <div style="float:left; margin-top: -6px; margin-left: 20px;" class="icon-comments">&nbsp;&nbsp;12
                Comments
            </div>
            <div style="float:left; margin-top: -6px; margin-left: 20px;" class="icon-eye-open">&nbsp;&nbsp;20 Hits
            </div>
        </div>

        <!-- END recent-news --></div>
</div>
<div id="center_out">
    <div id="center_in">
        <!-- BEGIN row -->
        <div class="misssion_category">
            <div class="misssion_category_image" style="background:url({IMG})"></div>
            <div class="misssion_category_right">
                <p class="category_title">{NAME}</p>
                <br/>

                <p class="category_info">{DECS} </p>

                <p class="category_info">Completed <strong>0</strong> out of <strong>{MD}</strong> tasks.</p>
                <br/>
                <button class="formcss1"><a title="View item."
                                            href="/index.php?page=category&mode=view&id={ID}">View This Category</a>
                </button>
            </div>
            <div style="border-bottom:2px dashed rgba(153, 153, 153, 0.56);margin-top:10px;">&nbsp;</div>
        </div>

        <div class="clearfix">&nbsp;</div>


        <!-- END row -->
    </div>
</div>

