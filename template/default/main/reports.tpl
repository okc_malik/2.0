<!-- BEGIN default -->
<form>


<div class="mdl-cell mdl-cell--12-col-desktop mdl-cell--12-col-tablet mdl-cell--4-col-phone ">
    <table class="mdl-data-table mdl-js-data-table mdl-data-table--selectable mdl-shadow--2dp projects-table is-upgraded" data-upgraded=",MaterialDataTable">
        <thead>
        <tr>
            <th><label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect mdl-data-table__select mdl-js-ripple-effect--ignore-events is-upgraded" data-upgraded=",MaterialCheckbox,MaterialRipple"><input type="checkbox" class="mdl-checkbox__input"><span class="mdl-checkbox__focus-helper"></span><span class="mdl-checkbox__box-outline"><span class="mdl-checkbox__tick-outline"></span></span><span class="mdl-checkbox__ripple-container mdl-js-ripple-effect mdl-ripple--center" data-upgraded=",MaterialRipple"><span class="mdl-ripple is-animating" style="width: 103.823px; height: 103.823px; transform: translate(-50%, -50%) translate(18px, 18px);"></span></span></label></th><th class="mdl-data-table__cell--non-numeric">Name</th>
            <th class="mdl-data-table__cell--non-numeric">Created By</th>
            <th class="mdl-data-table__cell--non-numeric">Date</th>
            <th class="mdl-data-table__cell--non-numeric">Description</th>
            <th class="mdl-data-table__cell--non-numeric">Options</th>
        </tr>
        </thead>
        <tbody>
        <!-- BEGIN row -->
        <tr class="">
            <td><label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect mdl-data-table__select mdl-js-ripple-effect--ignore-events is-upgraded" data-upgraded=",MaterialCheckbox,MaterialRipple"><input type="checkbox" class="mdl-checkbox__input"><span class="mdl-checkbox__focus-helper"></span><span class="mdl-checkbox__box-outline"><span class="mdl-checkbox__tick-outline"></span></span><span class="mdl-checkbox__ripple-container mdl-js-ripple-effect mdl-ripple--center" data-upgraded=",MaterialRipple"><span class="mdl-ripple"></span></span></label></td>
            <td class="mdl-data-table__cell--non-numeric">{TITLE}</td>
            <td class="mdl-data-table__cell--non-numeric">
                <span class="label label--mini background-color--mint">{CREATORNAME}</span>
            </td>
            <td class="mdl-data-table__cell--non-numeric">{DATECREATED}</td>
            <td class="mdl-data-table__cell--non-numeric">{DESC}</td>
            <td class="mdl-data-table__cell--non-numeric">
                <a href="{REFH}" class="button">View Report</a>
            </td>
            <!-- END row -->
        </tbody>
    </table>
</div>
</form>
<!-- END default -->

<!-- BEGIN create -->

<form action="" method="post" name="create" target="_self" enctype="multipart/form-data">

    <label for="fname">User Id For:</label>
    <input type="number" id="forid" required name="forid"><br><br>
    <textarea id="desc" rows="4" cols="50" placeholder="Description of the report." name="desc"></textarea>
    <br><br>
    Select File to Upload:
    <input type="file" name="file">
    <input type="submit" name="submit" value="Upload">

</form>
<!-- END create -->