<div id="forum_text">This Pirate Zone's Privacy Policy.</div>
<div class="boxone" style="margin-top: 10px; border-top: 1px solid #C5C5C5; border-left: 1px solid #C5C5C5; border-right: 1px solid #C5C5C5; border-bottom: 1px solid #C5C5C5;"><font class="font1" style="font-weight: normal;">Policy </font></div>
<div class="boxthree" style="border-left: 1px solid #C5C5C5; border-right: 1px solid #C5C5C5; border-bottom: 1px solid #C5C5C5; background: url(../template/default/images/fpattern.png) repeat;"><font class="section_activity" style="color:#000">Below is our pirvacy policy.<br/>
  <hr/>
  <br />
This Privacy Policy governs the manner in which Pirate-Zone collects, uses, maintains and discloses information collected from users (each, a "User") of the <a href="http://www.pirate-zone.com">www.pirate-zone.com</a> website ("Site"). This privacy policy applies to the Site and all products and services offered by Pirate-Zone.<br><br>

<b>Personal identification information</b><br><br>

We may collect personal identification information from Users in a variety of ways, including, but not limited to, when Users visit our site, register on the site, and in connection with other activities, services, features or resources we make available on our Site. Users may be asked for, as appropriate, email address. Users may, however, visit our Site anonymously. We will collect personal identification information from Users only if they voluntarily submit such information to us. Users can always refuse to supply personally identification information, except that it may prevent them from engaging in certain Site related activities.<br><br>

<b>Non-personal identification information</b><br><br>

We may collect non-personal identification information about Users whenever they interact with our Site. Non-personal identification information may include the browser name, the type of computer and technical information about Users means of connection to our Site, such as the operating system and the Internet service providers utilized and other similar information.<br><br>

<b>Web browser cookies</b><br><br>

Our Site may use "cookies" to enhance User experience. User's web browser places cookies on their hard drive for record-keeping purposes and sometimes to track information about them. User may choose to set their web browser to refuse cookies, or to alert you when cookies are being sent. If they do so, note that some parts of the Site may not function properly.<br><br>

<b>How we use collected information</b><br><br>

Pirate-Zone may collect and use Users personal information for the following purposes:<br>
<ul>
<li><i>- To improve customer service</i><br>
	Information you provide helps us respond to your customer service requests and support needs more efficiently.</li>
<li><i>- To send periodic emails</i><br>
We may use the email address to respond to their inquiries, questions, and/or other requests. If User decides to opt-in to our mailing list, they will receive emails that may include company news, updates, related product or service information, etc. If at any time the User would like to unsubscribe from receiving future emails, they may do so by contacting us via our Site.</li>
</ul>
<b>How we protect your information</b><br><br>

We adopt appropriate data collection, storage and processing practices and security measures to protect against unauthorized access, alteration, disclosure or destruction of your personal information, username, password, transaction information and data stored on our Site.<br><br>

<b>Sharing your personal information</b><br><br>

We do not sell, trade, or rent Users personal identification information to others. We may share generic aggregated demographic information not linked to any personal identification information regarding visitors and users with our business partners, trusted affiliates and advertisers for the purposes outlined above.<br><br>

<b>Advertising</b><br><br>

Ads appearing on our site may be delivered to Users by advertising partners, who may set cookies. These cookies allow the ad server to recognize your computer each time they send you an online advertisement to compile non personal identification information about you or others who use your computer. This information allows ad networks to, among other things, deliver targeted advertisements that they believe will be of most interest to you. This privacy policy does not cover the use of cookies by any advertisers.<br><br>

<b>Changes to this privacy policy</b><br><br>

Pirate-Zone has the discretion to update this privacy policy at any time. When we do, we will revise the updated date at the bottom of this page. We encourage Users to frequently check this page for any changes to stay informed about how we are helping to protect the personal information we collect. You acknowledge and agree that it is your responsibility to review this privacy policy periodically and become aware of modifications.<br><br>

<b>Your acceptance of these terms</b><br><br>

By using this Site, you signify your acceptance of this policy. If you do not agree to this policy, please do not use our Site. Your continued use of the Site following the posting of changes to this policy will be deemed your acceptance of those changes.<br><br>

<b>Contacting us</b><br><br>

If you have any questions about this Privacy Policy, the practices of this site, or your dealings with this site, please contact us at:<br>
<a href="http://www.pirate-zone.com">Pirate-Zone</a><br>
<a href="http://www.pirate-zone.com">www.pirate-zone.com</a><br>
webmaster@pirate-zone.com<br>
<br>
This document was last updated on May 13, 2013<br><br>

