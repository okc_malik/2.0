<div style="margin-top:10px; width:1000px"></div>
<div id="left">
    <div class="h1" style='cursor:pointer;'>{NAME}'s Recent Topics</div>
    <div id="main_list" style="margin-bottom:10px;">
        <!-- BEGIN recent_topics -->
        <div class="list_left">
            <p class="list_news">Title: <a href="http://forum.senior.walentsoftware.com/index.php?area=forum&section=topic&t={ID}"
                                           class="list_news">{TITLE}</a></p>

            <p>{TEXT}</p>

            <div style="clear:both">&nbsp;</div>
            <div style="float:left; margin-top: -6px;" class="icon-calendar">&nbsp;&nbsp;{DATE}</div>
            <div style="float:left; margin-top: -6px; margin-left: 20px;" class="icon-comments">&nbsp;&nbsp;{POSTS}
                Replies
            </div>
            <div style="float:left; margin-top: -6px; margin-left: 20px;" class="icon-eye-open">&nbsp;&nbsp;20 Hits
            </div>
            <br>
        </div>

        <!-- END recent_topics --></div>
    <div style="clear:both">&nbsp;</div>
    <div style="clear:both">&nbsp;</div>

    <div class="h1" style='cursor:pointer;'>{NAME}'s Recent Posts</div>
    <div id="main_list" style="margin-bottom:15px; display:none">
        <!-- BEGIN recent_posts -->
        <div class="list_left">
            <!--<p class="list_news">Title: <a href="http://forum.senior.walentsoftware.com/index.php?area=forum&section=topic&t={TOPICID}" class="list_news">{TITLE}</a></p>-->

            <p>{TEXT}</p>

            <div style="clear:both">&nbsp;</div>
            <div style="float:left; margin-top: -6px;" class="icon-calendar">&nbsp;&nbsp;{DATE}</div>
            <div style="float:left; margin-top: -6px; margin-left: 20px;" class="icon-ok-circle">&nbsp;&nbsp;<a
                        href="http://forum.senior.walentsoftware.com/index.php?area=forum&section=topic&t={TOPICID}"
                        style="color:#555; text-decoration:underline">Topic</a></div>
            <div style="float:left; margin-top: -6px; margin-left: 20px;" class="icon-eye-open">&nbsp;&nbsp;20 Likes
            </div>
            <br>
        </div>

        <!-- END recent_posts --></div>

    <div style="clear:both">&nbsp;</div>
    <div style="clear:both">&nbsp;</div>

    <div class="h1" style='cursor:pointer;'>Recent Achievements</div>
    <div id="main_list" style="display:none; margin-bottom:15px;">
        <div class="list_left" style="border-bottom:none">
            <p class="e_title" style="margin:0 0 0 0; font-size:15px">Members</p>

            <p>Total Members: {TOT}</p>

            <p>Latest Member: <u><a href="http://senior.walentsoftware.com/?area=profile&user={NID}">{NNAME}</a></u></p>

            <div style="clear:both">&nbsp;</div>
            <p class="e_title" style="margin:0 0 0 5px; font-size:15px">Online</p>

            <p>Webmasters online: {W}<br/>
                Admins online: {A}<br/>
                Moderators online: {M}<br/>
                Members online: {ME}<br/>
                Guests online: {G}</p>
        </div>
    </div>
    <div style="clear:both">&nbsp;</div>
    <div style="clear:both">&nbsp;</div>
</div>
<div id="center_in">
    <div id="profile">
        <div id="profile_header">
            <div class="profile_avatar"><img src="{AVATAR}" style="margin-top:15px; border:3px solid #666;"/>

                <p class="profile_name">{STATUS} {NAME}</p></div>
            <div class="profile_header2">


                <p class="profile_exp">{EXP} Experience Points</p> <br/>

                <div class="exp_bar">
                    <div style="width: 75%; height:100%; background:#CCC">&nbsp;</div>
                </div>
                <p class="current_exp">75/100</p>

            </div>

            <div class="profile_header3">
                <!-- <p class="level">{LEVEL}</p>

                <p class="rank">{RANK}</p> <br/>

                <p class="ratio">Experience Rank:&nbsp;&nbsp;{ERANK}</p>

                <p class="ratio">Wins:&nbsp;&nbsp;{WIN}</p>

                <p class="ratio">Losses:&nbsp;&nbsp;{LOSS}</p>

                <p class="ratio">Win Percentage:&nbsp;&nbsp;{WINP}</p>-->
            </div>
        </div>
        <div style="clear:both">&nbsp;</div>
        <div class="user_info_1">
            <p class="user_info_header"> {NAME}'s Information</p>

            <div style="padding: 10px;">
                <p style="font-family: Calibri, Arial, sans-serif; font-size: 15px; color: rgb(33, 122, 153); padding: 10px 10px 5px 10px; border-bottom: 3px dotted #ccc;">
                    &nbsp;&nbsp;&nbsp;&nbsp; User-ID: <span style="color: #fff; font-weight:bold">{ID}</span></p>

                <p style="font-family: Calibri, Arial, sans-serif; font-size: 15px; color: rgb(33, 122, 153); padding: 10px 10px 5px 10px; border-bottom: 3px dotted #ccc;">
                    &nbsp;&nbsp;&nbsp;&nbsp;Site Rank: <span style="color: #fff; font-weight:bold">{SITERANK}</span></p>

                <p style="font-family: Calibri, Arial, sans-serif; font-size: 15px; color: rgb(33, 122, 153); padding: 10px 10px 5px 10px; border-bottom: 3px dotted #ccc;">
                    &nbsp;&nbsp;&nbsp;&nbsp;Forum Rank: <span style="color: #fff;font-weight:bold">{RANK}</span></p>

                <p style="font-family: Calibri, Arial, sans-serif; font-size: 15px; color: rgb(33, 122, 153); padding: 10px 10px 5px 10px; border-bottom: 3px dotted #ccc;">
                    &nbsp;&nbsp;&nbsp;&nbsp;Email: <span style="color: #fff;font-weight:bold">N/A</span></p>

                <p style="font-family: Calibri, Arial, sans-serif; font-size: 15px; color: rgb(33, 122, 153); padding: 10px 10px 5px 10px; border-bottom: 3px dotted #ccc;">
                    &nbsp;&nbsp;&nbsp;&nbsp;Last seen online: <span style="color: #fff;font-weight:bold"><a href="{FL}"><b> {PAGE}</b></a>
                </p>

                <p style="font-family: Calibri, Arial, sans-serif; font-size: 15px; color: rgb(33, 122, 153); padding: 10px 10px 5px 10px; border-bottom: 3px dotted #ccc;">
                    &nbsp;&nbsp;&nbsp;&nbsp; Last Online: <span style="color: #fff;font-weight:bold">{LASTLOGIN}</span>
                </p>

                <p style="font-family: Calibri, Arial, sans-serif; font-size: 15px; color: rgb(33, 122, 153); padding: 10px 10px 5px 10px; border-bottom: 3px dotted #ccc;">
                    &nbsp;&nbsp;&nbsp;&nbsp; Registered on: <span style="color: #fff;font-weight:bold">{JOINED}</span>
                </p>
            </div>
        </div>
        <div class="user_info_2"><p class="user_info_header"> Faction Information</p>

            <div style="padding: 10px;">
                <div class="profile_avatar" style="background:none;margin-right:5px"><img src="{AVATAR}"
                                                                                          style="margin-top:10px; border:3px solid #666;"/>

                    <p class="profile_name" style="font-size:15px;">Factionless </p></div>
                <p class="ratio"><span style="color: rgb(33, 122, 153);">Rank:</span> {RANK}</p>
                <br/>

                <p class="ratio"><span style="color: rgb(33, 122, 153);">Joined:</span> N/A</p>
                <br/>

                <!--<p class="ratio"><span style="color: rgb(33, 122, 153);">Wins:</span> {CWIN}</p>
                <br/>-->

                <!--<p class="ratio"><span style="color: rgb(33, 122, 153);">Losses:</span> {CLOSS}</p>
                <br/>-->

                <p class="ratio"><span style="color: rgb(33, 122, 153);">Faction Rank:</span> {CRANK}</p>
                <br/>

                <p class="user_info_header2">Gaming Information</p>
            </div>
        </div>
        <div style="clear:both">&nbsp;</div>
        <div class="about_me">
            <div class="profile_avatar" style="margin-right:10px;background:none"><img src="{AVATAR}"
                                                                                       style="margin-top:15px; border:3px solid #666;"/>

                <p class="profile_name">About Me!</p></div>
            <p class="about_me_text">bleh blebh blehbleh blebh blehbleh blebh blehbleh blebh blehbleh blebh blehbleh
                blebh blehbleh blebh blehbleh blebh blehbleh blebh blehbleh blebh blehbleh blebh blehbleh blebh blehbleh
                blebh blehbleh blebh blehbleh blebh blehbleh blebh bleh</p></div>
    </div>
</div>
</div>
<div style="clear:both;">&nbsp;</div>