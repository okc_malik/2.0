<div style="margin-top:10px; width:1000px"></div>
<div id="left">
    <div class="h1" style='cursor:pointer;'>{TITLE}</div>
    <div id="main_list" style="margin-bottom:10px;">
        <!-- BEGIN recent-news -->
        <div class="m_list_left">
            <p class="list_news">Message Statistics</p>

            <p style="color: #7C7C7C">Users who've you have messaged the most!</p>

            <div class="avatar_cell">
                <img src="./{AVATAR}" title="Username Tag Here"
                     style="width: 45px; height: 45px; vertical-align: top; margin-top: 10px; float: left; border: solid 3px #fff"/>&nbsp;&nbsp;<img
                        src="./{AVATAR}"
                        style="width: 45px; height: 45px; vertical-align: top; margin-top: 10px; margin-left: 25px; float: left; border: solid 3px #fff"/><img
                        src="./{AVATAR}"
                        style="width: 45px; height: 45px; vertical-align: top; margin-top: 10px; margin-left: 25px; float: left; border: solid 3px #fff"/>
            </div>

            <div style="clear:both">&nbsp;</div>
            <div class="line"></div>

            <p style="margin-top: 5px; color: #7C7C7C">Users who've messaged you the most!</p>

            <div class="avatar_cell">
                <img src="./{AVATAR}" title="Username Tag Here"
                     style="width: 45px; height: 45px; vertical-align: top; margin-top: 10px; float: left; border: solid 3px #fff"/>&nbsp;&nbsp;<img
                        src="./{AVATAR}"
                        style="width: 45px; height: 45px; vertical-align: top; margin-top: 10px; margin-left: 25px; float: left; border: solid 3px #fff"/><img
                        src="./{AVATAR}"
                        style="width: 45px; height: 45px; vertical-align: top; margin-top: 10px; margin-left: 25px; float: left; border: solid 3px #fff"/>
            </div>

            <div style="clear:both">&nbsp;</div>
        </div>

        <!-- END recent-news --></div>

    <div style="clear:both">&nbsp;</div>
    <div style="clear:both">&nbsp;</div>

    <div class="h1" style='cursor:pointer;'>Message Legend</div>
    <div id="main_list" style="display:none; margin-bottom:15px;">
        <div class="list_left" style="border-bottom:none">
            <p class="e_title" style="margin:0 0 0 0; font-size:15px">Members</p>

            <p>Total Members: {TOT}</p>

            <p>Latest Member: <u><a href="http://senior.walentsoftware.com/?area=profile&user={NID}">{NNAME}</a></u></p>

            <div style="clear:both">&nbsp;</div>
            <p class="e_title" style="margin:0 0 0 5px; font-size:15px">Online</p>

            <p>Webmasters online: {W}<br/>
                Admins online: {A}<br/>
                Moderators online: {M}<br/>
                Members online: {ME}<br/>
                Guests online: {G}</p>
        </div>
    </div>
    <div style="clear:both">&nbsp;</div>
    <div style="clear:both">&nbsp;</div>
</div>

<div id="mail_out">
    <div id="mail_in">
        <a href="?page=mail&amp;action=new" style="margin-right: 10px; text-decoration: none;">
            <button class="formcss1" style="margin-left: 0;">Create new private message</button>
        </a><a href="?page=mail" style="margin-right: 10px; text-decoration: none;">
            <button class="formcss1" style="margin-left: 0;">Inbox</button>
        </a><a href="?page=mail&amp;action=sent" style="margin-right: 10px; text-decoration: none;">
            <button class="formcss1" style="margin-left: 0; background:#666">Outbox</button>
        </a><!-- BEGIN delete -->
        <input style="margin-left: 30px" name="delete" type="submit" id="delete"
               value="Delete the selected messages" class="formcss1"></form>
        <!-- END delete -->

        <!-- BEGIN messages -->
        <table class="table">

            <tr>
                <td class="table1"><input name="checkbox[]" style="margin: 0px auto" type="checkbox" id="checkbox[]"
                                          value="{ID}"></td>
                <td class="number">
                    <div class="{ICON}">&nbsp;</div>
                </td>
                <td class="table3">Message sent to:</td>
                <td class="table4"><a href="http://senior.walentsoftware.com/?page=profile&user={BY}" class="tooltip"
                                      data="{NAME}"><img src="{AVATAR}" class="avatar_fix"/></a></td>
                <td class="table1"><a href="./?page=mail&amp;action=view&amp;m={ID}" class="tooltip"
                                      data="{TITLE}"><b>{TITLE1}</b></a></td>
                <td class="table2"><i>Message sent on</i> {DATE}</td>
            </tr>
        </table>
        <!-- END messages -->

    </div>
</div>

<div style="clear:both;">&nbsp;</div>