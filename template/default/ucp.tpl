<div id="left">
    <div class="h1" style='cursor:pointer;'>Website Area Settings</div>
    <div id="main_list" style="margin-bottom:10px;">
        <div class="list_left">
            <p class="list_news">

            <div align="center"><a href="./?account=ucp&amp;mode=account" class="ui"
                                   style="font-size: 15px; margin-bottom: 10px; color:#666">Change Account
                    Details</a> <br/><a
                        href="./?account=ucp&amp;mode=details" class="ui"
                        style="font-size: 15px; margin-bottom: 10px; color:#666">Change Personal Details</a><br/>
                <a
                        href="./?account=ucp&amp;mode=settings" class="ui"
                        style="font-size: 15px; margin-bottom: 10px; color:#666">Change Website Details</a> <br/>
                <a
                        href="./?account=ucp&amp;mode=avatar" class="ui"
                        style="font-size: 15px; margin-bottom: 10px; color:#666">Change Avatar</a></div>
            </p>
        </div>
    </div>
    <div style="clear:both">&nbsp;</div>
    <div style="clear:both">&nbsp;</div>

    <div class="h1" style='cursor:pointer;'>Forum Area Settings</div>
    <div id="main_list" style="margin-bottom:15px">
        <div class="list_left">
            <p class="list_news">

            <div align="center"><a
                        href="http://forum.senior.walentsoftware.com/?account=ucp&amp;mode=signature" target="_blank"
                        class="ui" style="font-size: 15px; color:#666;">Change Signature</a></div>
            </p>
        </div>
    </div>
    <div style="clear:both">&nbsp;</div>
    <div style="clear:both">&nbsp;</div>
</div>

<div id="cp_center_out">
<div id="cp_center_in" style="padding:5px;">

    <!-- BEGIN default
    <td width="100%" valign="top">

        <div style="margin-left:10px">
            <div id="ucpdef_container">
                <div class="header_top"><font class="font1" style="font-weight: normal;">User Control Panel -
                        Default
                        Panel</font></div>
                <div id="ucp_wrapper">
                    <div id="ucpdef_first">
                        <div style="text-align: center;"><img src=""/></div>
                        <div>
                            <p class="ucpdef_text"><span
                                        style="vertical-align: middle; margin-right: 10px;">&#x2717; </span>Total
                                Post
                                Count: <span style="color: #666;">{POSTCOUNT}</span></p>

                            <p class="ucpdef_text"><span
                                        style="vertical-align: middle; margin-right: 10px;">&#x2717; </span>Date
                                Registered: <span style="color: #666;"> {JOINED} </span></p>

                            <p class="ucpdef_text"><span
                                        style="vertical-align: middle; margin-right: 10px;">&#x2717; </span>{L_EMAIL}
                                :
                                <span style="color: #666">{EMAIL} - <u>Hidden</u></span></p>

                            <p class="ucpdef_text"><span
                                        style="vertical-align: middle; margin-right: 10px;">&#x2717; </span>Site
                                Rank:
                                <span style="color: #666">{SITERANK}</span></p>

                            <p class="ucpdef_text"><span
                                        style="vertical-align: middle; margin-right: 10px;">&#x2717; </span>Forum
                                Rank:
                                <span style="color: #666">{RANK}</span></p>

                            <p class="ucpdef_text"><span
                                        style="vertical-align: middle; margin-right: 10px;">&#x2717; </span>Last
                                Login:
                                <span style="color: #666;"> {LASTLOGIN} </span></p>

                            <p class="ucpdef_text"><span
                                        style="vertical-align: middle; margin-right: 10px;">&#x2717; </span>Nickname:
                                <span style="color: #666;">{NICKN}</span></p>

                            <p class="ucpdef_text"><span
                                        style="vertical-align: middle; margin-right: 10px;">&#x2717; </span>
                                Warnings:
                                <span style="color: #000;">{WARNING}</span></p>

                            <p class="ucpdef_text"><span
                                        style="vertical-align: middle; margin-right: 10px;">&#x2717; </span>Amount
                                of
                                exp: <span style="color: #666;">0</span></p>
                        </div>
                    </div>
                    <div id="ucpdef_second">
                        {AVATAR}
                        <div style="margin-top: 30px; margin-left: -10px;">{RANKIMG}</div>
                    </div>
                    <div style="clear: both;">&nbsp;</div>
                </div>
            </div>
        </div>
    </td>
    </tr></table>
    <div style="clear: both;">&nbsp;</div>
     END default -->


    <!-- BEGIN account -->
    <form action="" method="post" name="register" target="_self">
        <p id="reg_head" style="margin-top: 10px; text-align: center"> Use a valid email to handle your account.</p>
        <font style="padding-top: 10px; font-family: Calibri, Arial, sans-serif; font-size: 15px; color: #555; font-style: italic;">Email
            Address:</font>
        <input name="email" type="text" id="input1" value="{EMAIL}" size="40" maxlength="100"/>

        <p id="reg_head" style="margin-top: 10px; text-align: center"> You cannot change your username.</p>
        <font style="padding-top: 10px; font-family: Calibri, Arial, sans-serif; font-size: 15px; color: #555; font-style: italic;">Username:</font>
        <input name="name" type="text" id="input1" id="name" value="{NAME}" size="40" maxlength="40"
               readonly="readonly" style="opacity: 0.5;"/>

        <p id="reg_head" style="margin-top: 10px; text-align: center"> You must enter your old password to change
            your info.</p>
        <font style="padding-top: 10px; font-family: Calibri, Arial, sans-serif; font-size: 15px; color: #555; font-style: italic;">Password:</font>
        <input name="pass" type="password" id="input1" size="40" maxlength="40"
               placeholder="Enter your old password here"/>

        <p id="reg_head" style="margin-top: 10px; text-align: center"> Crete a new password here.</p>
        <font style="padding-top: 10px; font-family: Calibri, Arial, sans-serif; font-size: 15px; color: #555; font-style: italic;">New
            Password:</font>
        <input name="newpass" type="password" id="input1" size="40" maxlength="40"
               placeholder="Enter your new password here"/>

        <p id="reg_head" style="margin-top: 10px; text-align: center"> Crete a new password here.</p>
        <font style="padding-top: 10px; font-family: Calibri, Arial, sans-serif; font-size: 15px; color: #555; font-style: italic;">Confirm
            Password:</font>
        <input name="confirmpass" type="password" id="input1" size="40" maxlength="40"
               placeholder="Confirm Your Password"/>

        <div align="center" style="margin-top: 5px;"><input name="Submit" type="submit" class="formcss1"
                                                            value="confirm"/></div>
    </form>
</div>
<!-- END account -->

<!-- BEGIN details -->
<form action="" method="post" name="register" target="_self">

    <p id="reg_head" style="margin-top: 10px; text-align: center"> Select your gender to display in your profile.</p>
    <font style="padding-top: 10px; font-family: Calibri, Arial, sans-serif; font-size: 15px; color: #555; font-style: italic;">{L_GENDER}
        :</font>
    <select id="input1" name="gender">
        <option value="1" selected>{L_MALE}</option>
        <option value="2">{L_FEMALE}</option>
        <option value="3">{L_HIDDEN}</option>
    </select>

    <p id="reg_head" style="margin-top: 10px; text-align: center"> Enter your nickname to display in your profile.</p>
    <font style="padding-top: 10px; font-family: Calibri, Arial, sans-serif; font-size: 15px; color: #555; font-style: italic;">{L_NICKN}
        :</font>
    <input name="nickn" id="input1" type="text" value="{NICKN}" size="40" maxlength="40"/>

    <p id="reg_head" style="margin-top: 10px; text-align: center"> Tell others your location.</p>
    <font style="padding-top: 10px; font-family: Calibri, Arial, sans-serif; font-size: 15px; color: #555; font-style: italic;">{L_LOCATION}
        :</font>
    <input name="location" id="input1" type="text" value="{LOCATION}" size="40" maxlength="40"/>

    <p id="reg_head" style="margin-top: 10px; text-align: center"> Enter your facebook profile link to display in your profile.</p>
    <font style="padding-top: 10px; font-family: Calibri, Arial, sans-serif; font-size: 15px; color: #555; font-style: italic;">{L_FACEBOOK}
        :</font>
    <input name="fb" id="input1" type="text" placeholder="" value="{FB}" size="40" maxlength="40"/>

    <p id="reg_head" style="margin-top: 10px; text-align: center"> Enter your twitter profile link to display in your profile.</p>
    <font style="padding-top: 10px; font-family: Calibri, Arial, sans-serif; font-size: 15px; color: #555; font-style: italic;">{L_TWITTER}
        :</font>
    <input name="tw" id="input1" type="text" placeholder="" value="{TW}" size="40" maxlength="40"/>

    <p id="reg_head" style="margin-top: 10px; text-align: center"> Enter your tumblr profile link to display in your profile.</p>
    <font style="padding-top: 10px; font-family: Calibri, Arial, sans-serif; font-size: 15px; color: #555; font-style: italic;">{L_TUMBLR}
        :</font>
    <input name="tb" id="input1" type="text" placeholder="" value="{TB}" size="40" maxlength="40"/>

    <p id="reg_head" style="margin-top: 10px; text-align: center"> Your personal or favorite website.</p>
    <font style="padding-top: 10px; font-family: Calibri, Arial, sans-serif; font-size: 15px; color: #555; font-style: italic;">{L_WEBSITE}
        :</font>
    <input name="website" type="text" id="input1" value="{WEBSITE}" placeholder="Enter your website here."/>

    <p id="reg_head" style="margin-top: 10px; text-align: center"> Naruto-Arena Account.</p>
    <font style="padding-top: 10px; font-family: Calibri, Arial, sans-serif; font-size: 15px; color: #555; font-style: italic;">{L_NA}
        :</font>
    <input name="website" type="text" id="input1" value="{NA}" placeholder="Enter your Naruto-Arena account."/>

    <p id="reg_head" style="margin-top: 10px; text-align: center"> Soul-Arena Account.</p>
    <font style="padding-top: 10px; font-family: Calibri, Arial, sans-serif; font-size: 15px; color: #555; font-style: italic;">{L_SA}
        :</font>
    <input name="website" type="text" id="input1" value="{SA}" placeholder="Enter your Soul-Arena account."/>

    <p id="reg_head" style="margin-top: 10px; text-align: center"> WarofNinja Account.</p>
    <font style="padding-top: 10px; font-family: Calibri, Arial, sans-serif; font-size: 15px; color: #555; font-style: italic;">{L_WON}
        :</font>
    <input name="website" type="text" id="input1" value="{WON}" placeholder="Enter your WarofNinja account."/>

    <p id="reg_head" style="margin-top: 10px; text-align: center"> League of Legends Account.</p>
    <font style="padding-top: 10px; font-family: Calibri, Arial, sans-serif; font-size: 15px; color: #555; font-style: italic;">{L_LOL}
        :</font>
    <input name="website" type="text" id="input1" value="{LOL}" placeholder="Enter your Summoner's name."/>

    <p id="reg_head" style="margin-top: 10px; text-align: center"> Playstation Network ID.</p>
    <font style="padding-top: 10px; font-family: Calibri, Arial, sans-serif; font-size: 15px; color: #555; font-style: italic;">{L_PSN}
        :</font>
    <input name="website" type="text" id="input1" value="{PSN}" placeholder="Enter your Playstation Network."/>

    <p id="reg_head" style="margin-top: 10px; text-align: center"> Xbox Live ID.</p>
    <font style="padding-top: 10px; font-family: Calibri, Arial, sans-serif; font-size: 15px; color: #555; font-style: italic;">{L_XBL}
        :</font>
    <input name="website" type="text" id="input1" value="{XBL}" placeholder="Enter your Xbox Live."/>

    <p id="reg_head" style="margin-top: 10px; text-align: center"> Twitch Stream.</p>
    <font style="padding-top: 10px; font-family: Calibri, Arial, sans-serif; font-size: 15px; color: #555; font-style: italic;">{L_TWITCH}
        :</font>
    <input name="website" type="text" id="input1" value="{TWITCH}" placeholder="Enter your Twitch profile."/>

    <p id="reg_head" style="margin-top: 10px; text-align: center"> Enter your GMT format here.</p>
    <font style="padding-top: 10px; font-family: Calibri, Arial, sans-serif; font-size: 15px; color: #555; font-style: italic;">{L_TIMEZONE}
        :</font>
    <select name="timezone" id="input1">
        <option value="-43200"{a}>{L_GMT_MINUS_1200}</option>
        <option value="-39600"{b}>{L_GMT_MINUS_1100}</option>
        <option value="-36000"{bb}>{L_GMT_MINUS_1000}</option>
        <option value="-32400"{c}>{L_GMT_MINUS_900}</option>
        <option value="-28800"{d}>{L_GMT_MINUS_800}</option>
        <option value="-25200"{e}>{L_GMT_MINUS_700}</option>
        <option value="-21600"{f}>{L_GMT_MINUS_600}</option>
        <option value="-18000"{g}>{L_GMT_MINUS_500}</option>
        <option value="-14000"{h}>{L_GMT_MINUS_400}</option>
        <option value="-12200"{i}>{L_GMT_MINUS_330}</option>
        <option value="-10400"{j}>{L_GMT_MINUS_300}</option>
        <option value="-7200"{k}>{L_GMT_MINUS_200}</option>
        <option value="-3600"{l}>{L_GMT_MINUS_100}</option>
        <option value="0"{m}>{L_GMT_000}</option>
        <option value="3600"{n}>{L_GMT_PLUS_100}</option>
        <option value="7200"{o}>{L_GMT_PLUS_200}</option>
        <option value="10400"{p}>{L_GMT_PLUS_300}</option>
        <option value="12200"{q}>{L_GMT_PLUS_330}</option>
        <option value="14000"{r}>{L_GMT_PLUS_400}</option>
        <option value="16200"{rr}>{L_GMT_PLUS_430}</option>
        <option value="18000"{s}>{L_GMT_PLUS_500}</option>
        <option value="19800"{ss}>{L_GMT_PLUS_530}</option>
        <option value="20700"{sss}>{L_GMT_PLUS_545}</option>
        <option value="21600"{t}>{L_GMT_PLUS_600}</option>
        <option value="25200"{u}>{L_GMT_PLUS_700}</option>
        <option value="28800"{v}>{L_GMT_PLUS_800}</option>
        <option value="32400"{w}>{L_GMT_PLUS_900}</option>
        <option value="34200"{ww}>{L_GMT_PLUS_930}</option>
        <option value="36000"{www}>{L_GMT_PLUS_1000}</option>
        <option value="39600"{x}>{L_GMT_PLUS_1100}</option>
        <option value="43200"{y}>{L_GMT_PLUS_1200}</option>
    </select>

    <div align="center" style="margin-top: 10px;"><input name="Submit" type="submit" class="formcss1"
                                                         value="Update your details"/></div>
</form>
</div>
<div style="clear: both;">&nbsp;</div>
<!-- END details -->

<!-- BEGIN settings -->
<form action="" method="post" name="register" target="_self">
    <p id="reg_head" style="margin-top: 10px; text-align: center"> You have the option to choose the template you would
        like to see on
        the website.</p>
    <span class="ucp_input_info">Template:&nbsp;</span>
    <select name="template" id="ucp_input">
        {TEMPLATE_BOX}
    </select>

    <div style="text-align: center; margin-top: 10px;"><input name="Submit" type="submit" class="formcss1"
                                                              value="Update your settings"/></div>
</form>
<!-- END settings -->

<!-- BEGIN avatar -->
<p style="text-align: center; font-family: Calibri; font-size: 15px; color: #555;">1) The custom avatar must be
    100x100 (may be subject to change).</p>

<p style="text-align: center; font-family: Calibri; font-size: 15px; color: #555;">2) Custom avatars have to be
    below 100 KB.</p>

<p style="text-align: center; font-family: Calibri; font-size: 15px; color: #555; margin-bottom: 10px;">3) The only
    supported format is .png* any other formats will not work.</p>

<form name="newad" method="post" enctype="multipart/form-data" action="">
    <img src="./{AVATAR}"
         style="width: 100px; height: 100px; vertical-align: top; float: left; margin-left: 15px; border: solid 2px #666"/>

    <div style="float: left; text-align: center; margin-left: 100px; padding-top: 20px;">
        <input name="image" type="file" size="50" maxlength="50" class="ui"
               style="padding-left:15px;"/><br><br>
        <input name="Avi" type="submit" class="formcss1" value="Upload">
        <input name="Delete" type="submit" class="formcss1" value="Delete">
    </div>
    <div style="clear: both;">&nbsp;</div>
</form>
<!-- END avatar -->

<!-- BEGIN signature -->

<form action="" method="post" name="post" target="_self">
    <tr>

        <td colspan="2">
            <div align="center" style="margin-top: 10px;">

                <input type="button" value="B"
                       onclick="document.forms['post']. elements['signature'].value=document.forms['post']. elements['signature'].value+'[b][/b]'"
                       class="formcss1"/>

                <input type="button" value="i"
                       onclick="document.forms['post']. elements['signature'].value=document.forms['post']. elements['signature'].value+'[i][/i]'"
                       class="formcss1"/>

                <input type="button" value="U"
                       onclick="document.forms['post']. elements['signature'].value=document.forms['post']. elements['signature'].value+'[u][/u]'"
                       class="formcss1"/>

                <input type="button" value="Size"
                       onclick="document.forms['post']. elements['signature'].value=document.forms['post']. elements['signature'].value+'[size=8][/size]'"
                       class="formcss1"/>

                <input type="button" value="Color"
                       onclick="document.forms['post']. elements['signature'].value=document.forms['post']. elements['signature'].value+'[color=000000][/color]'"
                       class="formcss1"/>

                <input type="button" value="Align"
                       onclick="document.forms['post']. elements['signature'].value=document.forms['post']. elements['signature'].value+'[align=center]Text Here[/align]'"
                       class="formcss1"/>

                <input type="button" value="WWW"
                       onclick="document.forms['post']. elements['signature'].value=document.forms['post']. elements['signature'].value+'[url=http://link here]Text Here[/url]'"
                       class="formcss1"/>

                <input type="button" value="Img"
                       onclick="document.forms['post']. elements['signature'].value=document.forms['post']. elements['signature'].value+'[img]Text Here[/img]'"
                       class="formcss1"/>

                <input type="button" value="Youtube"
                       onclick="document.forms['post']. elements['signature'].value=document.forms['post']. elements['signature'].value+'[youtube]Text Here[/youtube]'"
                       class="formcss1"/>
                <input type="button" value="Spoiler"
                       onclick="document.forms['post']. elements['signature'].value=document.forms['post']. elements['signature'].value+'[spoiler= button text]content[/spoiler]'"
                       class="formcss1"/>

            </div>
        </td>

    </tr>

    <tr>
        <td>
            <div style="margin-top: 10px;" align="center"><textarea rows="2" name="signature" id="input1" cols="20"
                                                                    style="width: 600px; height: 100px;">{SIGNATURE}</textarea>
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <div align="center" style="margin-top: 5px;"><input name="Submit" type="submit" class="formcss1"
                                                                value="Update"/></div>
        </td>
    </tr>
</form>
</table></font></div>


</td></tr></table>
<div style="clear: both;">&nbsp;</div>


<!-- END signature -->


</div>

