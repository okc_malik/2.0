
<div id="mail_out">
    <div id="mail_in">
        <a href="?page=mail&amp;action=new" style="margin-right: 10px; text-decoration: none;">
            <button class="formcss1" style="margin-left: 0;">Create new private message</button>
        </a><a href="?page=mail" style="margin-right: 10px; text-decoration: none;">
            <button class="formcss1" style="margin-left: 0;">Inbox</button>
        </a><a href="?page=mail&amp;action=sent" style="margin-right: 10px; text-decoration: none;">
            <button class="formcss1" style="margin-left: 0;">Outbox</button>
        </a>

        <!-- BEGIN view -->
        <div class="table">
            <div class="message_title">{TITLE}  <p style="display: inline; float: right;">{DATE}</p></div>
            <div class="message_container">
                <div class="message_sender_info">
                    <p align="center" style="margin-bottom: 5px;">{STATUS} {AUTHOR}</p>

                    <p align="center">{RANK}</p>
                    <img src="{AVATAR}"
                         style="width: 100px; height: 100px; vertical-align: top; margin-top: 10px; margin-left:16px; margin-bottom:10px; border: solid 3px #666"/>
                </div>
                <div class="message_content">{TEXT}</div>
                <div style="clear:both">&nbsp;</div>
                <div class="message_signature">{SIGNATURE}</div>
                <div style="clear:both">&nbsp;</div>
            </div>
        </div>
        <!--<table class="table" cellspacing="0" cellpadding="2"><div class="message_title">{TITLE}  <p style="display: inline; float: right;">{DATE}</p></div>
 
      <tr class="message_container" style="border:2px dotted rgba(33, 122, 153, 1.0); padding: 5px;">
        <td valign="top" ><div class="message_sender_info">
                                    	<p align="center" style="margin-bottom: 5px;">{STATUS} {AUTHOR}</p>
                                    	<p align="center">{RANK}</p>
                                    	<img src="{AVATAR}" style="width: 100px; height: 100px; vertical-align: top; margin-top: 10px; margin-left:16px; margin-bottom:10px; border: solid 3px #666"/>
                                	</div></td><td class="" valign="top" style="word-wrap:break-word;max-width: 600px;  margin-top:8px;"><p class="message_content">{TEXT}</p><div class="message_signature">{SIGNATURE}</div></td></tr>
    </table>-->
        <!-- BEGIN reply -->
        <div class="message_title" style="margin-top:-0px; border-top:none">Send A Reply</div>
        <div class="message_container" style="border-bottom:2px dotted rgba(33, 122, 153, 1.0);">
            <form action="" method="post" name="postreply">
                <div align="center" style="margin-top: 10px; margin-bottom: 10px;">
                    <input type="button" value="Bold"
                           onclick="document.forms['postreply']. elements['message'].value=document.forms['postreply']. elements['message'].value+'[b][/b]'"
                           class="formcss1"/>
                    <input type="button" value="Italic"
                           onclick="document.forms['postreply']. elements['message'].value=document.forms['postreply']. elements['message'].value+'[i][/i]'"
                           class="formcss1"/>
                    <input type="button" value="Underline"
                           onclick="document.forms['postreply']. elements['message'].value=document.forms['postreply']. elements['message'].value+'[u][/u]'"
                           class="formcss1"/>
                    <input type="button" value="Text size"
                           onclick="document.forms['postreply']. elements['message'].value=document.forms['postreply']. elements['message'].value+'[size=8][/size]'"
                           class="formcss1"/>
                    <input type="button" value="Text color"
                           onclick="document.forms['postreply']. elements['message'].value=document.forms['postreply']. elements['message'].value+'[color=000000][/color]'"
                           class="formcss1"/>
                    <input type="button" value="Text align"
                           onclick="document.forms['postreply']. elements['message'].value=document.forms['postreply']. elements['message'].value+'[align=center]Text Here[/align]'"
                           class="formcss1"/>
                    <input type="button" value="Link"
                           onclick="document.forms['postreply']. elements['message'].value=document.forms['postreply']. elements['message'].value+'[url=http://Link Here]Text Here[/url]'"
                           class="formcss1"/>
                    <input type="button" value="Image"
                           onclick="document.forms['postreply']. elements['message'].value=document.forms['postreply']. elements['message'].value+'[img]Text Here[/img]'"
                           class="formcss1"/>
                    <input type="button" value="Youtube video"
                           onclick="document.forms['postreply']. elements['message'].value=document.forms['postreply']. elements['message'].value+'[youtube]Text Here[/youtube]'"
                           class="formcss1"/>
                    <input type="button" value="Spoiler"
                           onclick="document.forms['postreply']. elements['message'].value=document.forms['postreply']. elements['message'].value+'[Spoiler=button text]content[/spoiler]'"
                           class="formcss1"/>
                </div>
                <div align="center">
                    <a nohref
                       onclick="document.forms['postreply']. elements['message'].value=document.forms['postreply']. elements['message'].value+'[angry]'"/><img
                            src="./images/smiley/angry.gif" alt="" border="0"></a>
                    <a nohref
                       onclick="document.forms['postreply']. elements['message'].value=document.forms['postreply']. elements['message'].value+'[annoyed]'"/><img
                            src="./images/smiley/annoyed.gif" alt="" border="0"></a>
                    <a nohref
                       onclick="document.forms['postreply']. elements['message'].value=document.forms['postreply']. elements['message'].value+'[biggrin]'"/><img
                            src="./images/smiley/biggrin.gif" alt="" border="0"></a>
                    <a nohref
                       onclick="document.forms['postreply']. elements['message'].value=document.forms['postreply']. elements['message'].value+'[blink]'"/><img
                            src="./images/smiley/blink.gif" alt="" border="0"></a>
                    <a nohref
                       onclick="document.forms['postreply']. elements['message'].value=document.forms['postreply']. elements['message'].value+'[confused]'"/><img
                            src="./images/smiley/confused.gif" alt="" border="0"></a>
                    <a nohref
                       onclick="document.forms['postreply']. elements['message'].value=document.forms['postreply']. elements['message'].value+'[cool]'"/><img
                            src="./images/smiley/cool.gif" alt="" border="0"></a>
                    <a nohref
                       onclick="document.forms['postreply']. elements['message'].value=document.forms['postreply']. elements['message'].value+'[dry]'"/><img
                            src="./images/smiley/dry.gif" alt="" border="0"></a>
                    <a nohref
                       onclick="document.forms['postreply']. elements['message'].value=document.forms['postreply']. elements['message'].value+'[frown]'"/><img
                            src="./images/smiley/frown.gif" alt="" border="0"></a>
                    <a nohref
                       onclick="document.forms['postreply']. elements['message'].value=document.forms['postreply']. elements['message'].value+'[happy]'"/><img
                            src="./images/smiley/happy.gif" alt="" border="0"></a>
                    <a nohref
                       onclick="document.forms['postreply']. elements['message'].value=document.forms['postreply']. elements['message'].value+'[huh]'"/><img
                            src="./images/smiley/huh.gif" alt="" border="0"></a>
                    <a nohref
                       onclick="document.forms['postreply']. elements['message'].value=document.forms['postreply']. elements['message'].value+'[laugh]'"/><img
                            src="./images/smiley/laugh.gif" alt="" border="0"></a>
                    <a nohref
                       onclick="document.forms['postreply']. elements['message'].value=document.forms['postreply']. elements['message'].value+'[mad]'"/><img
                            src="./images/smiley/mad.gif" alt="" border="0"></a>
                    <a nohref
                       onclick="document.forms['postreply']. elements['message'].value=document.forms['postreply']. elements['message'].value+'[nerd]'"/><img
                            src="./images/smiley/nerd.gif" alt="" border="0"></a>
                    <a nohref
                       onclick="document.forms['postreply']. elements['message'].value=document.forms['postreply']. elements['message'].value+'[ohmy]'"/><img
                            src="./images/smiley/ohmy.gif" alt="" border="0"></a>
                    <a nohref
                       onclick="document.forms['postreply']. elements['message'].value=document.forms['postreply']. elements['message'].value+'[riiight]'"/><img
                            src="./images/smiley/riiight.gif" alt="" border="0"></a>
                    <a nohref
                       onclick="document.forms['postreply']. elements['message'].value=document.forms['postreply']. elements['message'].value+'[sad]'"/><img
                            src="./images/smiley/sad.gif" alt="" border="0"></a>
                    <a nohref
                       onclick="document.forms['postreply']. elements['message'].value=document.forms['postreply']. elements['message'].value+'[shocked]'"/><img
                            src="./images/smiley/shocked.gif" alt="" border="0"></a>
                    <a nohref
                       onclick="document.forms['postreply']. elements['message'].value=document.forms['postreply']. elements['message'].value+'[shy]'"/><img
                            src="./images/smiley/shy.gif" alt="" border="0"></a>
                    <a nohref
                       onclick="document.forms['postreply']. elements['message'].value=document.forms['postreply']. elements['message'].value+'[smile]'"/><img
                            src="./images/smiley/smile.gif" alt="" border="0"></a>
                    <a nohref
                       onclick="document.forms['postreply']. elements['message'].value=document.forms['postreply']. elements['message'].value+'[surprised]'"/><img
                            src="./images/smiley/surprised.gif" alt="" border="0"></a>
                    <a nohref
                       onclick="document.forms['postreply']. elements['message'].value=document.forms['postreply']. elements['message'].value+'[tired]'"/><img
                            src="./images/smiley/tired.gif" alt="" border="0"></a>
                    <a nohref
                       onclick="document.forms['postreply']. elements['message'].value=document.forms['postreply']. elements['message'].value+'[tongue]'"/><img
                            src="./images/smiley/tongue.gif" alt="" border="0"></a>
                    <a nohref
                       onclick="document.forms['postreply']. elements['message'].value=document.forms['postreply']. elements['message'].value+'[wink]'"/><img
                            src="./images/smiley/wink.gif" alt="" border="0"></a>

                </div>
                <div align="center">
                    <textarea name="message" id="message" rows="4" cols="40" style="width: 95%; height: 100"></textarea>
                </div>
                <div align="center"><input name="Submit" type="submit" value="Send" class="formcss1"/></div>

            </form>
            <!-- END reply -->

            <!-- END view -->

        </div>
    </div>

    <div style="clear:both;">&nbsp;</div>